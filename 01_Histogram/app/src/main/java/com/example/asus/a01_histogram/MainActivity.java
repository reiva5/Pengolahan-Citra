package com.example.asus.a01_histogram;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.CombinedChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.CombinedData;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    static final int REQUEST_IMAGE_CAPTURE = 1;
    static final String LOG_TAG = "MainClass";
    String mCurrentPhotoPath;
    static final int PICK_IMAGE_REQUEST = 2;
    private int width, height;
    private int[] red = new int[256];
    private int[] green = new int[256];
    private int[] blue = new int[256];

    BarChart redBarChart;

    private Bitmap img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dispatchTakePictureIntent();
        getPhotoFromFile();
    }

    private void getPhotoFromFile(){
        Button getPhotoFromFileButton = findViewById(R.id.get_button);

        getPhotoFromFileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent photoIntent = new Intent();
                photoIntent.setType("image/*");
                photoIntent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(photoIntent, "Select Picture"),
                        PICK_IMAGE_REQUEST);
            }
        });

    }

    private void dispatchTakePictureIntent(){
        Button takePictureButton = findViewById(R.id.take_picture_button);

        takePictureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                getPicture();
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        });
    }

//    private void getPicture(){
//        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//        startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
//        if (takePictureIntent.resolveActivity(getPackageManager()) != null){
//            File photoFile = null;
//            try {
//                photoFile = createImageFile();
//            } catch (IOException ex){
//                Log.d(LOG_TAG, "error get photo file");
//            }


//            if (photoFile != null){
//                Uri photoURI = FileProvider.getUriForFile(getApplicationContext(),
//                        "com.example.android.fileprovider",
//                        photoFile);
//                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
//                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
//            }
//        }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        ImageView mImageView = (ImageView) findViewById(R.id.photo_view);
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK){
            Bundle extras = data.getExtras();
            Bitmap bitmap = (Bitmap) extras.get("data");
            mImageView.setImageBitmap(bitmap);
            getBitmapPixel(bitmap);
            img = bitmap;
//            makeCombinedChart();
            makeHistogram(red, 256, R.id.histogram_red);
            makeHistogram(green, 256, R.id.histogram_green);
            makeHistogram(blue, 256, R.id.histogram_blue);
        } else if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK){
            Uri uri = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                getBitmapPixel(bitmap);
                mImageView.setImageBitmap(bitmap);
                img = bitmap;
//                makeCombinedChart();
                makeHistogram(red, 256, R.id.histogram_red);
                makeHistogram(green, 256, R.id.histogram_green);
                makeHistogram(blue, 256, R.id.histogram_blue);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void getBitmapPixel(Bitmap bitmap) {
        width = bitmap.getWidth();
        height = bitmap.getHeight();
        int pixel;
        for (int i = 0; i < 256; i++) {
            red[i] = 0;
            green[i] = 0;
            blue[i] = 0;
        }

        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                pixel = bitmap.getPixel(i, j);
                red[Color.red(pixel)]++;
                green[Color.green(pixel)]++;
                blue[Color.blue(pixel)]++;
            }
        }
//        Log.d(LOG_TAG, String.valueOf(red[100]));
//        Log.d(LOG_TAG, String.valueOf(green[100]));
//        Log.d(LOG_TAG, String.valueOf(blue[100]));
    }

//    public BarData generateBarData(int[] arr, int length, int choose_rgb){
//        ArrayList<BarEntry> barEntries = new ArrayList<>();
//        ArrayList<String> xAxis = new ArrayList<>();
//
//        for (int i=0; i<length; i++){
//            barEntries.add(new BarEntry(i, arr[i]));
//            xAxis.add(""+i);
//        }
//
//        BarDataSet barDataSet = new BarDataSet(barEntries, "pixel");
//        // choose_rgb, 0->red, 1->green, 2->blue
//        if (choose_rgb == 0){
//            barDataSet.setColor(Color.rgb(255, 0,0));
//        }
//        else if (choose_rgb == 1){
//            barDataSet.setColor(Color.rgb(0, 255,0));
//        }
//        else if (choose_rgb == 2){
//            barDataSet.setColor(Color.rgb(0, 0,255));
//        }
//
//        BarData barData = new BarData(barDataSet);
//        return barData;
//    }

//    public void makeLineChart(int length){
//        ArrayList<Entry> redEntries = new ArrayList<>();
//        ArrayList<Entry> greenEntries = new ArrayList<>();
//        ArrayList<Entry> blueEntries = new ArrayList<>();

//        ArrayList<String> xAxis = new ArrayList<>();
//        String[] xAxis = new String[length];
//
//        for (int i=0; i<length; i++){
//            redEntries.add(new BarEntry(i, red[i]));
//            greenEntries.add(new BarEntry(i, green[i]));
//            blueEntries.add(new BarEntry(i, blue[i]));
//            xAxis[i]=""+i;
//        }
//        ArrayList<LineDataSet> lines = new ArrayList<LineDataSet>();
//        LineDataSet set = new LineDataSet(redEntries, "red");
//        set.setDrawFilled(true);
//        lines.add(set);
//
//        LineDataSet set2 = new LineDataSet(greenEntries, "green");
//        set2.setDrawFilled(true);
//        lines.add(set2);
//
//        LineDataSet set3 = new LineDataSet(blueEntries, "blue");
//        set3.setDrawFilled(true);
//        lines.add(set3);
//
//        LineChart lineChart = (LineChart) findViewById(R.id.line_chart);
//        lineChart.setVisibility(View.VISIBLE);
//        lineChart.getDescription().setEnabled(false);
//        lineChart.getXAxis().setDrawGridLines(false);
//
//        lineChart.getAxisLeft().setDrawLabels(false);
//        lineChart.getAxisRight().setDrawLabels(false);
//        lineChart.getXAxis().setDrawLabels(false);
//        lineChart.getLegend().setEnabled(false);
//
//        LineData chartData = new LineData();
//        chartData.addDataSet(set);
//        chartData.addDataSet(set2);
//        chartData.addDataSet(set3);
//
//        lineChart.setData(chartData);
//
//        lineChart.invalidate();
//    }

//    public void makeCombinedChart(){
//        CombinedChart mChart = (CombinedChart) findViewById(R.id.combined_chart);
//        mChart.getDescription().setText("RGB");
//        mChart.setDrawOrder(new CombinedChart.DrawOrder[]{
//                CombinedChart.DrawOrder.BAR,
//                CombinedChart.DrawOrder.BAR,
//                CombinedChart.DrawOrder.BAR
//        });
//
//        CombinedData combinedData = new CombinedData();
//        combinedData.setData(generateBarData(red, 256, 0));
//        combinedData.setData(generateBarData(green, 256, 1));
//        combinedData.setData(generateBarData(blue, 256, 2));
//
//        mChart.setVisibility(View.VISIBLE);
//        mChart.setData(combinedData);
//
//        mChart.getDescription().setEnabled(false);
//        mChart.getXAxis().setDrawGridLines(false);
//
//        mChart.getAxisLeft().setDrawLabels(false);
//        mChart.getAxisRight().setDrawLabels(false);
//        mChart.getXAxis().setDrawLabels(false);
//        mChart.getLegend().setEnabled(false);
//
//        mChart.invalidate();
//    }

    public void makeHistogram(int[] arr, int length, int idHistogram){
        ArrayList<BarEntry> barEntries = new ArrayList<>();
        ArrayList<String> xAxis = new ArrayList<>();

        for (int i=0; i<length; i++){
            barEntries.add(new BarEntry(i, arr[i]));
            xAxis.add(""+i);
        }
        BarDataSet barDataSet = new BarDataSet(barEntries, "");

        if (idHistogram == R.id.histogram_red){
            barDataSet.setLabel("red");
            barDataSet.setColor(Color.rgb(255, 0,0));
        }
        else if (idHistogram == R.id.histogram_green){
            barDataSet.setLabel("green");
            barDataSet.setColor(Color.rgb(0, 255,0));
        }
        else if (idHistogram == R.id.histogram_blue){
            barDataSet.setLabel("blue");
            barDataSet.setColor(Color.rgb(0, 0,255));
        }

        BarData barData = new BarData(barDataSet);

        BarChart barChart = (BarChart) findViewById(idHistogram);
        barChart.setVisibility(View.VISIBLE);
        barChart.setData(barData);

        barChart.getDescription().setEnabled(false);
        barChart.getXAxis().setDrawGridLines(false);

        barChart.getAxisLeft().setDrawLabels(false);
        barChart.getAxisRight().setDrawLabels(false);
        barChart.getXAxis().setDrawLabels(false);
        barChart.getLegend().setEnabled(true);

        barChart.setTouchEnabled(true);
        barChart.setDragEnabled(true);
        barChart.setScaleEnabled(true);
        //        barChart.getAxisLeft().setDrawGridLines(false);
        //        barChart.getAxisRight().setDrawGridLines(false);
    }

//    private File createImageFile() throws IOException {
//        // Create Image file name
//        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
//        String imageFileName = "JPEG_" + timeStamp;
//        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
//        File image = File.createTempFile(
//                imageFileName,
//                ".jpg",
//                storageDir
//        );
//
//        mCurrentPhotoPath = image.getAbsolutePath();
//        return image;
//    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu){
//        getMenuInflater().inflate();
//    }
}
