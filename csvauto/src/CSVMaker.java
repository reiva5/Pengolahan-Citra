import javafx.util.Pair;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class CSVMaker {
    final static double threshold = 0.10;
    final static int BLACK = 1;
    final static int WHITE = 0;
    static BufferedImage img;
    static BufferedImage binary_picture;
    static int width, height;
    static int[][] binaryImage;
    static int[][] image;
    static int jumlah_area = 0;
    static int[] typeChainCode = new int[8];
    final static int[] dirX = {0, 1, 1, 1, 0, -1, -1, -1};
    final static int[] dirY = {-1, -1, 0, 1, 1, 1, 0, -1};
    static ThinnedImage thinnedImage;
    static ThinnedFeature thinnedFeature;
    static String filePath = "D:\\tugas\\sem7\\pengcit\\";
    static File csvFile = new File(filePath + "dataset.csv");
    static PrintWriter csvFileWriter;
    static String FILE_HEADER = "ascii," +
            "edge," +
            "interception," +
            "edge_atas," +
            "edge_bawah," +
            "edge_kiri," +
            "edge_kanan," +
            "interception_atas," +
            "interception_bawah," +
            "interception_kiri," +
            "interception_kanan," +
            "edge_kiri_atas," +
            "edge_kanan_atas," +
            "edge_kiri_bawah," +
            "edge_kanan_bawah," +
            "interception_kiri_atas," +
            "interception_kanan_atas," +
            "interception_kiri_bawah," +
            "interception_kanan_bawah," +
            "perbandingan_jarak," +
            "jumlah_area," +
//            "jumlah_kiri," +
//            "jumlah_kanan," +
//            "jumlah_atas," +
//            "jumlah_bawah," +
            "type_1," +
            "type_2," +
            "type_3," +
            "type_4," +
            "type_5," +
            "type_6," +
            "type_7," +
            "type_8";
    public static void main(String[] args) throws IOException {
        csvFileWriter = new PrintWriter(csvFile);
        csvFileWriter.println(FILE_HEADER);
        String[] folderNames = new String[]{"ascii_baru", "ascii_tangan_eki", "ascii_tangan_mius"};
        for (int i = 33; i < 127; ++i) {
            for (String folderName : folderNames) {
                for (int j = 0; j < 8; ++j) {
                    typeChainCode[j] = 0;
                }
                img = openFromFile(folderName, i, folderName.equals("ascii_baru") ? "JPG" : "PNG");
                binary_picture = makeBinaryImage(img);
                zhangSuen();
                thinnedImage = new ThinnedImage();
                thinnedFeature = thinnedImage.countEdgeInterception(image, jumlah_area, typeChainCode);
                csvFileWriter.println(i + "," + thinnedFeature.toString());
                csvFileWriter.flush();
            }
        }
    }

    static BufferedImage openFromFile(String folder, int ascii_number, String file_type){
        String file_path = "D:\\tugas\\sem7\\pengcit\\" + folder + "\\" + ascii_number + "." + file_type;
        Image picture = null;
        try {
            picture = ImageIO.read(new File(file_path));
        } catch (IOException e){
            e.printStackTrace();
        }
        return (BufferedImage) picture;
    }

    static int binaryColor(int img_rgb){
        Color color = new Color(img_rgb);
        if (color.getRed() + color.getBlue() + color.getGreen() < 127*3){
            return BLACK;
        } else {
            return WHITE;
        }
    }

    static BufferedImage makeBinaryImage(BufferedImage picture){
        BufferedImage result = new BufferedImage(picture.getWidth(), picture.getHeight(), BufferedImage.TYPE_3BYTE_BGR);
        for (int y = 0; y < result.getHeight(); ++y){
            for (int x = 0; x < result.getWidth(); ++x){
                if (binaryColor(picture.getRGB(x, y)) == BLACK){
                    result.setRGB(x, y, new Color(0, 0, 0).getRGB());
                } else {
                    result.setRGB(x, y, new Color(255, 255, 255).getRGB());
                }
            }
        }
        return result;
    }

    static void getAnotherBinaryImage() {
        width = img.getWidth();
        height = img.getHeight();
        binaryImage = new int[height][width];
        for (int x = 0; x < width; ++x) {
            for (int y = 0; y < height; ++y) {
                binaryImage[y][x] = binaryColor(img.getRGB(x, y));
            }
        }
    }

    static void zhangSuen() {
        getAnotherBinaryImage();
        image = binaryImage;

        List<Point> changing1, changing2;
        changing1 = new ArrayList<>();
        changing2 = new ArrayList<>();
        changing1.add(new Point(0,0));
        changing2.add(new Point(0,0));
        while (!changing1.isEmpty() || !changing2.isEmpty()) {
            // Step 1
            changing1.clear();
            for (int y = 1; y < image.length-1; ++y) {
                for (int x = 1; x < image[0].length-1; ++x) {
                    int[] P = neighbours(x,y,image);
                    if (image[y][x] == 1 &&
                            2 <= getSum(P) && getSum(P) <= 6 &&
                            transitions(P) == BLACK &&
                            P[0] * P[2] * P[4] == WHITE &&
                            P[2] * P[4] * P[6] == WHITE) {
                        changing1.add(new Point(x,y));
                    }
                }
            }
            for (Point p : changing1) {
                image[p.y][p.x] = WHITE;
            }
            // Step 2;
            changing2.clear();
            for (int y = 1; y < image.length-1; ++y) {
                for (int x = 1; x < image[0].length-1; ++x) {
                    int[] P = neighbours(x,y,image);
                    if (image[y][x] == 1 &&
                            2 <= getSum(P) && getSum(P) <= 6 &&
                            transitions(P) == BLACK &&
                            P[0] * P[2] * P[6] == WHITE &&
                            P[0] * P[4] * P[6] == WHITE) {
                        changing2.add(new Point(x,y));
                    }
                }
            }
            for (Point p : changing2) {
                image[p.y][p.x] = WHITE;
            }
        }
        deleteSmallCorner();
        bfsDeleteBranch();
        printGamber(image);
    }

    static void printGamber(int[][] image) {
        for (int i = 0; i < image.length; ++i) {
            for (int j = 0; j < image[i].length; ++j) {
                System.err.print(image[i][j]);
            }
            System.err.println();
        }
    }

    static int[] neighbours(int x, int y, int[][] image) {
        int x_1 = x-1;
        int y_1 = y-1;
        int x1 = x+1;
        int y1 = y+1;
        return new int[] {image[y][x_1], image[y1][x_1], image[y1][x], image[y1][x1],
                image[y][x1], image[y_1][x1], image[y_1][x], image[y_1][x_1]};
    }

    static int transitions(int[] neighbours) {
        int[] tmp = new int[neighbours.length+1];
        for (int i = 0; i < neighbours.length; ++i) {
            tmp[i] = neighbours[i];
        }
        tmp[neighbours.length] = tmp[0];
        int count = 0;
        for (int i = 0; i < neighbours.length; ++i) {
            count += tmp[i] == WHITE && tmp[i+1] == BLACK ? 1 : 0;
        }
        return count;
    }

    static int getSum(int[] neighbours) {
        int res = 0;
        for (int data : neighbours) {
            res += data;
        }
        return res;
    }

    static void deleteSmallCorner() {
        int [][] b1 = {{0, 0, -1}, {0, 1, 1}, {-1, 1, 0}};
        int [][] b2 = {{0, 0, 0}, {0, 1, 1}, {1, 1, 0}};
        int [][] b3 = {{-1, 0, 0}, {1, 1, 0}, {0, 1, -1}};
        int [][] b4 = {{1, 0, 0}, {1, 1, 0}, {0, 1, 0}};
        int [][] b5 = {{1, 1, 0}, {0, 1, 1}, {0, 0, 0}};

        boolean bool_b1, bool_b2, bool_b3, bool_b4, bool_b5;

        for (int y = 1; y < image.length-1; ++y){
            for (int x = 1; x < image[0].length-1; ++x){
                int [][] matrix_3 = {{image[y-1][x-1], image[y-1][x], image[y-1][x+1]},
                        {image[y][x-1], image[y][x], image[y][x+1]},
                        {image[y+1][x-1], image[y+1][x], image[y+1][x+1]}};
                bool_b1 = checkMatrix(matrix_3, b1);
                bool_b2 = checkMatrix(matrix_3, b2);
                bool_b3 = checkMatrix(matrix_3, b3);
                bool_b4 = checkMatrix(matrix_3, b4);
                bool_b5 = checkMatrix(matrix_3, b5);
                if (bool_b1  || bool_b2 || bool_b3 || bool_b4 || bool_b5){
                    image[y][x] = WHITE;
                }
            }
        }
    }

    static boolean checkMatrix(int[][] matrix_asal, int[][] pembanding){
        for (int i = 0; i < 3; i++){
            for (int j = 0; j < 3; j++){
                if (pembanding[i][j] == BLACK && matrix_asal[i][j] == WHITE){
                    return false;
                } else if (pembanding[i][j] == WHITE && matrix_asal[i][j] == BLACK){
                    return false;
                }
            }
        }
        return true;
    }

    static void bfsDeleteBranch() {
        Boolean[][] visited = new Boolean[image.length][image[0].length];
        for (int y = 0; y < image.length; ++y) {
            for (int x = 0; x < image[0].length; ++x) {
                visited[y][x] = false;
            }
        }
        jumlah_area = 0;
        for (int y = 0; y < image.length; ++y) {
            for (int x = 0; x < image[0].length; ++x) {
                if (!visited[y][x] && image[y][x] == BLACK) {
                    bfs(new Point(x,y), visited);
                    ++jumlah_area;
                }
            }
        }
    }

    static void bfs(Point P, Boolean[][] visited) {
        ArrayList<Point> nodeUjung = new ArrayList<>();
        Queue<Point> queue = new LinkedList<>();
        ArrayList<Point> semiChainCode = new ArrayList<>();
        queue.add(P);
        while (!queue.isEmpty()) {
            Point tmp = queue.poll();
            if (!visited[tmp.y][tmp.x]) {
                int branch = 0;
                semiChainCode.add(tmp);
                visited[tmp.y][tmp.x] = true;
                for (int i = 0; i < dirX.length; ++i) {
                    Point nx = new Point(tmp.x + dirX[i], tmp.y + dirY[i]);
                    if (nx.x >= 0 && nx.y >= 0
                            && nx.x < visited[0].length && nx.y < visited.length
                            && image[nx.y][nx.x] == BLACK) {
                        ++branch;
                        if (!visited[nx.y][nx.x]) {
                            typeChainCode[i]++;
                            queue.add(nx);
                        }
                    }
                }
                if (branch == 1) {
                    nodeUjung.add(tmp);
                }
            }
        }
        for (Point point : semiChainCode) {
            visited[point.y][point.x] = false;
        }
        if (nodeUjung.size() > 0) {
            System.err.println("Ujungnya apa aja: " + nodeUjung.toString());
        }
        Integer count = 0;
        for (Point point : nodeUjung) {
            ArrayList<Integer> arah = new ArrayList<>();
            ArrayList<Point> candidateDeleted = new ArrayList<>();
            Queue<Pair<Point,Integer>> q = new LinkedList<>();
            q.add(new Pair<>(point, 0));
            while (!q.isEmpty()) {
                Pair<Point,Integer> junk = q.poll();
                Point tmp = junk.getKey();
                int u = junk.getValue();
                if (!visited[tmp.y][tmp.x]) {
                    Boolean valid = false;
                    visited[tmp.y][tmp.x] = true;
                    ArrayList<Integer> pattern = new ArrayList<>();
                    for (int i = 0; i < dirX.length; ++i) {
                        Point nx = new Point(tmp.x + dirX[(i+u)%8], tmp.y + dirY[(i+u)%8]);
                        if (nx.x >= 0 && nx.y >= 0 && nx.x < visited[0].length && nx.y < visited.length
                                && image[nx.y][nx.x] != WHITE) {
                            pattern.add(1);
                        } else {
                            pattern.add(0);
                        }
                    }
                    pattern.add(pattern.get(0));
                    for (int i = 0; i < 8; ++i) {
                        for (int j = i+2; j < (i == 0 ? 7 : 8); ++j) {
                            for (int k = j+2; k < (i == 0 ? 7 : 8); ++k) {
                                valid = valid || (pattern.get(i) == 1 && pattern.get(j) == 1 && pattern.get(k) == 1);
                            }
                        }
                    }
                    if (valid) {
                        q.add(new Pair<>(tmp, 0));
                        System.err.println("Pattern " + pattern.toString());
                        System.err.println("Deleted yang: " + tmp.x + " " + tmp.y);
                        break;
                    }
                    for (int i = 0; i < dirX.length; ++i) {
                        Point nx = new Point(tmp.x + dirX[(i+u)%8], tmp.y + dirY[(i+u)%8]);
                        if (nx.x >= 0 && nx.y >= 0 && nx.x < visited[0].length && nx.y < visited.length
                                && image[nx.y][nx.x] != WHITE) {
                            if (!visited[nx.y][nx.x]) {
                                q.add(new Pair<>(nx, (i+u)%8));
                                arah.add((i+u)%8);
                                candidateDeleted.add(nx);
                            }
                        }
                    }
                }
            }
            System.err.println("Arah: " + arah.toString());
            if ((double) candidateDeleted.size()/semiChainCode.size() < threshold) {
                System.err.println("Candidate deleted: " + candidateDeleted.toString());
                int curr = 0;
                for (Point p : candidateDeleted) {
                    image[p.y][p.x] = WHITE;
                    ++curr;
                }
                image[point.y][point.x] = WHITE;
                while (!q.isEmpty()) {
                    Point canc = q.poll().getKey();
                    image[canc.y][canc.x] = BLACK;
                }
            }
            for (Point p : candidateDeleted) {
                visited[p.y][p.x] = false;
            }
            visited[point.y][point.x] = false;
        }
        for (Point p : semiChainCode) {
            visited[p.y][p.x] = true;
        }
    }

}
