class ThinnedImage {
    public int countNeighbours(int x, int y, int[][] image) {
        int x_1 = x-1;
        int y_1 = y-1;
        int x1 = x+1;
        int y1 = y+1;
        int one = 0;
        if (image[y][x_1] != 0) one ++;
        if (image[y1][x_1] != 0) one ++;
        if (image[y1][x] != 0) one ++;
        if (image[y1][x1] != 0) one ++;
        if (image[y][x1] != 0) one ++;
        if (image[y_1][x1] != 0) one ++;
        if (image[y_1][x] != 0) one ++;
        if (image[y_1][x_1] != 0) one ++;
        return one;
    }

    public int tetanggaInterception(int x, int y, int[][] image) {
        int tetangga = countNeighbours(x, y, image);


        int bukan_tetangga = image[y-1][x]*image[y-1][x+1] + image[y-1][x+1]*image[y][x+1] + image[y][x+1]*image[y+1][x+1]
                + image[y+1][x+1]*image[y+1][x] + image[y+1][x]*image[y+1][x-1] + image[y+1][x-1]*image[y][x-1]
                + image[y][x-1]*image[y-1][x-1] + image[y-1][x-1]*image[y-1][x];
        return tetangga-bukan_tetangga;
    }


    public void draw(int[][] image) {
        int i = 0;
        for (int[] anImage : image) {
            StringBuilder dumm = new StringBuilder();
            for (int x = 0; x < image[0].length; ++x) {
                dumm.append(anImage[x]);
            }
            i++;
            if (i > 9) i = 0;
            System.err.println(i + ": " + dumm.toString());
        }

    }

    public ThinnedFeature countEdgeInterception (int[][] image, int jumlah_area, int[] typeChainCode) {
        int edge = 0;
        int interception = 0;
        int edge_atas = 0;
        int edge_bawah = 0;
        int edge_kiri = 0;
        int edge_kanan = 0;
        int edge_kiri_atas = 0;
        int edge_kanan_atas = 0;
        int edge_kiri_bawah =  0;
        int edge_kanan_bawah = 0;

        int interception_atas = 0;
        int interception_bawah = 0;
        int interception_kiri = 0;
        int interception_kanan = 0;
        int interception_kiri_atas = 0;
        int interception_kanan_atas = 0;
        int interception_kiri_bawah =  0;
        int interception_kanan_bawah = 0;

        float jumlah_atas = 0;
        float jumlah_bawah = 0;
        float jumlah_kiri = 0;
        float jumlah_kanan = 0;

        float total = 0;
        int y_atas, y_bawah, jarak_y_bawah;
        int jarak_y_atas = 0;
        boolean atas_checked = false;

        int setengah_y = image.length / 2;
        int setengah_x = image[0].length / 2;
        float perbandingan_jarak = 0;

        for (int y = 1; y < image.length - 1; ++y) {
            for (int x = 1; x < image[0].length - 1; ++x) {
                if (image[y][x] == 1) {
                    if (x < setengah_x) {
                        ++jumlah_kiri;
                    } else {
                        ++jumlah_kanan;
                    }

                    if (y < setengah_y) {
                        ++jumlah_atas;
                    } else {
                        ++jumlah_bawah;
                    }
                    ++total;

                    if(!atas_checked){
                        atas_checked = true;
                        y_atas = y;
                        jarak_y_atas = y_atas;
                    }
                    y_bawah = y;

                    int tetangga = tetanggaInterception(x, y, image);
                    if (tetangga == 1) {
                        System.err.println("LEBERO NEIGHBOURS: " + x + " - " + y +" : " + tetangga + "");
                        edge++;
                        if(x<setengah_x){
                            edge_kiri++;
                            if(y<setengah_y){
                                edge_kiri_atas++;
                            } else {
                                edge_kiri_bawah++;
                            }
                        } else {
                            edge_kanan++;
                            if(y<setengah_y){
                                edge_kanan_atas++;
                            } else {
                                edge_kanan_bawah++;
                            }
                        }
                        if(y<setengah_y){
                            edge_atas++;
                        } else{
                            edge_bawah++;
                        }
                    } else if (tetangga > 2){
//                        System.err.println("inteception LEBERO", x + " - " + y + " : " + tetangga);
                        image[y][x] = 3;
                        interception++;
                        if(x<setengah_x){
                            interception_kiri++;
                            if(y<setengah_y){
                                interception_kiri_atas++;
                            } else{
                                interception_kiri_bawah++;
                            }
                        } else {
                            interception_kanan++;
                            if(y<setengah_y){
                                interception_kanan_atas++;
                            } else{
                                interception_kanan_bawah++;
                            }
                        }
                        if(y<setengah_y){
                            interception_atas++;
                        } else{
                            interception_bawah++;
                        }
                    }

                    jarak_y_bawah = image.length-y_bawah;
                    perbandingan_jarak = (float)jarak_y_atas/jarak_y_bawah;
                }
            }
        }

        draw(image);
        return new ThinnedFeature(edge, interception, edge_atas,edge_bawah,edge_kiri,edge_kanan,
                interception_atas,interception_bawah,interception_kiri,interception_kanan,edge_kiri_atas,
                edge_kanan_atas,edge_kiri_bawah,edge_kanan_bawah,interception_kiri_atas,interception_kanan_atas,
                interception_kiri_bawah,interception_kanan_bawah,perbandingan_jarak, jumlah_area, jumlah_kiri/total,
                jumlah_kanan/total, jumlah_atas/total, jumlah_bawah/total, typeChainCode);
    }

    public static ThinnedImage getInstance() {
        return new ThinnedImage();
    }
}
