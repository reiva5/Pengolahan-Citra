package com.example.pengcit02;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

public class MainActivity extends AppCompatActivity {
    static final int REQUEST_TAKE_PHOTO = 1;
    private static final int CAMERA_REQUEST = 1888;
    public static float koefAlgTwo = (float) 0.01;
    private int PICK_IMAGE_REQUEST = 1;
    private ImageView imageView, result;
    private int width, height;
    private int[] red = new int[256];
    private int[] green = new int[256];
    private int[] blue = new int[256];
    private Bitmap img;
    private static final int MY_CAMERA_PERMISSION_CODE = 100;
    private String mCurrentPhotoPath;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button mButton = (Button) findViewById(R.id.button_get_image);
        Button mButton2 = (Button) findViewById(R.id.button_take_image);
        imageView = (ImageView) findViewById(R.id.img_orig);
        result = (ImageView) findViewById(R.id.img_new);
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent photoIntent = new Intent();
                photoIntent.setType("image/*");
                photoIntent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(photoIntent, "Select Picture"),
                        PICK_IMAGE_REQUEST);
            }
        });
        mButton2.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (ContextCompat.checkSelfPermission(v.getContext(), Manifest.permission.CAMERA)
                                != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions((Activity) v.getContext(), new String[]{Manifest.permission.CAMERA},
                                    MY_CAMERA_PERMISSION_CODE);
                        } else {
                            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            File photoFile = null;
                            try {
                                photoFile = createImageFile();
                            } catch (IOException ex) {
                                ex.printStackTrace();
                            }
                            if (photoFile != null) {
                                Uri photoURI = FileProvider.getUriForFile(v.getContext(),
                                        "com.example.pengcit02.fileprovider",
                                        photoFile);
                                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                                startActivityForResult(cameraIntent, REQUEST_TAKE_PHOTO);
                            }

                            startActivityForResult(cameraIntent, CAMERA_REQUEST);
                        }
                    }
                }
        );

//        Inisialisasi nol semua array
        for (int i = 0; i < 256; ++i) {
            red[i] = 0;
            green[i] = 0;
            blue[i] = 0;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_CAMERA_PERMISSION_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "camera permission granted", Toast.LENGTH_LONG).show();
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                File photoFile = null;
                try {
                    photoFile = createImageFile();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                if (photoFile != null) {
                    Uri photoURI = FileProvider.getUriForFile(this,
                            "com.example.pengcit02.fileprovider",
                            photoFile);
                    cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                    startActivityForResult(cameraIntent, REQUEST_TAKE_PHOTO);
                }

                startActivityForResult(cameraIntent, CAMERA_REQUEST);
            } else {
                Toast.makeText(this, "camera permission denied", Toast.LENGTH_LONG).show();
            }

        }

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null &&
            data.getData() != null) {
            Uri uri = data.getData();

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                getBitmapPixel(bitmap);
                imageView.setImageBitmap(bitmap);
                img = bitmap;
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
            assert data != null;
            setPic();
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }
    private void setPic() {
        // Get the dimensions of the View
        int targetW = imageView.getWidth();
        int targetH = imageView.getHeight();

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW/targetW, photoH/targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        getBitmapPixel(bitmap);
        imageView.setImageBitmap(bitmap);
        img = bitmap;
    }

    /**
     * Mendapatkan array of pixel dari gambar yang dipilih
     * @param bitmap Image
     */
    public void getBitmapPixel(Bitmap bitmap) {
        width = bitmap.getWidth();
        height = bitmap.getHeight();
        int pixel;
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                pixel = bitmap.getPixel(i, j);
                red[Color.red(pixel)]++;
                green[Color.green(pixel)]++;
                blue[Color.blue(pixel)]++;
            }
        }
    }
    public void getCumulative(long[] arr) {
//        Log.d("Pengali", String.valueOf(pengali));
        for (int i = 0; i < 256; ++i) {
            arr[i] = (long) (255 * arr[i]) / (arr[255] - arr[0]);
        }
    }
    public int mapNewPixel(int pixel, long[] newRed, long[] newGreen, long[] newBlue) {
        int dummRed = (int) newRed[Color.red(pixel)];
        int dummGreen = (int) newGreen[Color.green(pixel)];
        int dummBlue = (int) newBlue[Color.blue(pixel)];

        return Color.rgb(dummRed, dummGreen, dummBlue);
    }
    /**
     * 1. Transform anggota array ke i = total nilai dari indeks ke 0 hingga ke i
     */
    public void algOne(View v) {
        Bitmap test = img.copy(img.getConfig(), true);
        long[] newRed = new long[256];
        long[] newGreen = new long[256];
        long[] newBlue = new long[256];

        newRed[0] = red[0];
        newGreen[0] = green[0];
        newBlue[0] = blue[0];

        for (int i = 1; i < 256; ++i) {
            newRed[i] = red[i] + newRed[i - 1];
            newGreen[i] = green[i] + newGreen[i - 1];
            newBlue[i] = blue[i] + newBlue[i - 1];
        }

        getCumulative(newRed);
        getCumulative(newGreen);
        getCumulative(newBlue);

        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                int pixel = test.getPixel(i,j);

                test.setPixel(i, j, mapNewPixel(pixel, newRed, newGreen, newBlue));
//                red[Color.red(pixel)]++;
//                green[Color.green(pixel)]++;
//                blue[Color.blue(pixel)]++;
            }
        }
        result.setImageBitmap(test);
    }
    public int getMinAlgTwo(int[] arr, float batas) {
        int iterator = 0;
        boolean end = false;
        while (iterator < 256 && !end) {
            if (arr[iterator] >= batas) {
                end = true;
            } else {
                iterator++;
            }
        }
        return iterator;
    }
    public int getMaxAlgTwo(int[] arr, float batas) {
        int iterator = 255;
        boolean end = false;
        while (iterator >= 0 && !end) {
            if (arr[iterator] >= batas) {
                end = true;
            } else {
                iterator--;
            }
        }
        return iterator;
    }
    public void algTwo (View v) {
        Bitmap tempImage = img.copy(img.getConfig(), true);
        int[] newRed = red;
        int[] newGreen = green;
        int[] newBlue = blue;

        double discard_ratio = 0.01;
        int[][] hists = new int[3][256];

        hists[0] = newRed;
        hists[1] = newGreen;
        hists[2] = newBlue;

        int total = tempImage.getWidth() * tempImage.getHeight();

        int[] vmin = new int[3];
        int[] vmax = new int[3];

        for (int i = 0; i < 3; ++i) {
            for (int j = 0; j < 255; ++j) {
                hists[i][j + 1] += hists[i][j];
            }
            vmin[i] = 0;
            vmax[i] = 255;
            while(hists[i][vmin[i]] < discard_ratio * total) {
                vmin[i] += 1;
            }
            while (hists[i][vmax[i]] > (1 - discard_ratio) * total) {
                vmax[i] -= 1;
            }
            if (vmax[i] < 255 - 1) {
                vmax[i] += 1;
            }
        }

        for (int y = 0; y < width; ++y) {
            for (int x = 0; x < height; ++x) {
                int[] rgbVal = new int[3];

                for (int j = 0; j < 3; ++j) {
                    int val = 0;
                    int pixel = tempImage.getPixel(y, x);
                    int Rval = Color.red(pixel);
                    int Gval = Color.green(pixel);
                    int Bval = Color.green(pixel);

                    if (j == 0) val = Rval;
                    if (j == 1) val = Gval;
                    if (j == 2) val = Bval;

                    if (val < vmin[j]) val = vmin[j];
                    if (val > vmax[j]) val = vmax[j];

                    rgbVal[j] = (int)((val - vmin[j]) * 255.0 / (vmax[j] - vmin[j]));
                }

                tempImage.setPixel(y,x, Color.rgb(rgbVal[0], rgbVal[1], rgbVal[2]));
            }
        }
        result.setImageBitmap(tempImage);
    }
}
