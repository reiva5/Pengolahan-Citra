package com.example.pengcit05;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

public class MainActivity extends AppCompatActivity {
    static final int PICK_IMAGE_REQUEST = 2;
    private static final int CAMERA_REQUEST = 1888;

    private Bitmap img;
    private String mCurrentPhotoPath;
    ImageView mImageView, resultView;

    static int[] dirX = {0, 1, 1, 1, 0, -1 ,-1 ,-1};
    static int[] dirY = {-1, -1, 0, 1, 1, 1, 0, -1};

    static List<Point> toWhite = new ArrayList<>();
    static char[][] grid;

    int width, height;
    int[][] binaryImage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mImageView = (ImageView) findViewById(R.id.image_chaincode);
        resultView = findViewById(R.id.result);
        getPhotoFromFile();
        dispatchTakePictureIntent();
        Button mButton = findViewById(R.id.button_thinning);
        mButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        zhangSuen();
                    }
                }
        );
    }

    private int getA(int[][] binaryImage, int y, int x) {
        int count = 0;
//p2 p3
        if (y - 1 >= 0 && x + 1 < binaryImage[y].length && binaryImage[y - 1][x] == 0 && binaryImage[y - 1][x + 1] == 1) {
            count++;
        }
//p3 p4
        if (y - 1 >= 0 && x + 1 < binaryImage[y].length && binaryImage[y - 1][x + 1] == 0 && binaryImage[y][x + 1] == 1) {
            count++;
        }
//p4 p5
        if (y + 1 < binaryImage.length && x + 1 < binaryImage[y].length && binaryImage[y][x + 1] == 0 && binaryImage[y + 1][x + 1] == 1) {
            count++;
        }
//p5 p6
        if (y + 1 < binaryImage.length && x + 1 < binaryImage[y].length && binaryImage[y + 1][x + 1] == 0 && binaryImage[y + 1][x] == 1) {
            count++;
        }
//p6 p7
        if (y + 1 < binaryImage.length && x - 1 >= 0 && binaryImage[y + 1][x] == 0 && binaryImage[y + 1][x - 1] == 1) {
            count++;
        }
//p7 p8
        if (y + 1 < binaryImage.length && x - 1 >= 0 && binaryImage[y + 1][x - 1] == 0 && binaryImage[y][x - 1] == 1) {
            count++;
        }
//p8 p9
        if (y - 1 >= 0 && x - 1 >= 0 && binaryImage[y][x - 1] == 0 && binaryImage[y - 1][x - 1] == 1) {
            count++;
        }
//p9 p2
        if (y - 1 >= 0 && x - 1 >= 0 && binaryImage[y - 1][x - 1] == 0 && binaryImage[y - 1][x] == 1) {
            count++;
        }
        return count;
    }

    private int getB(int[][] binaryImage, int y, int x) {
        return binaryImage[y - 1][x] + binaryImage[y - 1][x + 1] + binaryImage[y][x + 1]
                + binaryImage[y + 1][x + 1] + binaryImage[y + 1][x] + binaryImage[y + 1][x - 1]
                + binaryImage[y][x - 1] + binaryImage[y - 1][x - 1];
    }
    public void doThinning() {
        getBinaryImage();
        int[][] image = binaryImage;
        int a,b;

        List<Point> pointsToChange = new LinkedList<>();
        boolean hasChange;
        Log.d("UKURAN", image.length + "");
        do {
            hasChange = false;
            for (int y = 1; y + 1 < image.length; y++) {
                for (int x = 1; x + 1 < image[y].length; x++) {
                    a = getA(image, y, x);
                    b = getB(image, y, x);
                    if (image[y][x] == 1 && 2 <= b && b <= 6 && a == 1 &&
                            (image[y-1][x] * image[y][x+1] * image[y+1][x] == 0) &&
                            (image[y][x+1] * image[y+1][x] * image[y][x-1] == 0)) {
                        pointsToChange.add(new Point(x, y));
                        hasChange = true;
                    }
                }

                for (Point point : pointsToChange) {
                    image[point.getY()][point.getX()] = 0;
                }

                pointsToChange.clear();

                for (y = 1; y + 1 < image.length; y++) {
                    for (int x = 1; x + 1 < image[y].length; x++) {
                        a = getA(image, y, x);
                        b = getB(image, y, x);
                        if (image[y][x] == 1 && 2 <= b && b <= 6 && a == 1
                                && (image[y - 1][x] * image[y][x + 1] * image[y][x - 1] == 0)
                                && (image[y - 1][x] * image[y + 1][x] * image[y][x - 1] == 0)) {
                            pointsToChange.add(new Point(x, y));
                            hasChange = true;
                        }
                    }
                }
                for (Point point : pointsToChange) {
                    image[point.getY()][point.getX()] = 0;
                }
                pointsToChange.clear();
            }
        } while (hasChange);

        updateNewImage(image);
    }
    void updateNewImage(int[][] newPixel) {
        Bitmap cloned = img.copy(img.getConfig(), true);

        for (int i = 0; i < width; ++i) {
            for (int j = 0; j < height; ++j) {
                if (newPixel[i][j] == 0) cloned.setPixel(i, j, Color.WHITE);
                else cloned.setPixel(i, j, Color.BLACK);
            }
        }
        resultView.setImageBitmap(cloned);
    }
    void getBinaryImage() {
        width = img.getWidth();
        height = img.getHeight();
        binaryImage = new int[width][height];
        for (int i = 0; i < width; ++i) {
            for (int j = 0; j < height; ++j) {
                int pixel = img.getPixel(i, j);
                int gray = (Color.red(pixel) + Color.green(pixel) + Color.blue(pixel)) / 3;
                if (gray < 127) binaryImage[i][j] = 1;
                else binaryImage[i][j] = 0;
            }
        }
    }
    /* ******* Bagian dapatkan image, jangan ubah disini biar gammpang ngopynya ********/
    private void setPic() {
        // Get the dimensions of the View
        int targetW = 300;
        int targetH = 300;

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW/targetW, photoH/targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        mImageView.setImageBitmap(bitmap);
        img = bitmap;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK){
//            Bundle extras = data.getExtras();
            assert data != null;
            setPic();
        } else if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK){
            Uri uri = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                mImageView.setImageBitmap(bitmap);
                img = bitmap;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    private void getPhotoFromFile(){
        Button getPhotoFromFileButton = findViewById(R.id.button_get_files);
        getPhotoFromFileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent photoIntent = new Intent();
                photoIntent.setType("image/*");
                photoIntent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(photoIntent, "Select Picture"),
                        PICK_IMAGE_REQUEST);
            }
        });
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        @SuppressLint("SimpleDateFormat") String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void dispatchTakePictureIntent(){
        Button takePictureButton = findViewById(R.id.button_get_camera);
        takePictureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                File photoFile = null;
                try {
                    photoFile = createImageFile();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                if (photoFile != null) {
                    Uri photoURI = FileProvider.getUriForFile(v.getContext(),
                            "com.example.pengcit05",
                            photoFile);
                    cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                    startActivityForResult(cameraIntent, CAMERA_REQUEST);
                }
            }
        });
    }
    /* ******************************************************************************* */

    private int[] neighbours(int x, int y, int[][] image) {
        int x_1 = x-1;
        int y_1 = y-1;
        int x1 = x+1;
        int y1 = y+1;
        return new int[] {image[y][x_1], image[y1][x_1], image[y1][x], image[y1][x1], // P2, P3, P4, P5
        image[y][x1], image[y_1][x1], image[y_1][x], image[y_1][x_1]}; // P6, P7, P8, P9
    }

    private int transitions(int[] neighbours) {
        int[] tmp = new int[neighbours.length+1];
        for (int i = 0; i < neighbours.length; ++i) {
            tmp[i] = neighbours[i];
        }
        tmp[neighbours.length] = tmp[0];
        int count = 0;
        for (int i = 0; i < neighbours.length; ++i) {
            count += tmp[i] == 0 && tmp[i+1] == 1 ? 1 : 0;
        }
        return count;
    }

    private int getSum(int[] neighbours) {
        int res = 0;
        for (int data : neighbours) {
            res += data;
        }
        return res;
    }

    void getAnotherBinaryImage() {
        width = img.getWidth();
        height = img.getHeight();
        binaryImage = new int[height][width];
        for (int x = 0; x < width; ++x) {
            for (int y = 0; y < height; ++y) {
                int pixel = img.getPixel(x, y);
                int gray = (Color.red(pixel) + Color.green(pixel) + Color.blue(pixel)) / 3;
                if (gray < 127) binaryImage[y][x] = 1;
                else binaryImage[y][x] = 0;
            }
        }
    }

    void updateNewImage2(int[][] newPixel) {
        Bitmap cloned = img.copy(img.getConfig(), true);

        for (int x = 0; x < width; ++x) {
            for (int y = 0; y < height; ++y) {
                if (newPixel[y][x] == 0) cloned.setPixel(x, y, Color.WHITE);
                else cloned.setPixel(x, y, Color.BLACK);
            }
        }
        resultView.setImageBitmap(cloned);
    }

    private void dfsUtil(int dir,
                         int x, int y,
                         int[][] image,
                         int parentX, int parentY,
                         Boolean[][] visit,
                         ArrayList<Integer> chainCode,
                         Boolean[] isCyclic) {
        visit[y][x] = true;
        for (int i = 0; i < dirX.length; ++i) {
            int nxX, nxY;
            nxX = x + dirX[(i+dir) % dirX.length];
            nxY = y + dirY[(i+dir) % dirY.length];
            if (nxX >= 0 && nxX < image[0].length &&
                    nxY >= 0 && nxY < image.length
                    ) {
                if (image[nxY][nxX] == 1) {
                    if (!visit[nxY][nxX]) {
                        chainCode.add((i + dir) % dirX.length);
                        dfsUtil((i + dir + dirX.length / 2) % dirX.length,
                                nxX, nxY,
                                image,
                                x, y,
                                visit,
                                chainCode,
                                isCyclic
                        );
                    } else {
                        if ((nxX != parentX || nxY != parentY) && (i % 2 != 0)) {
                            isCyclic[0] = true;
                        }
                    }
                }
            }
        }
    }

    private void getChainCode(int[][] image,
                              ArrayList<ArrayList<Integer>> result,
                              ArrayList<Boolean> datCyc) {
        Boolean[][] visit = new Boolean[image.length][image[0].length];
        for (int y = 0; y < image.length; ++y) {
            for (int x = 0; x < image[0].length; ++x) {
                visit[y][x] = false;
            }
        }
        for (int y = 0; y < image.length; ++y) {
            for (int x = 0; x < image[0].length; ++x) {
                if (image[y][x] == 1 && !visit[y][x]) {
                    ArrayList<Integer> chainCode = new ArrayList<>();
                    Boolean[] isCyclic = {false};
                    dfsUtil(0, x, y, image, -1, -1, visit, chainCode, isCyclic);
                    result.add(chainCode);
                    datCyc.add(isCyclic[0]);
                }
            }
        }
    }

    private void predict(ArrayList<ArrayList<Integer>> chainCodes, ArrayList<Integer> prediction) {
        for (ArrayList<Integer> chainCode : chainCodes) {
            float[] dir = new float[8];
            for (int i = 0; i < 8; ++i) dir[i] = 0;
            for (Integer code : chainCode) {
                ++dir[code];
            }
            ArrayList<Pair<Float, Float>> dat = new ArrayList<>();
            for (int i = 0; i < 8; ++i) {
                dat.add(new Pair<>(dir[i], (float) i));
            }
            for (int i = 0; i < 8; ++i) {
                for (int j = 0; j < 7; ++j) {
                    if (dat.get(j).first < dat.get(j+1).first) {
                        Pair<Float, Float> tmp = dat.get(j);
                        dat.set(j, dat.get(j+1));
                        dat.set(j+1, tmp);
                    }
                }
            }
            if (dat.get(0).first/dat.get(1).first >= 4.5) {
                prediction.add(1);
            } else if (dat.get(0).first/dat.get(1).first <= 1.5) {
                if (dat.get(0).first/dat.get(1).first >= 1 && dat.get(1).first/dat.get(2).first >= 1
                        && dat.get(1).first/dat.get(2).first <= 1.3
                        && dat.get(2).first/dat.get(3).first >= 1 && dat.get(2).first/dat.get(3).first <= 1.3) {
                    prediction.add(8);
                } else if (dat.get(0).first/dat.get(1).first >= 1 && dat.get(1).first/dat.get(2).first >= 1
                        && dat.get(2).first/dat.get(3).first > 1.3 && dat.get(2).first/dat.get(3).first <= 3) {
                    prediction.add(3);
                } else if (Math.abs(dat.get(0).first/dat.get(3).first - 6) <= 1) {
                    prediction.add(2);
                } else if (dat.get(0).first/dat.get(1).first >= 1 && dat.get(1).first/dat.get(2).first >= 1
                        && dat.get(1).first/dat.get(2).first > 1.3) {
                    prediction.add(5);
                } else {
                    prediction.add(7);
                }
            } else if (dat.get(0).first/dat.get(1).first <= 2.5) {
                prediction.add(4);
            } else if (dat.get(0).first/dat.get(1).first <= 1.111 && dat.get(1).first / dat.get(2).first <= 3.111){
                prediction.add(0);
            }
            Log.d("Pola", dat.toString());
        }
    }

    private void zhangSuen() {
        getAnotherBinaryImage();
        int[][] image = binaryImage;

        List<Point> changing1, changing2;
        changing1 = new ArrayList<>();
        changing2 = new ArrayList<>();
        changing1.add(new Point(0,0));
        changing2.add(new Point(0,0));
        for (int y = 1; y < image.length-1; ++y) {
            for (int x = 1; x < image[0].length-1; ++x) {
                System.err.print(image[y][x]);
            }
            System.err.println();
        }
        while (!changing1.isEmpty() || !changing2.isEmpty()) {
            // Step 1
            changing1.clear();
            for (int y = 1; y < image.length-1; ++y) {
                for (int x = 1; x < image[0].length-1; ++x) {
                    int[] P = neighbours(x,y,image);
                    if (image[y][x] == 1 &&
                            2 <= getSum(P) && getSum(P) <= 6 &&
                            transitions(P) == 1 &&
                            P[0] * P[2] * P[4] == 0 &&
                            P[2] * P[4] * P[6] == 0) {
                        changing1.add(new Point(x,y));
                    }
                }
            }
            for (Point p : changing1) {
                image[p.y][p.x] = 0;
            }
            // Step 2;
            changing2.clear();
            for (int y = 1; y < image.length-1; ++y) {
                for (int x = 1; x < image[0].length-1; ++x) {
                    int[] P = neighbours(x,y,image);
                    if (image[y][x] == 1 &&
                            2 <= getSum(P) && getSum(P) <= 6 &&
                            transitions(P) == 1 &&
                            P[0] * P[2] * P[6] == 0 &&
                            P[0] * P[4] * P[6] == 0) {
                        changing2.add(new Point(x,y));
                    }
                }
            }
            for (Point p : changing2) {
                image[p.y][p.x] = 0;
            }
        }
        updateNewImage2(image);
        ArrayList<ArrayList<Integer>> chainCode = new ArrayList<>();
        ArrayList<Boolean> dataCyc = new ArrayList<>();
        getChainCode(image, chainCode, dataCyc);
        ArrayList<Integer> prediction = new ArrayList<>();
        Log.d("Chaincode", chainCode.toString());
        Log.d("Cyclic", dataCyc.toString());
        predict(chainCode, prediction);
        Log.d("prediksi", prediction.toString());
    }
}