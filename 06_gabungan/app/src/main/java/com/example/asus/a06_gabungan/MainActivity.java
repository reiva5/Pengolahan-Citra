package com.example.asus.a06_gabungan;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Stack;

public class MainActivity extends AppCompatActivity {
    static final int PICK_IMAGE_REQUEST = 2;

    private static final int CAMERA_REQUEST = 1888;

    public static Bitmap img;
    public static byte[] byteArray;
    private String mCurrentPhotoPath;
    ImageView mImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getPicture();

        Button histogramButton = (Button) findViewById(R.id.button_histogram);
        histogramButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), HistogramActivity.class);
                i.putExtra("image", byteArray);
                startActivity(i);
            }
        });

        Button contrastButton = (Button) findViewById(R.id.button_contrast);
        contrastButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), ContrastActivity.class);
                i.putExtra("image", byteArray);
                startActivity(i);
            }
        });

        Button specificationButton = (Button) findViewById(R.id.button_specification);
        specificationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), SpecificationActivity.class);
                i.putExtra("image", byteArray);
                startActivity(i);
            }
        });

        Button chainCodeButton = (Button) findViewById(R.id.button_chaincode);
        chainCodeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), ChainCodeActivity.class);
                i.putExtra("image", byteArray);
                startActivity(i);
            }
        });

        Button thinningButton = (Button) findViewById(R.id.button_thinning);
        thinningButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), ThinningActivity.class);
                i.putExtra("image", byteArray);
                startActivity(i);
            }
        });

        Button filterButton = (Button) findViewById(R.id.button_filter_detection);
        filterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), FilterDetectionActivity.class);
                i.putExtra("image", byteArray);
                startActivity(i);
            }
        });

        Button faceButton = (Button) findViewById(R.id.button_face_detection);
        faceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), FaceDetectionActivity.class);
                i.putExtra("image", byteArray);
                startActivity(i);
            }
        });
    }

    private void convertBitmaptoByteArray(Bitmap img){
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        if(img.getHeight()*img.getWidth() > 720*480 ){
            img = Bitmap.createScaledBitmap(img, img.getWidth()*720*480/img.getWidth()/img.getHeight(),
                    img.getHeight()*720*480/img.getWidth()/img.getHeight(), true);
//            img = Bitmap.createScaledBitmap(img, img.getWidth()/5,
//                    img.getHeight()/5, true);
        }

        img.compress(Bitmap.CompressFormat.PNG, 100, stream);
        Log.d("UKURAN", "width : "+img.getWidth());
        Log.d("UKURAN", "height : "+img.getHeight());
        byteArray = stream.toByteArray();
    }

    private void getPicture(){
        getPhotoFromFile();
        dispatchTakePictureIntent();
    }

    private void getPhotoFromFile(){
        Button getPhotoFromFileButton = findViewById(R.id.button_get_files);

        getPhotoFromFileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent photoIntent = new Intent();
                photoIntent.setType("image/*");
                photoIntent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(photoIntent, "Select Picture"),
                        PICK_IMAGE_REQUEST);
            }
        });
    }

    private void dispatchTakePictureIntent(){
        Button takePictureButton = findViewById(R.id.button_get_camera);
        takePictureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                File photoFile = null;
                try {
                    photoFile = createImageFile();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                if (photoFile != null) {
                    Uri photoURI = FileProvider.getUriForFile(v.getContext(),
                            "com.example.asus.a06_gabungan.fileprovider",
                            photoFile);
                    cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                    startActivityForResult(cameraIntent, CAMERA_REQUEST);
                }
            }
        });
    }

    private void setPic() {
        // Get the dimensions of the View
        int targetW = 600;
        int targetH = 800;

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW/targetW, photoH/targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        mImageView.setImageBitmap(bitmap);
        img = bitmap;
        convertBitmaptoByteArray(img);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        mImageView = (ImageView) findViewById(R.id.image);
        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK){
//            Bundle extras = data.getExtras();
            assert data != null;
            setPic();
        } else if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK){
            Uri uri = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                mImageView.setImageBitmap(bitmap);
                img = bitmap;
                convertBitmaptoByteArray(img);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    private File createImageFile() throws IOException {
        // Create an image file name
        @SuppressLint("SimpleDateFormat") String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }
}
