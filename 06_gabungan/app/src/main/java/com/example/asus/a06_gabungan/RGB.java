package com.example.asus.a06_gabungan;

import android.graphics.Color;

public class RGB {
    private int R;
    private int G;
    private int B;
    public RGB(int pixel) {
        this.R = Color.red(pixel);
        this.G = Color.green(pixel);
        this.B = Color.blue(pixel);
    }

    public int getR() {
        return R;
    }

    public int getG() {
        return G;
    }

    public int getB() {
        return B;
    }
}
