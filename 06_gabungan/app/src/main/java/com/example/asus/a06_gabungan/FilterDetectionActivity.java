package com.example.asus.a06_gabungan;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.Image;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.asus.a06_gabungan.filterdetection.Custom;
import com.example.asus.a06_gabungan.filterdetection.Filter;
import com.example.asus.a06_gabungan.filterdetection.FreiChen;
import com.example.asus.a06_gabungan.filterdetection.Prewit;
import com.example.asus.a06_gabungan.filterdetection.Roberts;
import com.example.asus.a06_gabungan.filterdetection.Sobel;

import java.util.Arrays;

import static java.lang.Math.abs;
import static java.lang.Math.sqrt;

public class FilterDetectionActivity extends AppCompatActivity {
    ImageView mImageView, result;
    Bitmap img;
    int[][] Gx = new int[3][];
    int[][] Gy = new int[3][];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_detection);

        setImage();

        result = (ImageView) findViewById(R.id.img_filter);
        Button filterButton = (Button) findViewById(R.id.button_filter);
        filterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                result.setImageBitmap(new Filter(img).makeMedianImage());
            }
        });
        Button kurangSeberang = findViewById(R.id.button_kurang_seberang);
        kurangSeberang.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        result.setImageBitmap(new Filter(img).neighbourKurangSebrang());
                    }
                }
        );
        Button kurangTengah = findViewById(R.id.button_kurang_tengah);
        kurangTengah.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        result.setImageBitmap(new Filter(img).kurangTengah());
                    }
                }
        );
    }

    private void setImage(){
        mImageView = findViewById(R.id.image);
        byte[] byteArray = getIntent().getByteArrayExtra("image");
        img = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
        mImageView.setImageBitmap(img);
    }

    public void revealCustom(View v){
        Button button_ok_custom = (Button) findViewById(R.id.button_ok_custom);
        LinearLayout input_x_layout = (LinearLayout) findViewById(R.id.input_x_layout);
        LinearLayout input_y_layout = (LinearLayout) findViewById(R.id.input_y_layout);
        button_ok_custom.setVisibility(View.VISIBLE);
        input_x_layout.setVisibility(View.VISIBLE);
        input_y_layout.setVisibility(View.VISIBLE);

    }

    public void initializeMatrixCustom(){
        EditText edit_x_1, edit_x_2, edit_x_3, edit_x_4, edit_x_5, edit_x_6, edit_x_7, edit_x_8, edit_x_9;
        EditText edit_y_1, edit_y_2, edit_y_3, edit_y_4, edit_y_5, edit_y_6, edit_y_7, edit_y_8, edit_y_9;

        edit_x_1 = (EditText) findViewById(R.id.input_x_1);
        edit_x_2 = (EditText) findViewById(R.id.input_x_2);
        edit_x_3 = (EditText) findViewById(R.id.input_x_3);
        edit_x_4 = (EditText) findViewById(R.id.input_x_4);
        edit_x_5 = (EditText) findViewById(R.id.input_x_5);
        edit_x_6 = (EditText) findViewById(R.id.input_x_6);
        edit_x_7 = (EditText) findViewById(R.id.input_x_7);
        edit_x_8 = (EditText) findViewById(R.id.input_x_8);
        edit_x_9 = (EditText) findViewById(R.id.input_x_9);

        edit_y_1 = (EditText) findViewById(R.id.input_y_1);
        edit_y_2 = (EditText) findViewById(R.id.input_y_2);
        edit_y_3 = (EditText) findViewById(R.id.input_y_3);
        edit_y_4 = (EditText) findViewById(R.id.input_y_4);
        edit_y_5 = (EditText) findViewById(R.id.input_y_5);
        edit_y_6 = (EditText) findViewById(R.id.input_y_6);
        edit_y_7 = (EditText) findViewById(R.id.input_y_7);
        edit_y_8 = (EditText) findViewById(R.id.input_y_8);
        edit_y_9 = (EditText) findViewById(R.id.input_y_9);

        int int_x_1, int_x_2, int_x_3, int_x_4, int_x_5, int_x_6, int_x_7, int_x_8, int_x_9;
        int int_y_1, int_y_2, int_y_3, int_y_4, int_y_5, int_y_6, int_y_7, int_y_8, int_y_9;

        int_x_1 = Integer.parseInt(edit_x_1.getText().toString());
        int_x_2 = Integer.parseInt(edit_x_2.getText().toString());
        int_x_3 = Integer.parseInt(edit_x_3.getText().toString());
        int_x_4 = Integer.parseInt(edit_x_4.getText().toString());
        int_x_5 = Integer.parseInt(edit_x_5.getText().toString());
        int_x_6 = Integer.parseInt(edit_x_6.getText().toString());
        int_x_7 = Integer.parseInt(edit_x_7.getText().toString());
        int_x_8 = Integer.parseInt(edit_x_8.getText().toString());
        int_x_9 = Integer.parseInt(edit_x_9.getText().toString());

        int_y_1 = Integer.parseInt(edit_y_1.getText().toString());
        int_y_2 = Integer.parseInt(edit_y_2.getText().toString());
        int_y_3 = Integer.parseInt(edit_y_3.getText().toString());
        int_y_4 = Integer.parseInt(edit_y_4.getText().toString());
        int_y_5 = Integer.parseInt(edit_y_5.getText().toString());
        int_y_6 = Integer.parseInt(edit_y_6.getText().toString());
        int_y_7 = Integer.parseInt(edit_y_7.getText().toString());
        int_y_8 = Integer.parseInt(edit_y_8.getText().toString());
        int_y_9 = Integer.parseInt(edit_y_9.getText().toString());

        Gx[0] = new int[] {int_x_1, int_x_2, int_x_3};
        Gx[1] = new int[] {int_x_4, int_x_5, int_x_6};
        Gx[2] = new int[] {int_x_7, int_x_8, int_x_9};

        Gy[0] = new int[] {int_y_1, int_y_2, int_y_3};
        Gy[1] = new int[] {int_y_4, int_y_5, int_y_6};
        Gy[2] = new int[] {int_y_7, int_y_8, int_y_9};
    }


    public void operator(View v) {
        if (v.getId() == R.id.button_prewit) {
            result.setImageBitmap(new Prewit(img).getResult());
        } else if (v.getId() == R.id.button_sobel) {
            result.setImageBitmap(new Sobel(img).getResult());
        } else if (v.getId() == R.id.button_roberts) {
            result.setImageBitmap(new Roberts(img).getResult());
        } else if (v.getId() == R.id.button_frei_chen){
            result.setImageBitmap(new FreiChen(img).getResult());
        } else if (v.getId() == R.id.button_ok_custom){
            initializeMatrixCustom();
            result.setImageBitmap(new Custom(img, Gx, Gy).getResult());
        } else {
            result.setImageBitmap(new Sobel(img).getResult());
        }
    }
}
