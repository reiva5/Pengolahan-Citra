package com.example.asus.a06_gabungan.filterdetection;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.widget.EditText;

import static java.lang.Math.sqrt;

public class Custom extends Filter {
    final String type = "custom";
    private int[][] Gx_custom;
    private int[][] Gy_custom;
    public Custom(Bitmap img, int[][] Gx, int [][]Gy) {
        super(img);
        Gx_custom = Gx;
        Gy_custom = Gy;
    }

    public Bitmap getResult() {
        Bitmap dummy = img.copy(img.getConfig(), true);
        int w = dummy.getWidth();
        int h = dummy.getHeight();
        double[] clr = new double[3];
        for (int y = 1; y < h - 1; ++y) {
            for (int x = 1; x < w - 1; ++x) {
                int[][][] tetangga = tetanggaToMatrix(x, y, type);
                int[] kali_x_red = kaliKonvolusiMatrix(Gx_custom, tetangga);
                int[] kali_y_red = kaliKonvolusiMatrix(Gy_custom, tetangga);
                for (int i = 0; i < 3; ++i) {
                    clr[i] = sqrt((kali_x_red[i] * kali_x_red[i]) +
                            (kali_y_red[i] * kali_y_red[i]));
                    maxRGB[i] = (int) (threshold_sobel * maxRGB[i]);
                }
                dummy.setPixel(x, y, Color.rgb(max(clr[0], maxRGB[0]),
                        max(clr[1], maxRGB[1]), max(clr[2], maxRGB[2])));
            }
        }
        Bitmap result_image = dummy.copy(dummy.getConfig(), true);

        for(int y=0; y<dummy.getHeight(); y++){
            for(int x=0; x<dummy.getWidth(); x++){
                int pixel = dummy.getPixel(x, y);
                int gray = (Color.red(pixel) + Color.green(pixel) + Color.blue(pixel)) / 3;
                if (gray < 80){
                    result_image.setPixel(x, y, Color.rgb(0, 0, 0));
                }
                else{
                    result_image.setPixel(x, y, Color.rgb(255, 255, 255));
                }
            }
        }

        return result_image;
    }
}
