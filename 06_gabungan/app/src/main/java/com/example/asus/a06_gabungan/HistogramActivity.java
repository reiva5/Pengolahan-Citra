package com.example.asus.a06_gabungan;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;

import java.util.ArrayList;

public class HistogramActivity extends AppCompatActivity {
    ImageView mImageView;
    Bitmap img;

    private int width, height;
    private int[] red = new int[256];
    private int[] green = new int[256];
    private int[] blue = new int[256];

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_histogram);
        setImage();
        setHistogram();
    }


    private void setImage(){
        mImageView = findViewById(R.id.image);
        byte[] byteArray = getIntent().getByteArrayExtra("image");
        img = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
        mImageView.setImageBitmap(img);
        getBitmapPixel(img);
    }

    private void setHistogram(){
        makeHistogram(red, 256, R.id.histogram_red);
        makeHistogram(green, 256, R.id.histogram_green);
        makeHistogram(blue, 256, R.id.histogram_blue);
    }

    public void getBitmapPixel(Bitmap bitmap) {
        width = bitmap.getWidth();
        height = bitmap.getHeight();
        int pixel;
        for (int i = 0; i < 256; i++) {
            red[i] = 0;
            green[i] = 0;
            blue[i] = 0;
        }

        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                pixel = bitmap.getPixel(i, j);
                red[Color.red(pixel)]++;
                green[Color.green(pixel)]++;
                blue[Color.blue(pixel)]++;
            }
        }
    }

    public void makeHistogram(int[] arr, int length, int idHistogram){
        ArrayList<BarEntry> barEntries = new ArrayList<>();
        ArrayList<String> xAxis = new ArrayList<>();

        for (int i=0; i<length; i++){
            barEntries.add(new BarEntry(i, arr[i]));
            xAxis.add(""+i);
        }
        BarDataSet barDataSet = new BarDataSet(barEntries, "");

        if (idHistogram == R.id.histogram_red){
            barDataSet.setLabel("red");
            barDataSet.setColor(Color.rgb(255, 0,0));
        }
        else if (idHistogram == R.id.histogram_green){
            barDataSet.setLabel("green");
            barDataSet.setColor(Color.rgb(0, 255,0));
        }
        else if (idHistogram == R.id.histogram_blue){
            barDataSet.setLabel("blue");
            barDataSet.setColor(Color.rgb(0, 0,255));
        }

        BarData barData = new BarData(barDataSet);

        BarChart barChart = (BarChart) findViewById(idHistogram);
        barChart.setVisibility(View.VISIBLE);
        barChart.setData(barData);

        barChart.getDescription().setEnabled(false);
        barChart.getXAxis().setDrawGridLines(false);

        barChart.getAxisLeft().setDrawLabels(false);
        barChart.getAxisRight().setDrawLabels(false);
        barChart.getXAxis().setDrawLabels(false);
        barChart.getLegend().setEnabled(true);

        barChart.setTouchEnabled(true);
        barChart.setDragEnabled(true);
        barChart.setScaleEnabled(true);
    }
}
