package com.example.asus.a06_gabungan;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Stack;

public class ThinningActivity extends AppCompatActivity {
    ImageView mImageView, resultView;
    Bitmap img;

    public static int jumlah_area = 0;

    private final static int BLACK = 1;
    private final static int WHITE = 0;

    final static int[] dirX = {0, 1, 1, 1, 0, -1, -1, -1};
    final static int[] dirY = {-1, -1, 0, 1, 1, 1, 0, -1};

    final static int[][] nbrs = {{0, -1}, {1, -1}, {1, 0}, {1, 1}, {0, 1},
            {-1, 1}, {-1, 0}, {-1, -1}, {0, -1}};

    final static int[][][] nbrGroups = {{{0, 2, 4}, {2, 4, 6}}, {{0, 2, 6},
            {0, 4, 6}}};

    static List<Point> toWhite = new ArrayList<>();
    static char[][] grid;

    int width, height;
    int[][] binaryImage;
    int[][] image;

    static int[] typeChainCode = new int[8];

    public ThinnedImage thinnedImage;
    public ThinnedFeature thinnedFeature;
    public TextView predict_text_view, detail_text_view;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thinning);
        setImage();

        resultView = findViewById(R.id.result);
        Button mButton = findViewById(R.id.button_thinning);
        Button predict = findViewById(R.id.button_predict);
        mButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        zhangSuen();
                        thinnedImage = new ThinnedImage();
                        thinnedFeature = thinnedImage.countEdgeInterception(image, jumlah_area, typeChainCode);
//                        Log.d("thinnedFeature", "DAFTAR FITUR");
//                        Log.d("thinnedFeature", "Edge : "+thinnedFeature.edge);
//                        Log.d("thinnedFeature", "Interception : "+thinnedFeature.interception);
//                        Log.d("thinnedFeature", "Edge Atas : "+thinnedFeature.edge_atas);
//                        Log.d("thinnedFeature", "Edge Bawah : "+thinnedFeature.edge_bawah);
//                        Log.d("thinnedFeature", "Edge Kiri : "+thinnedFeature.edge_kiri);
//                        Log.d("thinnedFeature", "Edge Kanan : "+thinnedFeature.edge_kanan);
//                        Log.d("thinnedFeature", "Interception Atas : "+thinnedFeature.interception_atas);
//                        Log.d("thinnedFeature", "Interception Bawah : "+thinnedFeature.interception_bawah);
//                        Log.d("thinnedFeature", "Interception Kiri : "+thinnedFeature.interception_kiri);
//                        Log.d("thinnedFeature", "Interception Kanan : "+thinnedFeature.interception_kanan);
//
//                        Log.d("thinnedFeature", "Edge Kiri Atas : "+thinnedFeature.edge_kiri_atas);
//                        Log.d("thinnedFeature", "Edge Kanan Atas : "+thinnedFeature.edge_kanan_atas);
//                        Log.d("thinnedFeature", "Edge Kiri Bawah : "+thinnedFeature.edge_kiri_bawah);
//                        Log.d("thinnedFeature", "Edge Kanan Bawah : "+thinnedFeature.edge_kanan_bawah);
//                        Log.d("thinnedFeature", "Interception Kiri Atas : "+thinnedFeature.interception_kiri_atas);
//                        Log.d("thinnedFeature", "Interception Kanan Atas : "+thinnedFeature.interception_kanan_atas);
//                        Log.d("thinnedFeature", "Interception Kiri Bawah : "+thinnedFeature.interception_kiri_bawah);
//                        Log.d("thinnedFeature", "Interception Kanan Bawah : "+thinnedFeature.interception_kanan_bawah);
//
//                        Log.d("thinnedFeature", "Perbandingan Jarak : "+thinnedFeature.perbandingan_jarak);
//
//                        Log.d("thinnedFeature", "Jumlah Area : " + thinnedFeature.jumlah_area);
//                        Log.d("thinnedFeature", "Jumlah Kiri : " + thinnedFeature.jumlah_kiri);
//                        Log.d("thinnedFeature", "Jumlah Kanan : " + thinnedFeature.jumlah_kanan);
//                        Log.d("thinnedFeature", "Jumlah Atas : " + thinnedFeature.jumlah_atas);
//                        Log.d("thinnedFeature", "Jumlah Bawah : " + thinnedFeature.jumlah_bawah);
                    }
                }
        );


        predict.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
//                        Log.d("LEBERO", ThinnedImage.getInstance().countEdgeInterception(image, jumlah_area).toString());
                        zhangSuen();
//   ThinnedImage.getInstance().draw(image);
                        thinnedImage = new ThinnedImage();
                        thinnedFeature = thinnedImage.countEdgeInterception(image, jumlah_area, typeChainCode);
                        predictAscii(thinnedFeature);
                    }
                }
        );
    }

    private void setImage(){
        mImageView = findViewById(R.id.image);
        byte[] byteArray = getIntent().getByteArrayExtra("image");
        img = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
        mImageView.setImageBitmap(img);
    }

    private int getA(int[][] binaryImage, int y, int x) {
        int count = 0;
//p2 p3
        if (y - 1 >= 0 && x + 1 < binaryImage[y].length && binaryImage[y - 1][x] == 0 && binaryImage[y - 1][x + 1] == 1) {
            count++;
        }
//p3 p4
        if (y - 1 >= 0 && x + 1 < binaryImage[y].length && binaryImage[y - 1][x + 1] == 0 && binaryImage[y][x + 1] == 1) {
            count++;
        }
//p4 p5
        if (y + 1 < binaryImage.length && x + 1 < binaryImage[y].length && binaryImage[y][x + 1] == 0 && binaryImage[y + 1][x + 1] == 1) {
            count++;
        }
//p5 p6
        if (y + 1 < binaryImage.length && x + 1 < binaryImage[y].length && binaryImage[y + 1][x + 1] == 0 && binaryImage[y + 1][x] == 1) {
            count++;
        }
//p6 p7
        if (y + 1 < binaryImage.length && x - 1 >= 0 && binaryImage[y + 1][x] == 0 && binaryImage[y + 1][x - 1] == 1) {
            count++;
        }
//p7 p8
        if (y + 1 < binaryImage.length && x - 1 >= 0 && binaryImage[y + 1][x - 1] == 0 && binaryImage[y][x - 1] == 1) {
            count++;
        }
//p8 p9
        if (y - 1 >= 0 && x - 1 >= 0 && binaryImage[y][x - 1] == 0 && binaryImage[y - 1][x - 1] == 1) {
            count++;
        }
//p9 p2
        if (y - 1 >= 0 && x - 1 >= 0 && binaryImage[y - 1][x - 1] == 0 && binaryImage[y - 1][x] == 1) {
            count++;
        }
        return count;
    }

    private int getB(int[][] binaryImage, int y, int x) {
        return binaryImage[y - 1][x] + binaryImage[y - 1][x + 1] + binaryImage[y][x + 1]
                + binaryImage[y + 1][x + 1] + binaryImage[y + 1][x] + binaryImage[y + 1][x - 1]
                + binaryImage[y][x - 1] + binaryImage[y - 1][x - 1];
    }

    public void doThinning() {
        getBinaryImage();
        int[][] image = binaryImage;
        int a,b;

        List<Point> pointsToChange = new LinkedList<>();
        boolean hasChange;
        Log.d("UKURAN", image.length + "");
        do {
            hasChange = false;
            for (int y = 1; y + 1 < image.length; y++) {
                for (int x = 1; x + 1 < image[y].length; x++) {
                    a = getA(image, y, x);
                    b = getB(image, y, x);
                    if (image[y][x] == 1 && 2 <= b && b <= 6 && a == 1 &&
                            (image[y-1][x] * image[y][x+1] * image[y+1][x] == 0) &&
                            (image[y][x+1] * image[y+1][x] * image[y][x-1] == 0)) {
                        pointsToChange.add(new Point(x, y));
                        hasChange = true;
                    }
                }

                for (Point point : pointsToChange) {
                    image[point.getY()][point.getX()] = 0;
                }

                pointsToChange.clear();

                for (y = 1; y + 1 < image.length; y++) {
                    for (int x = 1; x + 1 < image[y].length; x++) {
                        a = getA(image, y, x);
                        b = getB(image, y, x);
                        if (image[y][x] == 1 && 2 <= b && b <= 6 && a == 1
                                && (image[y - 1][x] * image[y][x + 1] * image[y][x - 1] == 0)
                                && (image[y - 1][x] * image[y + 1][x] * image[y][x - 1] == 0)) {
                            pointsToChange.add(new Point(x, y));
                            hasChange = true;
                        }
                    }
                }
                for (Point point : pointsToChange) {
                    image[point.getY()][point.getX()] = 0;
                }
                pointsToChange.clear();
            }
        } while (hasChange);

        updateNewImage(image);
    }

    void updateNewImage(int[][] newPixel) {
        Bitmap cloned = img.copy(img.getConfig(), true);

        for (int i = 0; i < width; ++i) {
            for (int j = 0; j < height; ++j) {
                if (newPixel[i][j] == WHITE) cloned.setPixel(i, j, Color.WHITE);
                else cloned.setPixel(i, j, Color.BLACK);
            }
        }
        resultView.setImageBitmap(cloned);
    }
    void getBinaryImage() {
        width = img.getWidth();
        height = img.getHeight();
        binaryImage = new int[width][height];
        for (int i = 0; i < width; ++i) {
            for (int j = 0; j < height; ++j) {
                int pixel = img.getPixel(i, j);
                int gray = (Color.red(pixel) + Color.green(pixel) + Color.blue(pixel)) / 3;
                if (gray < 127) binaryImage[i][j] = BLACK;
                else binaryImage[i][j] = WHITE;
            }
        }
    }

    private int[] neighbours(int x, int y, int[][] image) {
        int x_1 = x-1;
        int y_1 = y-1;
        int x1 = x+1;
        int y1 = y+1;
        return new int[] {image[y][x_1], image[y1][x_1], image[y1][x], image[y1][x1], // P2, P3, P4, P5
                image[y][x1], image[y_1][x1], image[y_1][x], image[y_1][x_1]}; // P6, P7, P8, P9
    }

    private int transitions(int[] neighbours) {
        int[] tmp = new int[neighbours.length+1];
        for (int i = 0; i < neighbours.length; ++i) {
            tmp[i] = neighbours[i];
        }
        tmp[neighbours.length] = tmp[0];
        int count = 0;
        for (int i = 0; i < neighbours.length; ++i) {
            count += tmp[i] == 0 && tmp[i+1] == 1 ? 1 : 0;
        }
        return count;
    }

    private int getSum(int[] neighbours) {
        int res = 0;
        for (int data : neighbours) {
            res += data;
        }
        return res;
    }

    void getAnotherBinaryImage() {
        width = img.getWidth();
        height = img.getHeight();
        binaryImage = new int[height][width];
        for (int x = 0; x < width; ++x) {
            for (int y = 0; y < height; ++y) {
                int pixel = img.getPixel(x, y);
                int gray = (Color.red(pixel) + Color.green(pixel) + Color.blue(pixel)) / 3;
                if (gray < 127) binaryImage[y][x] = BLACK;
                else binaryImage[y][x] = WHITE;
            }
        }
    }

    void updateNewImage2(int[][] newPixel) {
        Bitmap cloned = img.copy(img.getConfig(), true);

        for (int x = 0; x < width; ++x) {
            for (int y = 0; y < height; ++y) {
                if (newPixel[y][x] == 0) cloned.setPixel(x, y, Color.WHITE);
                else cloned.setPixel(x, y, Color.BLACK);
            }
        }
        resultView.setImageBitmap(cloned);
    }

    void deleteCorner(){
        int [][] b1 = {{0, 1, 0}, {1, 1, 0}, {-1, 0, 0}};
        int [][] b2 = {{-1, 1, -1}, {0, 1, 0}, {0, 0, 0}};
        int [][] b3 = {{0, 0, -1}, {0, 1, 1}, {0, 0, -1}};
        int [][] b4 = {{0, 0, 0}, {0, 1, 0}, {-1, 1, -1}};
        int [][] b5 = {{1, 0, 0}, {0, 1, 0}, {0, 0, 0}};
        int [][] b6 = {{0, 0, 1}, {0, 1, 0}, {0, 0, 0}};
        int [][] b7 = {{0, 0, 0}, {0, 1, 0}, {0, 0, 1}};
        int [][] b8 = {{0, 0, 0}, {0, 1, 0}, {1, 0, 0}};

        boolean bool_b1, bool_b2, bool_b3, bool_b4, bool_b5, bool_b6, bool_b7, bool_b8;
        int count_hapus = 0;
        int konstanta_ujung = (image.length/45);

        for(int y = 1; y < image.length-1; ++y){
            for(int x = 1; x < image[0].length-1; ++x){
                int [][] matriks_3 = {{image[y-1][x-1], image[y-1][x], image[y-1][x+1]},
                        {image[y][x-1], image[y][x], image[y][x+1]},
                        {image[y+1][x-1], image[y+1][x], image[y+1][x+1]}};
                bool_b1 = checkMatrix(matriks_3, b1);
                bool_b2 = checkMatrix(matriks_3, b2);
                bool_b3 = checkMatrix(matriks_3, b3);
                bool_b4 = checkMatrix(matriks_3, b4);
                bool_b5 = checkMatrix(matriks_3, b5);
                bool_b6 = checkMatrix(matriks_3, b6);
                bool_b7 = checkMatrix(matriks_3, b7);
                bool_b8 = checkMatrix(matriks_3, b8);
                if((count_hapus <= konstanta_ujung) && (bool_b1  || bool_b2 || bool_b3 || bool_b4 || bool_b5 || bool_b6 || bool_b7 || bool_b8)){
                    image[y][x] = 0;
                    count_hapus++;
                } else {
                    count_hapus = 0;
                }
            }
        }
    }

    void deleteSmallCorner() {
        int [][] b1 = {{0, 0, -1}, {0, 1, 1}, {-1, 1, 0}};
        int [][] b2 = {{0, 0, 0}, {0, 1, 1}, {1, 1, 0}};
        int [][] b3 = {{-1, 0, 0}, {1, 1, 0}, {0, 1, -1}};
        int [][] b4 = {{1, 0, 0}, {1, 1, 0}, {0, 1, 0}};
        int [][] b5 = {{1, 1, 0}, {0, 1, 1}, {0, 0, 0}};
//        int [][] b6 = {{0, 0, 0}, {1, 1, 0}, {0, 1, 1}};

        boolean bool_b1, bool_b2, bool_b3, bool_b4, bool_b5, bool_b6;
//        int count_hapus = 0;
//        int konstanta_ujung = 4 + (image.length/45);

        for(int y = 1; y < image.length-1; ++y){
            for(int x = 1; x < image[0].length-1; ++x){
                int [][] matriks_3 = {{image[y-1][x-1], image[y-1][x], image[y-1][x+1]},
                        {image[y][x-1], image[y][x], image[y][x+1]},
                        {image[y+1][x-1], image[y+1][x], image[y+1][x+1]}};
                bool_b1 = checkMatrix(matriks_3, b1);
                bool_b2 = checkMatrix(matriks_3, b2);
                bool_b3 = checkMatrix(matriks_3, b3);
                bool_b4 = checkMatrix(matriks_3, b4);
                bool_b5 = checkMatrix(matriks_3, b5);
//                bool_b6 = checkMatrix(matriks_3, b6);
                if(bool_b1  || bool_b2 || bool_b3 || bool_b4 || bool_b5){
                    image[y][x] = 0;
                }
            }
        }
    }

    boolean checkMatrix(int[][] matriks_asal, int[][] pembanding){
        for(int i=0; i<3; i++){
            for(int j=0; j<3; j++){
                if(pembanding[i][j] == 1 && matriks_asal[i][j] == 0){
                    return false;
                }
                else if(pembanding[i][j] == 0 && matriks_asal[i][j] == 1){
                    return false;
                }
            }
        }
        return true;
    }

    private void zhangSuen() {
        getAnotherBinaryImage();
        image = binaryImage;

        List<Point> changing1, changing2;
        changing1 = new ArrayList<>();
        changing2 = new ArrayList<>();
        changing1.add(new Point(0,0));
        changing2.add(new Point(0,0));
        while (!changing1.isEmpty() || !changing2.isEmpty()) {
            // Step 1
            changing1.clear();
            for (int y = 1; y < image.length-1; ++y) {
                for (int x = 1; x < image[0].length-1; ++x) {
                    int[] P = neighbours(x,y,image);
                    if (image[y][x] == 1 &&
                            2 <= getSum(P) && getSum(P) <= 6 &&
                            transitions(P) == 1 &&
                            P[0] * P[2] * P[4] == 0 &&
                            P[2] * P[4] * P[6] == 0) {
                        changing1.add(new Point(x,y));
                    }
                }
            }
            for (Point p : changing1) {
                image[p.y][p.x] = WHITE;
            }
            // Step 2;
            changing2.clear();
            for (int y = 1; y < image.length-1; ++y) {
                for (int x = 1; x < image[0].length-1; ++x) {
                    int[] P = neighbours(x,y,image);
                    if (image[y][x] == 1 &&
                            2 <= getSum(P) && getSum(P) <= 6 &&
                            transitions(P) == 1 &&
                            P[0] * P[2] * P[6] == 0 &&
                            P[0] * P[4] * P[6] == 0) {
                        changing2.add(new Point(x,y));
                    }
                }
            }
            for (Point p : changing2) {
                image[p.y][p.x] = WHITE;
            }
        }
//        deleteCorner();
        deleteSmallCorner();
        ThinnedImage.getInstance().draw(image);
        bfsDeleteBranch();
        ThinnedImage.getInstance().draw(image);
        updateNewImage2(image);
    }

    private void bfsDeleteBranch() {
        Boolean[][] visited = new Boolean[image.length][image[0].length];
        for (int y = 0; y < image.length; ++y) {
            for (int x = 0; x < image[0].length; ++x) {
                visited[y][x] = false;
            }
        }
        jumlah_area = 0;
        for (int y = 0; y < image.length; ++y) {
            for (int x = 0; x < image[0].length; ++x) {
                if (!visited[y][x] && image[y][x] == BLACK) {
                    bfs(new Point(x,y), visited);
                    ++jumlah_area;
                }
            }
        }
    }

    private void bfs(Point P, Boolean[][] visited) {
        ArrayList<Point> nodeUjung = new ArrayList<>();
        Queue<Point> queue = new LinkedList<>();
        ArrayList<Point> semiChainCode = new ArrayList<>();
        queue.add(P);
        while (!queue.isEmpty()) {
            Point tmp = queue.poll();
            if (!visited[tmp.y][tmp.x]) {
                int branch = 0;
                semiChainCode.add(tmp);
                visited[tmp.y][tmp.x] = true;
                for (int i = 0; i < dirX.length; ++i) {
                    Point nx = new Point(tmp.x + dirX[i], tmp.y + dirY[i]);
                    if (nx.x >= 0 && nx.y >= 0
                            && nx.x < visited[0].length && nx.y < visited.length
                            && image[nx.y][nx.x] == BLACK) {
                        ++branch;
                        if (!visited[nx.y][nx.x]) {
                            typeChainCode[i]++;
                            queue.add(nx);
                        }
                    }
                }
                if (branch == 1) {
                    nodeUjung.add(tmp);
                }
            }
        }
        for (Point point : semiChainCode) {
            visited[point.y][point.x] = false;
        }
        if (nodeUjung.size() > 0) {
            Log.d("Ujungnya apa aja", nodeUjung.toString());
        }
        Integer count = 0;
        for (Point point : nodeUjung) {
            ArrayList<Integer> arah = new ArrayList<>();
            ArrayList<Point> candidateDeleted = new ArrayList<>();
            Queue<Pair<Point,Integer>> q = new LinkedList<>();
            q.add(new Pair<>(point, 0));
            while (!q.isEmpty()) {
                Pair<Point,Integer> junk = q.poll();
                Point tmp = junk.first;
                int u = junk.second;
                if (!visited[tmp.y][tmp.x]) {
                    Boolean valid = false;
                    visited[tmp.y][tmp.x] = true;
                    ArrayList<Integer> pattern = new ArrayList<>();
                    for (int i = 0; i < dirX.length; ++i) {
                        Point nx = new Point(tmp.x + dirX[(i+u)%8], tmp.y + dirY[(i+u)%8]);
                        if (nx.x >= 0 && nx.y >= 0 && nx.x < visited[0].length && nx.y < visited.length
                                && image[nx.y][nx.x] != WHITE) {
                            pattern.add(1);
                        } else {
                            pattern.add(0);
                        }
                    }
                    pattern.add(pattern.get(0));
                    for (int i = 0; i < 8; ++i) {
                        for (int j = i+2; j < (i == 0 ? 7 : 8); ++j) {
                            for (int k = j+2; k < (i == 0 ? 7 : 8); ++k) {
                                valid = valid || (pattern.get(i) == 1 && pattern.get(j) == 1 && pattern.get(k) == 1);
                            }
                        }
                    }
                    if (valid) {
                        q.add(new Pair<>(tmp, 0));
                        Log.d("Pattern", pattern.toString());
                        Log.d("Deleted yang: ", tmp.x + " " + tmp.y);
                        break;
                    }
                    for (int i = 0; i < dirX.length; ++i) {
                        Point nx = new Point(tmp.x + dirX[(i+u)%8], tmp.y + dirY[(i+u)%8]);
                        if (nx.x >= 0 && nx.y >= 0 && nx.x < visited[0].length && nx.y < visited.length
                                && image[nx.y][nx.x] != WHITE) {
                            if (!visited[nx.y][nx.x]) {
                                q.add(new Pair<>(nx, (i+u)%8));
                                arah.add((i+u)%8);
                                candidateDeleted.add(nx);
                            }
                        }
                    }
                }
            }
            Log.d("Arah ", arah.toString());
            if ((double) candidateDeleted.size()/semiChainCode.size() < 0.05) {
                Log.d("Candidate deleted ", candidateDeleted.toString());
                int curr = 0;
                for (Point p : candidateDeleted) {
                    image[p.y][p.x] = WHITE;
                    ++curr;
                }
                image[point.y][point.x] = WHITE;
                while (!q.isEmpty()) {
                    Point canc = q.poll().first;
                    image[canc.y][canc.x] = BLACK;
                }
            }
            for (Point p : candidateDeleted) {
                visited[p.y][p.x] = false;
            }
            visited[point.y][point.x] = false;
        }
        for (Point p : semiChainCode) {
            visited[p.y][p.x] = true;
        }
    }

    private void dfs(ArrayList<Point> pointDeleted, Point P, Boolean[][] visited, int[][] image) {
        Stack<Point> stack = new Stack<>();
        stack.add(P);
        while (!stack.isEmpty()) {
            Point tmp = stack.pop();
            if (!visited[tmp.y][tmp.x]) {
                pointDeleted.add(tmp);
                image[tmp.y][tmp.x] = 3;
                visited[tmp.y][tmp.x] = true;
                for (int i = 0; i < dirX.length; ++i) {
                    Point nx = new Point(P.x + dirX[i], P.y + dirY[i]);
                    if (nx.x >= 0 && nx.y >= 0
                            && nx.x < visited[0].length && nx.y < visited.length
                            && image[nx.y][nx.x] == BLACK && !visited[nx.y][nx.x]) {
                        stack.push(nx);
                    }
                }
            }
        }
    }

    public int countNeighbours(int[][] image) {
//        {image[y][x_1], image[y1][x_1], image[y1][x], image[y1][x1], // P2, P3, P4, P5
//                image[y][x1], image[y_1][x1], image[y_1][x], image[y_1][x_1]};
        int count = 0;
        for(int i=0; i<3; i++){
            for(int j=0; j<3; j++){
                count += image[i][j];
            }
        }
        return count-1;
    }

    public boolean isEdge(int[][] image){
        return countNeighbours(image) == 1;
    }

    void predictAngka(ThinnedFeature thinnedFeature){
        predict_text_view = findViewById(R.id.predict_text_view);
        String prediction = "";
        if(thinnedFeature.edge == 4){
            if(thinnedFeature.interception == 4){
                prediction = "4";
            } else {
                prediction = "3";
            }
        }
        if(thinnedFeature.edge == 3){
            prediction = "9";
        }
        if(thinnedFeature.edge == 1){
            prediction = "6";
        }
        if(thinnedFeature.edge == 0){
            prediction = "0";
        }
        if(thinnedFeature.edge == 2 && thinnedFeature.interception == 4){
            prediction = "8";
        }
        if(thinnedFeature.edge == 2 && thinnedFeature.interception == 0){
            if(thinnedFeature.edge_kiri == 2){
                prediction = "7";
            }
            else if (thinnedFeature.perbandingan_jarak < 1){
                prediction = "2";
            }
            else if (thinnedFeature.perbandingan_jarak > 1.35){
                prediction = "1";
            }
            else {
                prediction = "5";
            }
        }
        predict_text_view.setText("Hasil Prediksi : " + prediction);
    }

    void predictAscii(ThinnedFeature thinnedFeature){
        predict_text_view = findViewById(R.id.predict_text_view);
        detail_text_view = findViewById(R.id.detail_text_view);
        int ascii = new RulesAscii().getPredict(thinnedFeature);
        String prediction = Character.toString((char) ascii);

        Log.d("Fitur", thinnedFeature.toString());
        predict_text_view.setText("Hasil Prediksi : " + prediction);
//        detail_text_view.setText(printDetail(thinnedFeature));
    }

    String printDetail(ThinnedFeature thinnedFeature){
        return "edge = " + thinnedFeature.edge + "\n" +
                "interception = " + thinnedFeature.interception + "\n" +
                "edge_atas = " + thinnedFeature.edge_atas + "\n" +
                "edge_bawah = " + thinnedFeature.edge_bawah + "\n" +
                "edge_kiri = " + thinnedFeature.edge_kiri + "\n" +
                "edge_kanan = " + thinnedFeature.edge_kanan + "\n" +
                "interception_atas = " + thinnedFeature.interception_atas + "\n" +
                "interception_bawah = " + thinnedFeature.interception_bawah + "\n" +
                "interception_kiri = " + thinnedFeature.interception_kiri + "\n" +
                "interception_kanan = " + thinnedFeature.interception_kanan + "\n" +
                "edge_kiri_atas = " + thinnedFeature.edge_kiri_atas + "\n" +
                "edge_kanan_atas = " + thinnedFeature.edge_kanan_atas + "\n" +
                "edge_kiri_bawah = " + thinnedFeature.edge_kiri_bawah + "\n" +
                "edge_kanan_bawah = " + thinnedFeature.edge_kanan_bawah + "\n" +
                "interception_kiri_atas = " + thinnedFeature.interception_kiri_atas + "\n" +
                "interception_kanan_atas = " + thinnedFeature.interception_kanan_atas + "\n" +
                "interception_kiri_bawah = " + thinnedFeature.interception_kiri_bawah + "\n" +
                "interception_kanan_bawah = " + thinnedFeature.interception_kanan_bawah + "\n" +
                "perbandingan_jarak = " + thinnedFeature.perbandingan_jarak + "\n" +
                "jumlah_area = " + thinnedFeature.jumlah_area + "\n" +
                "jumlah_kiri = " + thinnedFeature.jumlah_kiri + "\n" +
                "jumlah_kanan = " + thinnedFeature.jumlah_kanan + "\n" +
                "jumlah_atas = " + thinnedFeature.jumlah_atas + "\n" +
                "jumlah_bawah = " + thinnedFeature.jumlah_bawah + "\n";
    }

}