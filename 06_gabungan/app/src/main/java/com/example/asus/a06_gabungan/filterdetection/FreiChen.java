package com.example.asus.a06_gabungan.filterdetection;

import android.graphics.Bitmap;
import android.graphics.Color;

import static java.lang.Math.sqrt;

public class FreiChen extends Filter {
    private double[][][] G_Frein = new double[9][][];
    final String type = "frei-chen";

    private double[][] multiply(double cons, double[][] data) {
        double[][] tmp = new double[data.length][data[0].length];
        for (int i = 0; i < data.length; ++i) {
            for (int j = 0; j < data[0].length; ++j) {
                tmp[i][j] = cons * data[i][j];
            }
        }
        return tmp;
    }

    public FreiChen(Bitmap img) {
        super(img);
        G_Frein[0] = multiply(1/(2*sqrt(2)), new double[][]{{1, sqrt(2), 1}, {0, 0, 0}, {-1, -sqrt(2), -1}});
        G_Frein[1] = multiply(1/(2*sqrt(2)), new double[][]{{1, 0, -1}, {sqrt(2), 0, -sqrt(2)}, {1, 0, -1}});
        G_Frein[2] = multiply(1/(2*sqrt(2)), new double[][]{{0, -1, sqrt(2)}, {1, 0, -1}, {-sqrt(2), 1, 0}});
        G_Frein[3] = multiply(1/(2*sqrt(2)), new double[][]{{sqrt(2), -1, 0}, {-1, 0, 1}, {0, 1, -sqrt(2)}});
        G_Frein[4] = multiply(0.5, new double[][]{{0, 1, 0}, {-1, 0, -1}, {0, 1, 0}});
        G_Frein[5] = multiply(0.5, new double[][]{{-1, 0, 1}, {0, 0, 0}, {1, 0, -1}});
        G_Frein[6] = multiply(1/6, new double[][]{{1, -2, 1}, {-2, 4, -2}, {1, -2, 1}});
        G_Frein[7] = multiply(1/6, new double[][]{{-2, 1, -2}, {1, 4, 1}, {-2, 1, -2}});
        G_Frein[8] = multiply(1/3, new double[][]{{1, 1, 1}, {1, 1, 1}, {1, 1, 1}});
    }

    public Bitmap getResult() {
        Bitmap dummy = img.copy(img.getConfig(), true);
        int w = dummy.getWidth();
        int h = dummy.getHeight();
        double[] clr = new double[3];
        for (int y = 1; y < h - 1; ++y) {
            for (int x = 1; x < w - 1; ++x) {
                int[][][] tetangga = tetanggaToMatrix(x, y, type);
                double[][] kali_red = new double[9][];
                for (int i = 0; i < 9; ++i) {
                    kali_red[i] = kaliKonvolusiMatrix(G_Frein[i], tetangga);
                }
                for (int i = 0; i < 3; ++i) {
                    clr[i] = 0;
                    for (int j = 0; j < 9; ++j) {
                        clr[i] += kali_red[j][i]*kali_red[j][i];
                    }
                    clr[i] = sqrt(clr[i]);
                    maxRGB[i] = (int) (threshold_sobel * maxRGB[i]);
                }
                dummy.setPixel(x, y, Color.rgb(max(clr[0], maxRGB[0]),
                        max(clr[1], maxRGB[1]), max(clr[2], maxRGB[2])));
            }
        }
        Bitmap result_image = dummy.copy(dummy.getConfig(), true);

        for(int y=0; y<dummy.getHeight(); y++){
            for(int x=0; x<dummy.getWidth(); x++){
                int pixel = dummy.getPixel(x, y);
                int gray = (Color.red(pixel) + Color.green(pixel) + Color.blue(pixel)) / 3;
                if (gray < 80){
                    result_image.setPixel(x, y, Color.rgb(0, 0, 0));
                }
                else{
                    result_image.setPixel(x, y, Color.rgb(255, 255, 255));
                }
            }
        }

        return result_image;
    }
}
