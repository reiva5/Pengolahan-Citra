package com.example.asus.a06_gabungan;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Pair;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;
import com.example.asus.a06_gabungan.facerecognition.FaceRecognition2;

import com.example.asus.a06_gabungan.facerecognition.FaceRecognition;
import com.example.asus.a06_gabungan.facerecognition.Morphological;
import com.example.asus.a06_gabungan.filterdetection.*;

public class FaceDetectionActivity extends AppCompatActivity {
    ImageView mImageView, result;
    Bitmap img, processedImage;
    Button thicken;
    Morphological morphologicalOperator;
    Pair first;
    int flag = 0;

    public void selesai() {
        Toast.makeText(getApplicationContext(), "Beres gan!!", Toast.LENGTH_SHORT).show();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_face_detection);
        setImage();
        result = (ImageView) findViewById(R.id.img_filter);
        Button filterButton = (Button) findViewById(R.id.button_filter);
        filterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FaceRecognition fr = new FaceRecognition(img);
                result.setImageBitmap(fr.getResult());
            }
        });
        Button secondFilter = findViewById(R.id.button_second_filter);
        secondFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                process();
            }
        });
    }
    void process() {
        FaceRecognition2 instance = new FaceRecognition2();
//        if (flag == 0) {
            processedImage = instance.toBlackWhite(img);
            result.setImageBitmap(processedImage);
//            flag = 1;
//        } else if (flag == 1) {
            processedImage = instance.dilation(processedImage, 3);
            result.setImageBitmap(processedImage);
//            flag = 2;
//        } else if (flag == 2) {
            processedImage = instance.erosion(processedImage, 3);
            result.setImageBitmap(processedImage);
//            flag = 3;
//        } else if (flag == 3) {
            processedImage = instance.process(processedImage, img);
            result.setImageBitmap(processedImage);
//            flag = 4;
//        }
    }
    private Bitmap toGrayscale() {
        Bitmap newImage = img.copy(img.getConfig(), true);
        int width = img.getWidth();
        int height = img.getHeight();

        for (int y = 0; y < height; ++y) {
            for (int x = 0; x < width; ++x) {
                int pixel = img.getPixel(x, y);
                int grey = (int) ((Color.red(pixel) + Color.green(pixel) + Color.blue(pixel)) / 3);

                newImage.setPixel(x, y, Color.rgb(grey, grey, grey));
            }
        }

        return newImage;
    }

    private void setImage(){
        mImageView = findViewById(R.id.image);
        byte[] byteArray = getIntent().getByteArrayExtra("image");
        img = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
        mImageView.setImageBitmap(img);
    }

    void dot(Pair col, Pair row, Bitmap img) {
        int x = (int) col.first;
        int y = (int) col.second;

        img.setPixel(x - 1, y, Color.rgb(255, 0, 0));
        img.setPixel(x - 1, y + 1, Color.rgb(255, 0, 0));
        img.setPixel(x - 1, y - 1, Color.rgb(255, 0, 0));
        img.setPixel(x, y - 1, Color.rgb(255, 0, 0));
        img.setPixel(x, y, Color.rgb(255, 0, 0));
        img.setPixel(x, y + 1, Color.rgb(255, 0, 0));
        img.setPixel(x + 1, y - 1, Color.rgb(255, 0, 0));
        img.setPixel(x + 1, y, Color.rgb(255, 0, 0));
        img.setPixel(x + 1, y + 1, Color.rgb(255, 0, 0));

        x = (int) row.first;
        y = (int) row.second;

        img.setPixel(x - 1, y, Color.rgb(255, 0, 0));
        img.setPixel(x - 1, y + 1, Color.rgb(255, 0, 0));
        img.setPixel(x - 1, y - 1, Color.rgb(255, 0, 0));
        img.setPixel(x, y - 1, Color.rgb(255, 0, 0));
        img.setPixel(x, y, Color.rgb(255, 0, 0));
        img.setPixel(x, y + 1, Color.rgb(255, 0, 0));
        img.setPixel(x + 1, y - 1, Color.rgb(255, 0, 0));
        img.setPixel(x + 1, y, Color.rgb(255, 0, 0));
        img.setPixel(x + 1, y + 1, Color.rgb(255, 0, 0));
    }
    void setRetDot() {
        Pair x = (Pair) first.first;
        Pair y = (Pair) first.second;
        Bitmap newImage = processedImage.copy(processedImage.getConfig(), true);

        Toast.makeText(getApplicationContext(), "First Col : " + x.first
                + ", " + x.second + " - First Row : " + y.first
                + ", " + y.second , Toast.LENGTH_LONG).show();

        dot(x, y, newImage);

        result.setImageBitmap(newImage);
    }
}