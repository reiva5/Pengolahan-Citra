package com.example.asus.a06_gabungan;

public class ThinnedFeature {
    public int edge;
    public int interception;
    public int edge_atas;
    public int edge_bawah;
    public int edge_kiri;
    public int edge_kanan;
    public int interception_atas;
    public int interception_bawah;
    public int interception_kiri;
    public int interception_kanan;
    public int edge_kiri_atas;
    public int edge_kanan_atas;
    public int edge_kiri_bawah;
    public int edge_kanan_bawah;
    public int interception_kiri_atas;
    public int interception_kanan_atas;
    public int interception_kiri_bawah;
    public int interception_kanan_bawah;
    public float perbandingan_jarak;
    public int jumlah_area;
    public float jumlah_kiri;
    public float jumlah_kanan;
    public float jumlah_atas;
    public float jumlah_bawah;
    public double type_1;
    public double type_2;
    public double type_3;
    public double type_4;
    public double type_5;
    public double type_6;
    public double type_7;
    public double type_8;

    public ThinnedFeature(int edge, int interception,int edge_atas,int edge_bawah,int edge_kiri,int edge_kanan,
                          int interception_atas,int interception_bawah,int interception_kiri,int interception_kanan,
                          int edge_kiri_atas,int edge_kanan_atas,int edge_kiri_bawah,int edge_kanan_bawah,
                          int interception_kiri_atas,int interception_kanan_atas,int interception_kiri_bawah,
                          int interception_kanan_bawah,float perbandingan_jarak, int jumlah_area, float jumlah_kiri, float jumlah_kanan, float jumlah_atas, float jumlah_bawah,
                          int[] typeChainCode) {
        int total = 0;
        for (int type: typeChainCode) {
            total += type;
        }
        this.edge = edge;
        this.interception = interception;
        this.edge_atas = edge_atas;
        this.edge_bawah = edge_bawah;
        this.edge_kiri = edge_kiri;
        this.edge_kanan = edge_kanan;
        this.interception_atas = interception_atas;
        this.interception_bawah = interception_bawah;
        this.interception_kiri = interception_kiri;
        this.interception_kanan = interception_kanan;
        this.edge_kiri_atas = edge_kiri_atas;
        this.edge_kanan_atas = edge_kanan_atas;
        this.edge_kiri_bawah = edge_kiri_bawah;
        this.edge_kanan_bawah = edge_kanan_bawah;
        this. interception_kiri_atas = interception_kiri_atas;
        this.interception_kanan_atas = interception_kanan_atas;
        this.interception_kiri_bawah = interception_kiri_bawah;
        this.interception_kanan_bawah = interception_kanan_bawah;
        this.perbandingan_jarak = perbandingan_jarak;
        this.jumlah_area = jumlah_area;
        this.jumlah_kiri = jumlah_kiri;
        this.jumlah_kanan = jumlah_kanan;
        this.jumlah_atas = jumlah_atas;
        this.jumlah_bawah = jumlah_bawah;
        total = total == 0 ? 1 : total;
        this.type_1 = (double) typeChainCode[0]/total;
        this.type_2 = (double) typeChainCode[1]/total;
        this.type_3 = (double) typeChainCode[2]/total;
        this.type_4 = (double) typeChainCode[3]/total;
        this.type_5 = (double) typeChainCode[4]/total;
        this.type_6 = (double) typeChainCode[5]/total;
        this.type_7 = (double) typeChainCode[6]/total;
        this.type_8 = (double) typeChainCode[7]/total;
    }

    @Override
    public String toString() {
//        return "edge : " + edge + " - interception : " + interception;
        return edge + "," +
                interception + "," +
                edge_atas + "," +
                edge_bawah + "," +
                edge_kiri + "," +
                edge_kanan + "," +
                interception_atas + "," +
                interception_bawah + "," +
                interception_kiri + "," +
                interception_kanan + "," +
                edge_kiri_atas + "," +
                edge_kanan_atas + "," +
                edge_kiri_bawah + "," +
                edge_kanan_bawah + "," +
                interception_kiri_atas + "," +
                interception_kanan_atas + "," +
                interception_kiri_bawah + "," +
                interception_kanan_bawah + "," +
                perbandingan_jarak + "," +
                jumlah_area + "," +
//                jumlah_kiri + "," +
//                jumlah_kanan + "," +
//                jumlah_atas + "," +
//                jumlah_bawah + "," +
                type_1 + "," +
                type_2 + "," +
                type_3 + "," +
                type_4 + "," +
                type_5 + "," +
                type_6 + "," +
                type_7 + "," +
                type_8;
    }
}
