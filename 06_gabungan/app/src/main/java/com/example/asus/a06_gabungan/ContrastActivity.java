package com.example.asus.a06_gabungan;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.Image;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class ContrastActivity extends AppCompatActivity {
    ImageView mImageView;
    Bitmap img;

    static final int REQUEST_TAKE_PHOTO = 1;
    private static final int CAMERA_REQUEST = 1888;
    public static float koefAlgTwo = (float) 0.01;
    private int PICK_IMAGE_REQUEST = 1;
    private ImageView result;
    private int width, height;
    private int[] red = new int[256];
    private int[] green = new int[256];
    private int[] blue = new int[256];
    private static final int MY_CAMERA_PERMISSION_CODE = 100;
    private String mCurrentPhotoPath;

    private Button algoritma_1, algoritma_2;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contrast);

        result = (ImageView) findViewById(R.id.img_new);
        algoritma_1 = (Button) findViewById(R.id.button_algoritma_1);
        algoritma_2 = (Button) findViewById(R.id.button_algoritma_2);

        setImage();

        for (int i = 0; i < 256; ++i) {
            red[i] = 0;
            green[i] = 0;
            blue[i] = 0;
        }

        getBitmapPixel(img);

        algoritma_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                algOne(v);
            }
        });

        algoritma_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                algTwo(v);
            }
        });
    }

    private void setImage(){
        mImageView = findViewById(R.id.image);
        byte[] byteArray = getIntent().getByteArrayExtra("image");
        img = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
        mImageView.setImageBitmap(img);
    }

    public void getBitmapPixel(Bitmap bitmap) {
        width = bitmap.getWidth();
        height = bitmap.getHeight();
        int pixel;
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                pixel = bitmap.getPixel(i, j);
                red[Color.red(pixel)]++;
                green[Color.green(pixel)]++;
                blue[Color.blue(pixel)]++;
            }
        }
    }

    public void getCumulative(long[] arr) {
//        Log.d("Pengali", String.valueOf(pengali));
        for (int i = 0; i < 256; ++i) {
            arr[i] = (long) (255 * arr[i]) / (arr[255] - arr[0]);
        }
    }
    public int mapNewPixel(int pixel, long[] newRed, long[] newGreen, long[] newBlue) {
        int dummRed = (int) newRed[Color.red(pixel)];
        int dummGreen = (int) newGreen[Color.green(pixel)];
        int dummBlue = (int) newBlue[Color.blue(pixel)];

        return Color.rgb(dummRed, dummGreen, dummBlue);
    }
    /**
     * 1. Transform anggota array ke i = total nilai dari indeks ke 0 hingga ke i
     */
    public void algOne(View v) {
        Bitmap test = img.copy(img.getConfig(), true);
        long[] newRed = new long[256];
        long[] newGreen = new long[256];
        long[] newBlue = new long[256];

        newRed[0] = red[0];
        newGreen[0] = green[0];
        newBlue[0] = blue[0];

        for (int i = 1; i < 256; ++i) {
            newRed[i] = red[i] + newRed[i - 1];
            newGreen[i] = green[i] + newGreen[i - 1];
            newBlue[i] = blue[i] + newBlue[i - 1];
        }

        getCumulative(newRed);
        getCumulative(newGreen);
        getCumulative(newBlue);

        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                int pixel = test.getPixel(i,j);

                test.setPixel(i, j, mapNewPixel(pixel, newRed, newGreen, newBlue));
            }
        }
        result.setImageBitmap(test);
    }
    public int getMinAlgTwo(int[] arr, float batas) {
        int iterator = 0;
        boolean end = false;
        while (iterator < 256 && !end) {
            if (arr[iterator] >= batas) {
                end = true;
            } else {
                iterator++;
            }
        }
        return iterator;
    }
    public int getMaxAlgTwo(int[] arr, float batas) {
        int iterator = 255;
        boolean end = false;
        while (iterator >= 0 && !end) {
            if (arr[iterator] >= batas) {
                end = true;
            } else {
                iterator--;
            }
        }
        return iterator;
    }
    public void algTwo (View v) {
        Bitmap tempImage = img.copy(img.getConfig(), true);
        int[] newRed = red;
        int[] newGreen = green;
        int[] newBlue = blue;

        double discard_ratio = 0.01;
        int[][] hists = new int[3][256];

        hists[0] = newRed;
        hists[1] = newGreen;
        hists[2] = newBlue;

        int total = tempImage.getWidth() * tempImage.getHeight();

        int[] vmin = new int[3];
        int[] vmax = new int[3];

        for (int i = 0; i < 3; ++i) {
            for (int j = 0; j < 255; ++j) {
                hists[i][j + 1] += hists[i][j];
            }
            vmin[i] = 0;
            vmax[i] = 255;
            while(hists[i][vmin[i]] < discard_ratio * total) {
                vmin[i] += 1;
            }
            while (hists[i][vmax[i]] > (1 - discard_ratio) * total) {
                vmax[i] -= 1;
            }
            if (vmax[i] < 255 - 1) {
                vmax[i] += 1;
            }
        }

        for (int y = 0; y < width; ++y) {
            for (int x = 0; x < height; ++x) {
                int[] rgbVal = new int[3];

                for (int j = 0; j < 3; ++j) {
                    int val = 0;
                    int pixel = tempImage.getPixel(y, x);
                    int Rval = Color.red(pixel);
                    int Gval = Color.green(pixel);
                    int Bval = Color.green(pixel);

                    if (j == 0) val = Rval;
                    if (j == 1) val = Gval;
                    if (j == 2) val = Bval;

                    if (val < vmin[j]) val = vmin[j];
                    if (val > vmax[j]) val = vmax[j];

                    rgbVal[j] = (int)((val - vmin[j]) * 255.0 / (vmax[j] - vmin[j]));
                }

                tempImage.setPixel(y,x, Color.rgb(rgbVal[0], rgbVal[1], rgbVal[2]));
            }
        }
        result.setImageBitmap(tempImage);
    }
}
