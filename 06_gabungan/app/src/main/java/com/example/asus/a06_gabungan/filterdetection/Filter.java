package com.example.asus.a06_gabungan.filterdetection;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.util.Log;

import java.util.Arrays;

import static java.lang.Math.abs;

public class Filter {
    Bitmap img;
    int[] maxRGB = new int[]{0,0,0};
    final double threshold_sobel = 0.7;

    public Filter(Bitmap img) {
        this.img = img;
    }

    private int[] getRGB(int pixel) {
        int r = Color.red(pixel);
        int g = Color.green(pixel);
        int b = Color.blue(pixel);

        return new int[]{r, g, b};
    }

    public int[][][] tetanggaToMatrix(int x, int y, String type) {
        int[][][] dumm = new int[3][3][];
        int[][][] dumm2 = new int [2][2][];
        if(!type.equals("roberts")){
            dumm[0][0] = getRGB(img.getPixel(x - 1, y - 1));
            dumm[0][1] = getRGB(img.getPixel(x, y - 1));
            dumm[0][2] = getRGB(img.getPixel(x + 1, y - 1));
            dumm[1][0] = getRGB(img.getPixel(x - 1, y));
            dumm[1][1] = getRGB(img.getPixel(x, y));
            dumm[1][2] = getRGB(img.getPixel(x + 1, y));
            dumm[2][0] = getRGB(img.getPixel(x - 1, y + 1));
            dumm[2][1] = getRGB(img.getPixel(x, y + 1));
            dumm[2][2] = getRGB(img.getPixel(x + 1, y + 1));
            return dumm;
        } else{
            dumm2[0][0] = getRGB(img.getPixel(x, y));
            dumm2[0][1] = getRGB(img.getPixel(x, y+1));
            dumm2[1][0] = getRGB(img.getPixel(x+1, y));
            dumm2[1][1] = getRGB(img.getPixel(x+1, y+1));
            return dumm2;
        }
    }

    public int[] getTetangga(int x, int y, int height, int width) {
        int[] array_tetangga;
        if(x == 0 && y == 0){
            array_tetangga = new int[]{img.getPixel(x, y), img.getPixel(x+1, y), img.getPixel(x, y+1)};
        } else if(x == width-1 && y == 0){
            array_tetangga = new int[]{img.getPixel(x, y), img.getPixel(x-1, y), img.getPixel(x, y+1)};
        } else if(x == width-1 && y == height-1){
            array_tetangga = new int[]{img.getPixel(x, y), img.getPixel(x-1, y), img.getPixel(x, y-1)};
        } else if(x == 0 && y == height-1){
            array_tetangga = new int[]{img.getPixel(x, y), img.getPixel(x+1, y), img.getPixel(x, y-1)};
        } else if(x == 0){
            array_tetangga = new int[]{img.getPixel(x, y), img.getPixel(x, y+1), img.getPixel(x, y-1),
                    img.getPixel(x+1, y), img.getPixel(x+1, y+1), img.getPixel(x+1, y-1)};
        } else if(x == width-1){
            array_tetangga = new int[]{img.getPixel(x, y), img.getPixel(x, y+1), img.getPixel(x, y-1),
                    img.getPixel(x-1, y), img.getPixel(x-1, y+1), img.getPixel(x-1, y-1)};
        } else if(y == 0){
            array_tetangga = new int[]{img.getPixel(x, y), img.getPixel(x+1, y), img.getPixel(x-1, y),
                    img.getPixel(x, y+1), img.getPixel(x+1, y+1), img.getPixel(x-1, y+1)};
        } else if(y == height-1){
            array_tetangga = new int[]{img.getPixel(x, y), img.getPixel(x+1, y), img.getPixel(x-1, y),
                    img.getPixel(x, y-1), img.getPixel(x+1, y-1), img.getPixel(x-1, y-1)};
        } else {
            array_tetangga = new int[]{img.getPixel(x, y+1), img.getPixel(x+1, y+1), img.getPixel(x-1, y+1),
                    img.getPixel(x, y), img.getPixel(x+1, y), img.getPixel(x-1, y),
                    img.getPixel(x, y-1), img.getPixel(x+1, y-1), img.getPixel(x-1, y-1)};
        }

        return array_tetangga;
    }

    public int[] getMedian(int[] array){
        int length = array.length;

        int[] red_array = new int[length];
        int[] blue_array = new int[length];
        int[] green_array = new int[length];
        int median_red, median_blue, median_green;
        for (int i=0; i<length; ++i){
            red_array[i] = Color.red(array[i]);
            green_array[i] = Color.green(array[i]);
            blue_array[i] = Color.blue(array[i]);
        }
        Arrays.sort(red_array);
        Arrays.sort(green_array);
        Arrays.sort(green_array);

        if(length%2 == 1){
            median_red = red_array[1 + length/2];
            median_green = green_array[1 + length/2];
            median_blue = blue_array[1 + length/2];
        } else {
            median_red = (red_array[length/2] + red_array[1 + length/2])/2;
            median_green = (green_array[length/2] + green_array[1 + length/2])/2;
            median_blue = (blue_array[length/2] + blue_array[1 + length/2])/2;
        }
        return new int[]{median_red, median_green, median_blue};
    }

    private int[] toRGB(int pixel) {
        int[] rgb = new int[3];
        rgb[0] = Color.red(pixel);
        rgb[1] = Color.green(pixel);
        rgb[2] = Color.blue(pixel);

        return rgb;
    }

    private int[] difference(int[] a, int[] b) {
        int[] dumm = new int[3];
        for (int i = 0; i < 3; ++i) {
            dumm[i] = abs(a[i] - b[i]);
        }

        return dumm;
    }

    private void updateMax(int[] max, int[] diff) {
        if (max[0] < diff[0]) {
            max[0] = diff[0];
        }
        if (max[1] < diff[1]) {
            max[1] = diff[1];
        }
        if (max[2] < diff[2]) {
            max[2] = diff[2];
        }
    }

    public Bitmap kurangTengah() {
        Bitmap dummy = img.copy(img.getConfig(), true);
        int height = img.getHeight();
        int width = img.getWidth();
        int[] sebrang = new int[8];
        for(int y = 0; y < height; ++y) {
            for (int x = 0; x < width; ++x) {
                int[] neighbors = getTetangga(x, y, height, width);
                int[] max = new int[]{0, 0, 0};
                for (int i = 0; i < neighbors.length; ++i) {
                    int[] diff = new int[]{0,0,0};
                    int[] rgb = toRGB(neighbors[i]);
                    int[] me = toRGB(img.getPixel(x, y));
                    diff[0] = abs(rgb[0] - me[0]);
                    diff[1] = abs(rgb[1] - me[1]);
                    diff[2] = abs(rgb[2] - me[2]);

                    if (max[0] < diff[0]) {
                        max[0] = diff[0];
                    }
                    if (max[1] < diff[1]) {
                        max[1] = diff[1];
                    }
                    if (max[2] < diff[2]) {
                        max[2] = diff[2];
                    }
                }

                dummy.setPixel(x, y, Color.rgb(max[0], max[1], max[2]));
            }
        }
        return dummy;
    }

    public Bitmap neighbourKurangSebrang() {
        Bitmap dummy = img.copy(img.getConfig(), true);
        int height = img.getHeight();
        int width = img.getWidth();

        for(int y = 1; y < height - 1; ++y) {
            for (int x = 1; x < width - 1; ++x) {
                int[] neighbors = getTetangga(x, y, height, width);
                int[] max = new int[]{0, 0, 0};
                for (int i = 0; i < 5; ++i) {
                    int[] diff = new int[]{0,0,0};
                    int[] rgb = toRGB(img.getPixel(x,y));
                    int[] rgb1, rgb2;
                    // Timur - Barat
                    rgb1 = toRGB(img.getPixel(x - 1, y));
                    rgb2 = toRGB(img.getPixel(x + 1, y));
                    diff = difference(rgb1, rgb2);
                    updateMax(max, diff);

                    // Utara - Selatan
                    rgb1 = toRGB(img.getPixel(x, y - 1));
                    rgb2 = toRGB(img.getPixel(x, y + 1));
                    diff = difference(rgb1, rgb2);
                    updateMax(max, diff);

                    // Tenggara - Barat Laut
                    rgb1 = toRGB(img.getPixel(x - 1, y - 1));
                    rgb2 = toRGB(img.getPixel(x + 1, y + 1));
                    diff = difference(rgb1, rgb2);
                    updateMax(max, diff);

                    // Timur Laut - Barat Daya
                    rgb1 = toRGB(img.getPixel(x - 1, y + 1));
                    rgb2 = toRGB(img.getPixel(x + 1, y - 1));
                    diff = difference(rgb1, rgb2);
                    updateMax(max, diff);
                }
                dummy.setPixel(x, y, Color.rgb(max[0], max[1], max[2]));
            }
        }
        return dummy;
    }

    public Bitmap makeMedianImage(){
        Bitmap test = img.copy(img.getConfig(), true);
        int height = img.getHeight();
        int width = img.getWidth();
        int[] median;

        for(int y = 0; y < height; ++y){
            for(int x = 0; x < width; ++x){
                if(x == 0 && y == 0){
                    int[] array_tetangga = {img.getPixel(x, y), img.getPixel(x+1, y), img.getPixel(x, y+1)};
                    median = getMedian(array_tetangga);
                } else if(x == width-1 && y == 0){
                    int[] array_tetangga = {img.getPixel(x, y), img.getPixel(x-1, y), img.getPixel(x, y+1)};
                    median = getMedian(array_tetangga);
                } else if(x == width-1 && y == height-1){
                    int[] array_tetangga = {img.getPixel(x, y), img.getPixel(x-1, y), img.getPixel(x, y-1)};
                    median = getMedian(array_tetangga);
                } else if(x == 0 && y == height-1){
                    int[] array_tetangga = {img.getPixel(x, y), img.getPixel(x+1, y), img.getPixel(x, y-1)};
                    median = getMedian(array_tetangga);
                } else if(x == 0){
                    int[] array_tetangga = {img.getPixel(x, y), img.getPixel(x, y+1), img.getPixel(x, y-1),
                            img.getPixel(x+1, y), img.getPixel(x+1, y+1), img.getPixel(x+1, y-1)};
                    median = getMedian(array_tetangga);
                } else if(x == width-1){
                    int[] array_tetangga = {img.getPixel(x, y), img.getPixel(x, y+1), img.getPixel(x, y-1),
                            img.getPixel(x-1, y), img.getPixel(x-1, y+1), img.getPixel(x-1, y-1)};
                    median = getMedian(array_tetangga);
                } else if(y == 0){
                    int[] array_tetangga = {img.getPixel(x, y), img.getPixel(x+1, y), img.getPixel(x-1, y),
                            img.getPixel(x, y+1), img.getPixel(x+1, y+1), img.getPixel(x-1, y+1)};
                    median = getMedian(array_tetangga);
                } else if(y == height-1){
                    int[] array_tetangga = {img.getPixel(x, y), img.getPixel(x+1, y), img.getPixel(x-1, y),
                            img.getPixel(x, y-1), img.getPixel(x+1, y-1), img.getPixel(x-1, y-1)};
                    median = getMedian(array_tetangga);
                } else {
                    int[] array_tetangga = {img.getPixel(x, y+1), img.getPixel(x+1, y+1), img.getPixel(x-1, y+1),
                            img.getPixel(x, y), img.getPixel(x+1, y), img.getPixel(x-1, y),
                            img.getPixel(x, y-1), img.getPixel(x+1, y-1), img.getPixel(x-1, y-1)};
                    median = getMedian(array_tetangga);
                }
                Log.d("FILTER", "red : "+median[0]);
                Log.d("FILTER", "green : "+median[1]);
                Log.d("FILTER", "blue : "+median[2]);
                test.setPixel(x, y, Color.rgb(median[0], median[1], median[2]));
            }
        }
        return test;
    }

    public double[] kaliKonvolusiMatrix(double[][] constKonvolusi, int[][][] B) {
        double[] sum = new double[]{0, 0, 0};
        for (int i = 0; i < B.length; ++i) {
            for (int j = 0; j < B[0].length; ++j) {
                for (int k = 0; k < 3; ++k) {
                    sum[k] += (constKonvolusi[i][j] * B[i][j][k]);
                }
            }
        }

        return sum;
    }

    public int[] kaliKonvolusiMatrix(int[][] constKonvolusi, int[][][] B) {
        int[] sum = new int[]{0, 0, 0};
        for (int i = 0; i < B.length; ++i) {
            for (int j = 0; j < B[0].length; ++j) {
                for (int k = 0; k < 3; ++k) {
                    sum[k] += (constKonvolusi[i][j] * B[i][j][k]);
                }
            }
        }

        return sum;
    }

    public int max(double a, int b) {
        if (a > b) {
            return (int) a;
        } else {
            return b;
        }
    }

}
