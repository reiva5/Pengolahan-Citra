package com.example.asus.a06_gabungan;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;

import java.util.ArrayList;

public class SpecificationActivity extends AppCompatActivity {
    private int width, height;
    private float[] input_red = new float[256];
    private float[] input_green = new float[256];
    private float[] input_blue = new float[256];
    private float[] user_red = new float[256];
    private float[] user_green = new float[256];
    private float[] user_blue = new float[256];
    private TextView angka_pertama_edit;
    private TextView angka_kedua_edit;
    private TextView angka_ketiga_edit;
    ImageView mImageView, newImageView;
    Bitmap img;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_specification);
        setImage();
        getBitmapPixel(img);

        angka_pertama_edit = (TextView) findViewById(R.id.angka_pertama);
        angka_kedua_edit = (TextView) findViewById(R.id.angka_kedua);
        angka_ketiga_edit = (TextView) findViewById(R.id.angka_ketiga);
        SeekBar seekBar = findViewById(R.id.angka_pertama_slider);
        SeekBar seekBar2 = findViewById(R.id.angka_kedua_slider);
        SeekBar seekBar3 = findViewById(R.id.angka_ketiga_slider);
        angka_pertama_edit.setText(String.valueOf(seekBar.getProgress()));
        angka_kedua_edit.setText(String.valueOf(seekBar2.getProgress()));
        angka_ketiga_edit.setText(String.valueOf(seekBar3.getProgress()));
        seekBar.setOnSeekBarChangeListener(
                new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                        angka_pertama_edit.setText(String.valueOf(i));
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                    }
                }
        );
        seekBar2.setOnSeekBarChangeListener(
                new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                        angka_kedua_edit.setText(String.valueOf(i));
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                    }
                }
        );
        seekBar3.setOnSeekBarChangeListener(
                new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                        angka_ketiga_edit.setText(String.valueOf(i));
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                    }
                }
        );

        makeHistogram(input_red, 256, R.id.histogram_input_red);
        makeHistogram(input_green, 256, R.id.histogram_input_green);
        makeHistogram(input_blue, 256, R.id.histogram_input_blue);
        executeUserButton();
        makeNewImage();
    }

    private void setImage(){
        mImageView = findViewById(R.id.image);
        byte[] byteArray = getIntent().getByteArrayExtra("image");
        img = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
        mImageView.setImageBitmap(img);
    }

    public void getBitmapPixel(Bitmap bitmap) {
        width = bitmap.getWidth();
        height = bitmap.getHeight();
        int pixel;
        for (int i = 0; i < 256; i++) {
            input_red[i] = 0;
            input_green[i] = 0;
            input_blue[i] = 0;
        }

        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                pixel = bitmap.getPixel(i, j);
                input_red[Color.red(pixel)]++;
                input_green[Color.green(pixel)]++;
                input_blue[Color.blue(pixel)]++;
            }
        }
    }

    public void executeUserButton(){
        Button userHistogramButton = findViewById(R.id.user_histogram_button);
        userHistogramButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                float angka_pertama = Float.parseFloat(angka_pertama_edit.getText().toString());
                float angka_kedua = Float.parseFloat(angka_kedua_edit.getText().toString());
                float angka_ketiga = Float.parseFloat(angka_ketiga_edit.getText().toString());

                getUserHistogramArray(angka_pertama,angka_kedua,angka_ketiga);
                makeHistogram(user_red, 256, R.id.histogram_user_red);
                makeHistogram(user_green, 256, R.id.histogram_user_green);
                makeHistogram(user_blue, 256, R.id.histogram_user_blue);
            }
        });
    }

    public void getUserHistogramArray(float angka_pertama, float angka_kedua, float angka_ketiga){
        for (int i = 0; i < 256; i++) {
            user_red[i] = 0;
            user_blue[i] = 0;
            user_green[i] = 0;
        }

        for(int i=0; i<128; i++){
            user_red[i] = angka_pertama + (i * (angka_kedua - angka_pertama))/127;
            user_blue[i] = angka_pertama + (i * (angka_kedua - angka_pertama))/127;
            user_green[i] = angka_pertama + (i * (angka_kedua - angka_pertama))/127;
        }
        for(int i=128; i<255; i++){
            user_red[i] = angka_kedua + ((i-127) * (angka_ketiga - angka_kedua))/127;
            user_blue[i] = angka_kedua + ((i-127) * (angka_ketiga - angka_kedua))/127;
            user_green[i] = angka_kedua + ((i-127) * (angka_ketiga - angka_kedua))/127;
        }

//        int[] map = mapOriginalHistogram(roundCumulativeArray(input_red), roundCumulativeArray(user_red));
//        for(int i=0; i<256; i++){
//            Log.d(LOG_TAG, "isi map["+i+"] : "+map[i]);
//        }
    }

    public void makeHistogram(float[] arr, int length, int idHistogram){
        ArrayList<BarEntry> barEntries = new ArrayList<>();
        ArrayList<String> xAxis = new ArrayList<>();

        for (int i=0; i<length; i++){
            barEntries.add(new BarEntry(i, arr[i]));
            xAxis.add(""+i);
        }
        BarDataSet barDataSet = new BarDataSet(barEntries, "");

        if (idHistogram == R.id.histogram_input_red || idHistogram == R.id.histogram_user_red ||
                idHistogram == R.id.histogram_result_red){
            barDataSet.setLabel("red");
            barDataSet.setColor(Color.rgb(255, 0,0));
        }
        else if (idHistogram == R.id.histogram_input_green ||
                idHistogram == R.id.histogram_user_green ||
                idHistogram == R.id.histogram_result_green){
            barDataSet.setLabel("green");
            barDataSet.setColor(Color.rgb(0, 255,0));
        }
        else if (idHistogram == R.id.histogram_input_blue || idHistogram == R.id.histogram_user_blue
                || idHistogram == R.id.histogram_result_blue){
            barDataSet.setLabel("blue");
            barDataSet.setColor(Color.rgb(0, 0,255));
        }

        BarData barData = new BarData(barDataSet);

        BarChart barChart = (BarChart) findViewById(idHistogram);
        barChart.setVisibility(View.VISIBLE);
        barChart.setData(barData);

        barChart.getDescription().setEnabled(false);
        barChart.getXAxis().setDrawGridLines(false);

        barChart.getAxisLeft().setDrawLabels(false);
        barChart.getAxisRight().setDrawLabels(false);
        barChart.getXAxis().setDrawLabels(false);
        barChart.getLegend().setEnabled(true);

        barChart.setTouchEnabled(true);
        barChart.setDragEnabled(true);
        barChart.setScaleEnabled(true);
    }

    private void makeNewImage(){
        newImageView = findViewById(R.id.new_image);
        Button newImageButton = findViewById(R.id.make_new_image_button);

        newImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int[] map_red = mapOriginalHistogram(roundCumulativeArray(input_red), roundCumulativeArray(user_red));
                int[] map_green = mapOriginalHistogram(roundCumulativeArray(input_green), roundCumulativeArray(user_green));
                int[] map_blue = mapOriginalHistogram(roundCumulativeArray(input_blue), roundCumulativeArray(user_blue));
                Bitmap hasil = img.copy(img.getConfig(), true);

                float[] new_red = new float[256];
                float[] new_green= new float[256];
                float[] new_blue = new float[256];

                for(int i=0; i<width; i++){
                    for(int j=0; j<height; j++){
                        int pixel = hasil.getPixel(i, j);
                        int newPixel = mapNewPixel(pixel, map_red, map_green, map_blue);
                        hasil.setPixel(i, j, newPixel);
                        new_red[Color.red(newPixel)]++;
                        new_green[Color.green(newPixel)]++;
                        new_blue[Color.blue(newPixel)]++;
                    }
                }
                newImageView.setVisibility(View.VISIBLE);
                newImageView.setImageBitmap(hasil);
                makeHistogram(new_red, 256, R.id.histogram_result_red);
                makeHistogram(new_green, 256, R.id.histogram_result_green);
                makeHistogram(new_blue, 256, R.id.histogram_result_blue);
            }
        });
    }

    public int mapNewPixel(int pixel, int[] newRed, int[] newGreen, int[] newBlue) {
        int dummRed = (int) newRed[Color.red(pixel)];
        int dummGreen = (int) newGreen[Color.green(pixel)];
        int dummBlue = (int) newBlue[Color.blue(pixel)];

        return Color.rgb(dummRed, dummGreen, dummBlue);
    }

    private int[] roundCumulativeArray(float[] arr){
        float total = 0;
        int length = arr.length;
        int[] hasil = new int[length];
        float[] cumulative = new float[length];

        for(int i=0; i<length; i++){
            total = total + arr[i];
        }
        cumulative[0] = arr[0];
        for(int i=1; i<length; i++){
            cumulative[i] = arr[i] + cumulative[i-1];
        }
        for(int i=0; i<length; i++){
            cumulative[i] = cumulative[i] * (length-1) / total;
            hasil[i] = Math.round(cumulative[i]);
        }

        return hasil;
    }
    private int[] mapOriginalHistogram(int[] original_round, int[] target_round){
        int length = original_round.length;
        int[] hasil = new int[256];

        for(int i=0; i<length; i++){
            int j=0;
            while(target_round[j] < original_round[i] && j<length){
                j++;
            }
            hasil[i] = j;
        }
        return hasil;
    }
}
