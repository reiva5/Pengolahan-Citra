package com.example.asus.a06_gabungan.facerecognition;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.util.Log;
import android.util.Pair;

import com.example.asus.a06_gabungan.Point;
import com.example.asus.a06_gabungan.filterdetection.Sobel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class FaceRecognition {
    Bitmap img;
    int[] threshold;
    final int VAL_INF = 99999;
    public static final double ratio = 0.4;
    public static final double ratio_err = 0.1;
    private int[] dirX = new int[]{0, 1, 0, -1};
    private int[] dirY = new int[]{1, 0, -1, 0};

    public FaceRecognition(Bitmap img) {
        this.img = img;
        this.threshold = new int[]{10, 10, 10};
    }

    public FaceRecognition(Bitmap img, int[] threshold) {
        this(img);
        this.threshold = threshold;
    }

    Boolean valid(Point point) {
        return point.getX() < img.getWidth() &&
                point.getX() >= 0 &&
                point.getY() < img.getHeight() &&
                point.getY() >= 0;
    }

    int max(int[] array){
        int result = array[0];
        for(int i=1; i<array.length; i++){
            if (array[i] > result){
                result = array[i];
            }
        }
        return result;
    }

    int min(int[] array){
        int result = array[0];
        for(int i=1; i<array.length; i++){
            if (array[i] < result){
                result = array[i];
            }
        }
        return result;
    }

    Boolean validBrown(int[] colorPallete) {
        int r = colorPallete[0];
        int g = colorPallete[1];
        int b = colorPallete[2];
        int[] YCbCr = ColorHandler.getYCbCr(r, g, b);
//        int[] HSV = ColorHandler.getHSV(colorPallete);
//        int max = max(colorPallete);
//        int min = min(colorPallete);
//
//        return 95 < colorPallete[0] && colorPallete[0] <= 256 &&
//                40 < colorPallete[1] && colorPallete[1] <= 256 &&
//                20 < colorPallete[2] && colorPallete[2] <= 256 &&
//                colorPallete[0] > colorPallete[1] &&
//                colorPallete[0] > colorPallete[2] &&
//                max - min < 175 &&
//                (colorPallete[0] - colorPallete[1]) > 15;
        int r_g = r - g;
        if (r_g < 0) r_g *= -1;
        return r > 95 &&
                g > 40 &&
                b > 20 &&
                r > g &&
                r > b &&
                r_g > 15 &&
                YCbCr[2] > 135 &&
                YCbCr[1] > 85 &&
                YCbCr[0] > 80 &&
                YCbCr[2] <= (1.5862 * YCbCr[1]) + 20 &&
                YCbCr[2] >= (0.3448 * YCbCr[1]) + 76.2069 &&
                YCbCr[2] >= (-4.5652 * YCbCr[1]) + 234.5652 &&
                YCbCr[2] <= (-1.15 * YCbCr[1]) + 301.75 &&
                YCbCr[2] <= (-2.2857 * YCbCr[1]) + 432.85;
    }

    Boolean validRed(int [] colorPallete){
        return  colorPallete[0] >= 128
                && colorPallete[0] >= colorPallete[1]
                && colorPallete[0] >= colorPallete[2]
                && (float)colorPallete[0]/(colorPallete[0]-colorPallete[1]) < 3.2
                && (float)colorPallete[0]/(colorPallete[0]-colorPallete[2]) < 3.2;
//                && colorPallete[1]-colorPallete[2] <= 15
//                && colorPallete[1]-colorPallete[2] >= -15;
    }

    public int luas(Pair<Point, Point> batas){
        return (batas.second.getX()-batas.first.getX()) * (batas.second.getY()*batas.first.getY());
    }

    public Bitmap getResult() {
        Bitmap dummy = img.copy(img.getConfig(), true);
        int height = img.getHeight();
        int width = img.getWidth();
        int[][] type = new int[height][width];
        for (int i = 0; i < height; ++i) {
            for (int j = 0; j < width; ++j) {
                type[i][j] = -1;
            }
        }
        for (int i = 0; i < height; ++i) {
            for (int j = 0; j < width; ++j) {
                int pixel = img.getPixel(j, i);
                if (validBrown(new int[]{Color.red(pixel),
                        Color.green(pixel),
                        Color.blue(pixel)})) {
                    type[i][j] = 0;
                }
            }
        }
        Boolean visitOuterZone = false;
        int counter = 2;
        for (int i = 0; i < height; ++i) {
            for (int j = 0; j < width; ++j) {
                if (type[i][j] == 0) {
                    Queue<Point> q = new LinkedList<>();
                    q.add(new Point(j,i));
                    Point curr;
                    while (!q.isEmpty()) {
                        curr = q.poll();
                        if (type[curr.getY()][curr.getX()] == 0) {
                            type[curr.getY()][curr.getX()] = counter;
                            for (int k = 0; k < dirX.length; ++k) {
                                Point next = new Point(curr.getX() + dirX[k], curr.getY() + dirY[k]);
                                if (valid(next) && type[next.getY()][next.getX()] == 0) {
                                    q.add(next);
                                }
                            }
                        }
                    }
                    ++counter;
                } else if (!visitOuterZone) {
                    visitOuterZone = true;
                    Queue<Point> q = new LinkedList<>();
                    q.add(new Point(j,i));
                    Point curr;
                    while (!q.isEmpty()) {
                        curr = q.poll();
                        if (type[curr.getY()][curr.getX()] == -1) {
                            type[curr.getY()][curr.getX()] = -VAL_INF;
                            for (int k = 0; k < dirX.length; ++k) {
                                Point next = new Point(curr.getX() + dirX[k], curr.getY() + dirY[k]);
                                if (valid(next) && type[next.getY()][next.getX()] == -1) {
                                    q.add(next);
                                }
                            }
                        }
                    }
                }
            }
        }
        Boolean[] isFace = new Boolean[counter+1];
        int[] feature = new int[counter+1];
        ArrayList<Pair<Point,Point>>[] features = new ArrayList[counter+1];
        for (int i = 0; i < isFace.length; ++i) {
            isFace[i] = false;
            feature[i] = 0;
            features[i] = new ArrayList<>();
        }

        for (int i = 0; i < height; ++i) {
            for (int j = 0; j < width; ++j) {
                if (type[i][j] == -1) {
                    int parent = -1;
                    for (int k = 0; k < dirX.length; ++k) {
                        Point adj = new Point(j + dirX[k], i + dirY[k]);
                        if (valid(adj) && type[adj.getY()][adj.getX()] > 0) {
                            parent = type[adj.getY()][adj.getX()];
                            break;
                        }
                    }
                    if (parent == -1) continue;
                    int maxX, maxY, minX, minY;
                    Queue<Point> q = new LinkedList<>();
                    q.add(new Point(j,i));
                    maxX = j;
                    maxY = i;
                    minX = j;
                    minY = i;
                    Point curr;
                    while (!q.isEmpty()) {
                        curr = q.poll();
                        if (type[curr.getY()][curr.getX()] == -1) {
                            type[curr.getY()][curr.getX()] = -parent;
                            for (int k = 0; k < dirX.length; ++k) {
                                Point next = new Point(curr.getX() + dirX[k], curr.getY() + dirY[k]);
                                if (valid(next) && type[next.getY()][next.getX()] == -1) {
                                    q.add(next);
                                    maxX = Math.max(maxX, next.getX());
                                    minX = Math.min(minX, next.getX());
                                    maxY = Math.max(maxY, next.getY());
                                    minY = Math.min(minY, next.getY());
                                }
                            }
                        }
                    }
                    ++feature[parent];
                    features[parent].add(new Pair<>(new Point(minX, minY), new Point(maxX, maxY)));
                }
            }
        }
        for (int i = 1; i < counter; ++i) {
            isFace[i] = feature[i] > 1;
        }
        int[][] batasArea = new int[counter+1][4];
        for (int i = 1; i < counter; ++i) {
            batasArea[i][0] = width;
            batasArea[i][1] = -1;
            batasArea[i][2] = height;
            batasArea[i][3] = -1;
        }
        for (int i = 0; i < height; ++i) {
            for (int j = 0; j < width; ++j) {
                if (type[i][j] > 0) {
                    batasArea[type[i][j]][0] = Math.min(batasArea[type[i][j]][0], j);
                    batasArea[type[i][j]][1] = Math.max(batasArea[type[i][j]][1], j);
                    batasArea[type[i][j]][2] = Math.min(batasArea[type[i][j]][2], i);
                    batasArea[type[i][j]][3] = Math.max(batasArea[type[i][j]][3], i);
                }
            }
        }
        ArrayList<Pair<Point,Point>> finalResult = new ArrayList<>();
        for (int i = 1; i < counter; ++i) {
            if (!isFace[i]) continue;
            boolean contain;
            int featureCounter = 0;
            for (Pair<Point, Point> batas : features[i]) {
                contain = true;
//                double currRatio = 0;
////                if (batas.second.getX() != batas.first.getX()) {
//                    int pjg = batas.second.getX() - batas.first.getX();
//                    if (pjg < 0) pjg *= -1;
//                    int lebar = batas.second.getY() - batas.first.getY();
//                    if  (lebar < 0) lebar *= -1;
////                    currRatio = lebar / pjg;
////                }
////                int countBlck = 0;
////                if (currRatio > ratio - ratio_err && currRatio < ratio + ratio_err) {
//                int y1 = batasArea[i][2];
//                int y2 = batasArea[i][3];
//                int x1 = batasArea[i][0];
//                int x2 = batasArea[i][1];
//
//                if (lebar < pjg*2/3 && batas.first.getY()<y1*2/3+y2/3) {
//                    for (int y = batas.first.getY(); y <= batas.second.getY(); ++y) {
//                        for (int x = batas.first.getX(); x <= batas.second.getX(); ++x) {
////                            if (x == batas.first.getX() || x == batas.second.getX() || y == batas.first.getY() ||
////                                    y == batas.second.getY()) {
////                                dummy.setPixel(x, y, Color.rgb(255, 0, 0));
////                            }
//                            int pxl = img.getPixel(x, y);
//                            int r = Color.red(pxl);
//                            int g = Color.green(pxl);
//                            int b = Color.blue(pxl);
//                            if (((r + g + b) / 3) < 40) {
//                                contain = true;
//                            }
//                        }
//                    }
//                }
                if (contain) {
                    Log.d("HIDUNG", "pbdg luas : "+(float)luas(batas)/(batasArea[i][1]-batasArea[i][0])/(batasArea[i][3]-batasArea[i][2]));
                    finalResult.add(batas);
                    ++featureCounter;
                }
            }
            isFace[i] = featureCounter > 0;
            if (!isFace[i]) continue;
            int A = batasArea[i][2];
            int B = A + ((batasArea[i][1] - batasArea[i][0]) * 5 + 1)/ 4;
            batasArea[i][2] = A;
            batasArea[i][3] = B;
            for (int y = A; y <= B; ++y) {
                for (int x = batasArea[i][0]; x <= batasArea[i][1]; ++x) {
                    if (x == batasArea[i][0] || x == batasArea[i][1] || y == batasArea[i][2] ||
                            y == batasArea[i][3]){
                        dummy.setPixel(x, y, Color.rgb(0,255,0));
                    }
                }
            }
            int y_hidung_atas, y_hidung_bawah, x_hidung_kiri, x_hidung_kanan;
            y_hidung_atas = batasArea[i][3];
            x_hidung_kiri = batasArea[i][0];
            x_hidung_kanan = batasArea[i][1];

//            Log.d("HIDUNG", "ukuran : "+finalResult.size());
            if (finalResult.size() == 2){
                if(finalResult.get(0).first.getX() < finalResult.get(1).first.getX()){
                    x_hidung_kiri = finalResult.get(0).second.getX();
                    x_hidung_kanan = finalResult.get(1).first.getX();
                } else {
                    x_hidung_kiri = finalResult.get(1).second.getX();
                    x_hidung_kanan = finalResult.get(0).first.getX();
                }
                if(finalResult.get(0).first.getY() < finalResult.get(1).first.getY()){
                    y_hidung_atas = finalResult.get(0).first.getY();
                } else {
                    y_hidung_atas = finalResult.get(1).first.getY();
                }
            } else {
                for (Pair<Point, Point> batas : finalResult) {
                    if (y_hidung_atas > batas.first.getY()) {
                        y_hidung_atas = batas.first.getY();
                    }
                    if (x_hidung_kiri < batas.second.getX() && batas.second.getX() < (batasArea[i][0] * 2 / 5 + batasArea[i][1] * 3 / 5)) {
                        x_hidung_kiri = batas.second.getX();
                    }
                    if (x_hidung_kanan > batas.first.getX() && batas.first.getX() > (batasArea[i][0] * 3 / 5 + batasArea[i][1] * 2 / 5)) {
                        x_hidung_kanan = batas.first.getX();
                    }
                    if (x_hidung_kanan < x_hidung_kiri) {
                        int temp = x_hidung_kanan;
                        x_hidung_kanan = x_hidung_kiri;
                        x_hidung_kiri = temp;
                    }
                }
            }


            Boolean get_valid_red = false;
            Point bibir = new Point(-1, -1);
            int y_iter_hidung = y_hidung_atas;
            int x_tengah = (x_hidung_kanan+x_hidung_kiri)/2;
            while(!get_valid_red && y_iter_hidung<(float)batasArea[i][3]*3/4) {
                y_iter_hidung++;
                int pixel = img.getPixel(x_tengah, y_iter_hidung);
                Log.d("HIDUNG", "x y (r g b): "+ x_tengah + " "+y_iter_hidung+" ("+Color.red(pixel)+" "+Color.green(pixel)+" "+Color.blue(pixel)+")");
                int[] colorPallete = new int[]{Color.red(pixel), Color.green(pixel), Color.blue(pixel)};
//                if (validRed(colorPallete) || !validBrown(colorPallete)) {
//                    get_valid_red = true;
//                }
                if (validRed(colorPallete)) {
                    get_valid_red = true;
                    bibir = new Point(x_tengah, y_iter_hidung);
                }
            }

            y_hidung_bawah = y_hidung_atas+((y_iter_hidung-y_hidung_atas)*4/5);

            int x_bibir_kiri, x_bibir_kanan, y_bibir_atas, y_bibir_bawah;
            int y_iter_bibir = y_iter_hidung;
            x_bibir_kiri = x_hidung_kiri - (x_hidung_kanan-x_hidung_kiri)/2;
            x_bibir_kanan = x_hidung_kanan + (x_hidung_kanan-x_hidung_kiri)/2;
            y_bibir_atas = y_iter_hidung-1;
            Boolean not_lips = false;
            while(!not_lips && y_iter_bibir < batasArea[i][3]){
                y_iter_bibir++;
                int pixel = img.getPixel(x_tengah, y_iter_hidung);

                int[] colorPallete = new int[]{Color.red(pixel), Color.green(pixel), Color.blue(pixel)};
                if((float)(y_iter_bibir-y_bibir_atas)/(y_hidung_bawah-y_hidung_atas)>0.3 && (validBrown(colorPallete) || !validRed(colorPallete))){
                    not_lips = true;
                }
            }

            y_bibir_bawah = y_iter_bibir;

            Queue<Point> q = new LinkedList<>();
            q.add(bibir);
            Boolean[][] visit = new Boolean[img.getHeight()][img.getWidth()];
            for (int y = 0; y < img.getHeight(); ++y) {
                for (int x = 0; x < img.getWidth(); ++x) {
                    visit[y][x] = false;
                }
            }

            x_bibir_kiri = 1000000;
            x_bibir_kanan = -1;
            y_bibir_atas = 1000000;
            y_bibir_bawah = -1;

            while (!q.isEmpty()) {
                Point curr = q.poll();
                if (!visit[curr.getY()][curr.getX()]) {
                    visit[curr.getY()][curr.getX()] = true;
                    x_bibir_kiri = Math.min(x_bibir_kiri, curr.getX());
                    x_bibir_kanan = Math.max(x_bibir_kanan, curr.getX());
                    y_bibir_atas = Math.min(y_bibir_atas, curr.getY());
                    y_bibir_bawah = Math.max(y_bibir_bawah, curr.getY());
                    for (int k = 0; k < dirX.length; ++k) {
                        Point next = new Point(curr.getX() + dirX[k], curr.getY() + dirY[k]);
                        int pixel = img.getPixel(next.getX(), next.getY());
                        int[] colorPallete = new int[]{Color.red(pixel), Color.green(pixel), Color.blue(pixel)};
                        if (valid(next) && validRed(colorPallete) && !visit[next.getY()][next.getX()]) {
                            q.add(next);
                        }
                    }
                }
            }

            finalResult.add(new Pair<Point, Point>(new Point(x_hidung_kiri, y_hidung_atas), new Point(x_hidung_kanan, y_hidung_bawah)));
            finalResult.add(new Pair<Point, Point>(new Point(x_bibir_kiri, y_bibir_atas), new Point(x_bibir_kanan, y_bibir_bawah)));

        }



        for (Pair<Point, Point> batas : finalResult) {
            for (int y = batas.first.getY(); y <= batas.second.getY(); ++y) {
                for (int x = batas.first.getX(); x <= batas.second.getX(); ++x) {
                    if (x == batas.first.getX() || x == batas.second.getX() || y == batas.first.getY() ||
                            y == batas.second.getY()) {
                        dummy.setPixel(x, y, Color.rgb(255, 0, 0));
                    }
                }
            }
        }
        return dummy;
    }
}
