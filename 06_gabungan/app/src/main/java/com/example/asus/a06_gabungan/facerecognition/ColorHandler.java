package com.example.asus.a06_gabungan.facerecognition;

import android.graphics.Color;

public class ColorHandler {
    public static int[] getYCbCr(int r, int g, int b) {
        int[] YCbCr = new int[3];

        YCbCr[0] = (int) (0.299 * r + 0.587 * g + 0.114 * b);
        YCbCr[1] = (int) (128 - 0.169 * r - 0.331 * g + 0.5 * b);
        YCbCr[2] = (int) (128 + 0.5 * r - 0.419 * g - 0.081 * b);
        return YCbCr;
    }
    public static int max(int a, int b, int c) {
        if (a > b) {
            if (a > c) {
                return a;
            } else {
                return c;
            }
        } else {
            if (b > c) {
                return b;
            } else {
                return c;
            }
        }
    }

    public static int min(int a, int b, int c) {
        if (a < b) {
            if (a < c) {
                return a;
            } else {
                return c;
            }
        } else {
            if (b < c) {
                return b;
            } else {
                return c;
            }
        }
    }
    public static int[] getHSV(int[] rgb) {
        int[] _hsv = new int[3];
        int _min = ColorHandler.min(rgb[0], rgb[1], rgb[2]);
        int _max = ColorHandler.max(rgb[0], rgb[1], rgb[2]);
        _hsv[2] = _max;

        int delta = _max - _min;
        if (_max != 0) {
            _hsv[1] = delta/_max;
        } else {
            _hsv[1] = 0;
            _hsv[0] = -1;

            return _hsv;
        }

        if (rgb[0] == _max) {
            _hsv[0] = (rgb[1] - rgb[2]) / delta;
        } else if (rgb[1] == _max) {
            _hsv[0] = 2 + (rgb[2] - rgb[0]) / delta;
        } else {
            _hsv[0] = 4 + (rgb[0] - rgb[1]) / delta;
        }
        _hsv[0] *= 60;
        if (_hsv[0] < 0) {
            _hsv[0] += 360;
        }

        return _hsv;
    }
}
