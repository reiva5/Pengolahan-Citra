package com.example.asus.a06_gabungan.facerecognition;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.util.Log;
import android.util.Pair;

import com.example.asus.a06_gabungan.filterdetection.Filter;

public class Morphological {
    private int[][] diskstructuringElement;
    private int[][] conditionedMatriks;
    private int[][] othertructure;


    public Morphological() {
//        int pxl = Color.rgb(255, 255, 255);
//        Log.d("TEST FUNC", String.valueOf(isWhite(pxl)));
//        dilationResult = new int[3][3];
        setConstant();
    }

    public int[][] tetanggaToMatrix(int x, int y, Bitmap img) {
        int[][] dumm = new int[3][3];
        dumm[0][0] = Color.red(img.getPixel(x - 1, y - 1));
        dumm[0][1] = Color.red(img.getPixel(x, y - 1));
        dumm[0][2] = Color.red(img.getPixel(x + 1, y - 1));
        dumm[1][0] = Color.red(img.getPixel(x - 1, y));
        dumm[1][1] = Color.red(img.getPixel(x, y));
        dumm[1][2] = Color.red(img.getPixel(x + 1, y));
        dumm[2][0] = Color.red(img.getPixel(x - 1, y + 1));
        dumm[2][1] = Color.red(img.getPixel(x, y + 1));
        dumm[2][2] = Color.red(img.getPixel(x + 1, y + 1));

        return dumm;
    }

    private void setConstant() {
        diskstructuringElement = new int[3][];
        diskstructuringElement[0] = new int[]{255, 255, 255};
        diskstructuringElement[1] = new int[]{255, 255, 255};
        diskstructuringElement[2] = new int[]{255, 255, 255};
        othertructure = new int[3][];
        othertructure[0] = new int[]{0, 255, 0};
        othertructure[1] = new int[]{255, 255, 255};
        othertructure[2] = new int[]{0, 255, 0};
        conditionedMatriks = new int[3][3];
    }

    private void resetConditionedMatrix() {
        for (int i = 0; i < 3; ++i) {
            for (int j = 0; j < 3; ++j) {
                conditionedMatriks[i][j] = 0;
            }
        }
    }

    private boolean isMatriksSame(int[][] A, int[][] B) {
        boolean res = true;

        for (int i = 0; i < 3; ++i) {
            for (int j = 0; j < 3; ++j) {
                if (A[i][j] != B[i][j]) {
                    res = false;
                }
            }
        }

        return res;
    }

    private int getWhite() {
        return Color.rgb(255, 255, 255);
    }

    private int generateKernelColor(int in) {
        int r = in;//(in == 1) ? 255 : 0;

        return Color.rgb(r, r, r);
    }
    public Bitmap dilation(Bitmap img, int type) {
        Bitmap newImage = img.copy(img.getConfig(), true);
        int w = img.getWidth();
        int h = img.getHeight();

        for (int y = 1; y < h - 1; ++y) {
            for (int x = 1; x < w - 1; ++x) {
                int pxl = img.getPixel(x, y);
                newImage.setPixel(x, y, pxl);
                if (Color.red(pxl) == 255) { // Warna putih = gambar
                    if (type == 0) {
                        int[][] tetangga = tetanggaToMatrix(x, y, img);
                        int[][] xor = xorOperation(tetangga, othertructure);
                        newImage.setPixel(x - 1, y - 1,
                                generateKernelColor(xor[0][0]));
                        newImage.setPixel(x, y - 1,
                                generateKernelColor(xor[0][1]));
                        newImage.setPixel(x + 1, y - 1,
                                generateKernelColor(xor[0][2]));
                        newImage.setPixel(x - 1, y,
                                generateKernelColor(xor[1][0]));
                        newImage.setPixel(x + 1, y,
                                generateKernelColor(xor[1][2]));
                        newImage.setPixel(x - 1, y + 1,
                                generateKernelColor(xor[2][0]));
                        newImage.setPixel(x, y + 1,
                                generateKernelColor(xor[2][1]));
                        newImage.setPixel(x + 1, y + 1,
                                generateKernelColor(xor[2][2]));
                    } else {
//                        int col = (isWhite(img.getPixel(x-1, y-1))
                        newImage.setPixel(x - 1, y - 1,
                                generateKernelColor(othertructure[0][0]));
                        newImage.setPixel(x, y - 1,
                                generateKernelColor(othertructure[0][1]));
                        newImage.setPixel(x + 1, y - 1,
                                generateKernelColor(othertructure[0][2]));
                        newImage.setPixel(x - 1, y,
                                generateKernelColor(othertructure[1][0]));
                        newImage.setPixel(x + 1, y,
                                generateKernelColor(othertructure[1][2]));
                        newImage.setPixel(x - 1, y + 1,
                                generateKernelColor(othertructure[2][0]));
                        newImage.setPixel(x, y + 1,
                                generateKernelColor(othertructure[2][1]));
                        newImage.setPixel(x + 1, y + 1,
                                generateKernelColor(othertructure[2][2]));
                    }
                }
            }
        }

        return newImage;
    }

    private boolean isWhite(int pixel) {
        return (pixel == 255);
    }
    private int[][] xorOperation(int[][] A, int[][] B) { // Try using dilation
        int[][] res = new int[3][3];

        if (A[1][1] == 255) {
            for (int i = 0; i < 3; ++i) {
                for (int j = 0; j < 3; ++j) {
                    if (i != 1 && j != 1) {
                        if (isWhite(A[i][j]) && isWhite(B[i][j])) {
                            res[i][j] = 255;
                        } else if (!isWhite(A[i][j]) && !isWhite(B[i][j])) {
                            res[i][j] = 0;
                        } else {
                            res[i][j] = 255;
                        }
                    }
                }
            }
        }

        return res;
    }

    public Pair conditionedDilation(Bitmap img) {
        int w = img.getWidth();
        int h = (int) (img.getHeight() / 2);
        int[] firstRow = new int[]{h, 0};
        int[] firstCol = new int[]{w, 0};
        for (int y = 1; y < h - 1; ++y) {
            for (int x = 1; x < w - 1; ++x) {
                int pxl = img.getPixel(x, y);
                if (isWhite(pxl)) {
                    int countFirstRow = 0;
                    if (y < firstRow[0]) {
                        firstRow[0] = y;
                        firstRow[1] = x;
                    }
                    else if (y == firstRow[0]) {
                        int sum = firstRow[1] * countFirstRow;
                        countFirstRow += 1;
                        firstRow[1] = (int) ((sum+ x) / countFirstRow);
                    }
//                    Log.d("TEST ROW", firstRow[0] + " - " + firstRow[1]);
                    int countFirstCol = 0;
                    if (x < firstCol[0]) {
                        firstCol[0] = x;
                        firstCol[1] = y;
                    }
                    else {
                        int sum = firstCol[1] * countFirstCol;
                        countFirstCol += 1;
                        firstCol[1] = (int) ((sum + y) / countFirstCol);
                    }
                }
            }
        }
        Pair x_pair = new Pair(firstCol[0], firstCol[1]);
        Pair y_pair = new Pair(firstRow[1], firstRow[0]);
        return (new Pair(x_pair, y_pair));
    }

    public Bitmap erotion(Bitmap img) {
        Bitmap newImage = img.copy(img.getConfig(), true);
        int w = img.getWidth();
        int h = img.getHeight();

        for (int y = 1; y < h - 1; ++y) {
            for (int x = 1; x < w - 1; ++x) {
                int[][] tetangga = tetanggaToMatrix(x, y, img);
                if (!isMatriksSame(tetangga, diskstructuringElement)) {
                    newImage.setPixel(x, y, generateKernelColor(0));
                }
            }
        }
        return newImage;
    }

}