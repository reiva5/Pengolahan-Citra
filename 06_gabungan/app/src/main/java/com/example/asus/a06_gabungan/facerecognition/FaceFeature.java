package com.example.asus.a06_gabungan.facerecognition;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.util.Log;
import android.util.Pair;

import com.example.asus.a06_gabungan.Point;

import java.util.ArrayList;

public class FaceFeature {
    public ArrayList<Pair<Point, Point>> eyes;
    public Pair<Point, Point> nose;
    public Pair<Point, Point> mouth;
    public Pair<Point, Point> eyeArea;
    public Pair<Point, Point> mouthArea;
    public int faceProperHeight;

    private boolean isInBox(Pair<Point, Point> inner, Pair<Point, Point> outer) {
        return (inner.first.getY() >= outer.first.getY() &&  inner.first.getY() <= outer.second.getY()) ||
                (inner.second.getY() <= outer.second.getY() && inner.second.getY() >= outer.first.getY());
    }
    private String logBatas(Pair<Point, Point> batas) {
        return "(" + batas.first.getX() + ", " + batas.first.getY() + ") - (" +
                batas.second.getX() + ", " + batas.second.getY() + ")";
    }
    private int idxMax(int[] num) {
        int _max = 0;
        for (int i = 1; i < num.length; ++i) {
            if (num[i] > num[_max]) {
                _max = i;
            }
        }
        return _max;
    }
    private int[] max2() {
        int[] res = new int[2];
        int _max = 0;
        for (int i = 1; i < eyes.size(); ++i) {
            if (eyes.get(i).second.getY() > eyes.get(_max).second.getY()) {
                _max = i;
            }
        }
        res[0] = _max;
        if (res[0] == 0) _max = 1;
        else _max = 0;
        for (int i = 0; i < eyes.size(); ++i) {
            if (i != res[0]) {
                if (eyes.get(i).second.getY() > eyes.get(_max).second.getY()) {
                    _max = i;
                }
            }
        }
        res[1] = _max;
        Log.d("MAX MATA", res[0] + " - " + res[1]);

        return res;
    }
    private void selectBestEye() {
        ArrayList<Integer> dumm = new ArrayList<>();
        int [] max = max2();
        for (int i = 0; i < eyes.size(); ++i) {
            if (eyes.get(i).second.getY() < eyes.get(max[0]).second.getY() &&
                    eyes.get(i).second.getY() < eyes.get(max[1]).second.getY()) {
                dumm.add(i);

            }
        }

        ArrayList<Pair<Point, Point>> temp = new ArrayList<>();
        for (int i = 0; i < eyes.size(); ++i) {
            if (!dumm.contains(i)) {
                temp.add(eyes.get(i));
            }
        }
        this.eyes = temp;
    }


    FaceFeature(int[] face, ArrayList<Pair<Point, Point>> features, Bitmap img) {
        eyes = new ArrayList<>();
        int faceHeight = face[3] - face[2];
        int faceWidth = face[1] - face[0];
        float a = (0.25f * faceHeight) + face[2];
        float b = (0.45f * faceHeight) + face[2];
        eyeArea = new Pair<>(new Point(face[0], (int) a), new Point(face[1], (int) b));
        a = (0.75f * faceHeight) + face[2];
        b = (0.9f * faceHeight) + face[2];
        mouthArea = new Pair<>(new Point(face[0], (int) a), new Point(face[1], (int) b));
        for (Pair<Point, Point> batas : features) {
            Log.d("EYE EXTRACTION", ((0.25f * faceHeight) + face[2]) + " -- " + logBatas(batas));

            if (isInBox(batas, eyeArea)) {
                Pair<Point, Point> eye = new Pair<>(new Point(batas.first.getX(), batas.first.getY()),
                        new Point(batas.second.getX(), batas.second.getY()));
                //                break;
                eyes.add(eye);
            }
            if (mouth == null) {
                if (isInBox(batas, mouthArea)) {
                    int rightMouth = face[1] - batas.second.getX();
                    int leftMouth = batas.first.getX() - face[0];
                    if (rightMouth > leftMouth) {
                        batas.second.setX(face[1] - leftMouth);
                    } else {
                        batas.first.setX(face[0] + rightMouth);
                    }

                    int topMouth = batas.first.getY() - mouthArea.first.getY();
                    int bottomMouth =  mouthArea.second.getY() - batas.second.getY();
                    if (topMouth > bottomMouth) {
//                        if (batas.second.getY() < mouthArea.second.getY()) {
                            batas.first.setY(mouthArea.first.getY() + bottomMouth);
//                        }
                    } else {
//                        if (batas.first.getY() > mouthArea.first.getY()) {
                            batas.second.setY(mouthArea.second.getY() - topMouth);
//                        }
                    }

                    mouth = new Pair<>(new Point(batas.first.getX(), batas.first.getY()),
                            new Point(batas.second.getX(), batas.second.getY()));
                }
            }
        }

        if (eyes.size() > 2) {
            selectBestEye();
        }
        if (mouth != null) {
            int y_hidung_atas, y_hidung_bawah, x_hidung_kiri, x_hidung_kanan;
            y_hidung_atas = face[3];
            x_hidung_kiri = face[0];
            x_hidung_kanan = face[1];
            if (eyes.size() == 2) {
                if (eyes.get(0).first.getX() < eyes.get(1).first.getX()) {
                    x_hidung_kiri = eyes.get(0).second.getX();
                    x_hidung_kanan = eyes.get(1).first.getX();
                } else {
                    x_hidung_kiri = eyes.get(1).second.getX();
                    x_hidung_kanan = eyes.get(0).first.getX();
                }
                if (eyes.get(0).first.getY() < eyes.get(1).first.getY()) {
                    y_hidung_atas = eyes.get(0).first.getY();
                } else {
                    y_hidung_atas = eyes.get(1).first.getY();
                }
            } else {
                for (Pair<Point, Point> item : eyes) {
                    if (y_hidung_atas > item.first.getY()) {
                        y_hidung_atas = item.first.getY();
                    }
                    if (x_hidung_kiri < item.second.getX() && item.second.getX() < (face[0] * 2 / 5 + face[1] * 3 / 5)) {
                        x_hidung_kiri = item.second.getX();
                    }
                    if (x_hidung_kanan > item.first.getX() && item.first.getX() > (face[0] * 3 / 5 + face[1] * 2 / 5)) {
                        x_hidung_kanan = item.first.getX();
                    }
                    if (x_hidung_kanan < x_hidung_kiri) {
                        int temp = x_hidung_kanan;
                        x_hidung_kanan = x_hidung_kiri;
                        x_hidung_kiri = temp;
                    }
                }
            }


            y_hidung_bawah = y_hidung_atas + ((mouth.first.getY() - y_hidung_atas));
            nose = new Pair<Point, Point>(new Point(x_hidung_kiri, y_hidung_atas), new Point(x_hidung_kanan, y_hidung_bawah));

            if (nose.second.getX() > mouth.second.getX()) {
                mouth.second.setX(nose.second.getX());
            }
            if (nose.first.getX() < mouth.first.getX()) {
                mouth.first.setX(nose.first.getX());
            }
        }
    }

    private int max(int[] array){
        int result = array[0];
        for(int i=1; i<array.length; i++){
            if (array[i] > result){
                result = array[i];
            }
        }
        return result;
    }

    private int min(int[] array){
        int result = array[0];
        for(int i=1; i<array.length; i++){
            if (array[i] < result){
                result = array[i];
            }
        }
        return result;
    }

    public boolean hasEye() {
        return eyes.size() == 2;
    }

    boolean hasMouth() {
        return mouth != null;
    }

    public boolean hasNose() {
        return nose != null;
    }

    public void printPusat() {
        Log.d("PUSAT - ", "PUSAT");
        // pusat mata
        if (hasEye()) {
            for (int k = 0; k < eyes.size(); ++k) {
                Pair<Point, Point> eye = eyes.get(k);
                Log.d("MATA " + k, "(" +
                        (eye.first.getX() + eye.second.getX()) + ", " +
                        (eye.first.getY() + eye.second.getY())
                );
            }
        }

        if (hasNose()) {
            Log.d("HIDUNG ", "(" +
                    (nose.first.getX() + nose.second.getX()) + ", " +
                    (nose.first.getY() + nose.second.getY())
            );
        }

        if (hasMouth()) {
            Log.d("MULUT ", "(" +
                    (mouth.first.getX() + mouth.second.getX()) + ", " +
                    (mouth.first.getY() + mouth.second.getY())
            );
        }
    }
}
