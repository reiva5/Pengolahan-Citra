package com.example.asus.a06_gabungan.filterdetection;

import android.graphics.Bitmap;
import android.graphics.Color;

import static java.lang.Math.sqrt;

public class Sobel extends Filter {
    private int[][] Gx_sobel;
    private int[][] Gy_sobel;
    final String type = "sobel";

    public Sobel(Bitmap img) {
        super(img);
        Gx_sobel = new int[3][];
        Gx_sobel[0] = new int[]{-1, 0, 1};
        Gx_sobel[1] = new int[]{-2, 0, 2};
        Gx_sobel[2] = new int[]{-1, 0, 1};

        Gy_sobel = new int[3][];
        Gy_sobel[0] = new int[]{-1, -2, -1};
        Gy_sobel[1] = new int[]{0, 0, 0};
        Gy_sobel[2] = new int[]{1, 2, 1};
    }

    public Bitmap getResult() {
        Bitmap dummy = img.copy(img.getConfig(), true);
        int w = dummy.getWidth();
        int h = dummy.getHeight();
        double[] clr = new double[3];
        for (int y = 1; y < h - 1; ++y) {
            for (int x = 1; x < w - 1; ++x) {
                int[][][] tetangga = tetanggaToMatrix(x, y, type);
                int[] kali_x_red = kaliKonvolusiMatrix(Gx_sobel, tetangga);
                int[] kali_y_red = kaliKonvolusiMatrix(Gy_sobel, tetangga);
                for (int i = 0; i < 3; ++i) {
                    clr[i] = sqrt((kali_x_red[i] * kali_x_red[i]) +
                            (kali_y_red[i] * kali_y_red[i]));
                    maxRGB[i] = (int) (threshold_sobel * maxRGB[i]);
                }
                dummy.setPixel(x, y, Color.rgb(max(clr[0], maxRGB[0]),
                        max(clr[1], maxRGB[1]), max(clr[2], maxRGB[2])));
            }
        }
        Bitmap result_image = dummy.copy(dummy.getConfig(), true);

        for(int y=0; y<dummy.getHeight(); y++){
            for(int x=0; x<dummy.getWidth(); x++){
                int pixel = dummy.getPixel(x, y);
                int gray = (Color.red(pixel) + Color.green(pixel) + Color.blue(pixel)) / 3;
                if (gray < 80){
                    result_image.setPixel(x, y, Color.rgb(0, 0, 0));
                }
                else{
                    result_image.setPixel(x, y, Color.rgb(255, 255, 255));
                }
            }
        }

        return result_image;
    }
}
