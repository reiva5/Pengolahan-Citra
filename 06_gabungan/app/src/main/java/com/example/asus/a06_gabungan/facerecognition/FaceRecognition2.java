package com.example.asus.a06_gabungan.facerecognition;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.util.Log;
import android.util.Pair;

import com.example.asus.a06_gabungan.Point;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

public class FaceRecognition2 {
    private int[][] beforeMinkowski;
    private int[][] afterMinkowski;
    private int[] dirX = new int[]{0, 1, 0, -1};
    private int[] dirY = new int[]{1, 0, -1, 0};
    final int VAL_INF = 99999;
    private int height;
    private int width;
    private int[][][] operand = {
            {{1, 1, 1},
                    {1, 1, 1},
                    {1, 1, 1}},

            {{0, 1, 0},
                    {1, 1, 1},
                    {0, 1, 0}},

            {{1, 1, 1, 1, 1},
                    {1, 1, 1, 1, 1},
                    {1, 1, 1, 1, 1},
                    {1, 1, 1, 1, 1},
                    {1, 1, 1, 1, 1}},

            {{0, 0, 1, 0, 0},
                    {0, 1, 1, 1, 0},
                    {1, 1, 1, 1, 1},
                    {0, 1, 1, 1, 1},
                    {0, 0, 1, 1, 0}},

            {{0, 0, 1, 1, 0, 0},
                    {0, 1, 1, 1, 1, 0},
                    {1, 1, 1, 1, 1, 1},
                    {1, 1, 1, 1, 1, 1},
                    {0, 1, 1, 1, 1, 0},
                    {0, 0, 1, 1, 0, 0}}};

    int max(int[] array){
        int result = array[0];
        for(int i=1; i<array.length; i++){
            if (array[i] > result){
                result = array[i];
            }
        }
        return result;
    }

    int min(int[] array){
        int result = array[0];
        for(int i=1; i<array.length; i++){
            if (array[i] < result){
                result = array[i];
            }
        }
        return result;
    }

    Boolean validBrown(int[] colorPallete) {
        int r = colorPallete[0];
        int g = colorPallete[1];
        int b = colorPallete[2];
        int[] YCbCr = ColorHandler.getYCbCr(r, g, b);
//        int[] HSV = ColorHandler.getHSV(colorPallete);
        int max = max(colorPallete);
        int min = min(colorPallete);
//
//        return 95 < colorPallete[0] && colorPallete[0] <= 256 &&
//                40 < colorPallete[1] && colorPallete[1] <= 256 &&
//                20 < colorPallete[2] && colorPallete[2] <= 256 &&
//                colorPallete[0] > colorPallete[1] &&
//                colorPallete[0] > colorPallete[2] &&
//                max - min < 175 &&
//                (colorPallete[0] - colorPallete[1]) > 15;
        int r_g = r - g;
        if (r_g < 0) r_g *= -1;
        return r > 95 &&
                g > 40 &&
                b > 20 &&
                r > g &&
                r > b &&
                r_g > 15 &&
                YCbCr[2] > 135 &&
                YCbCr[1] > 85 &&
                YCbCr[0] > 80 &&
                YCbCr[2] <= (1.5862 * YCbCr[1]) + 20 &&
                YCbCr[2] >= (0.3448 * YCbCr[1]) + 76.2069 &&
                YCbCr[2] >= (-4.5652 * YCbCr[1]) + 234.5652 &&
                YCbCr[2] <= (-1.15 * YCbCr[1]) + 301.75 &&
                YCbCr[2] <= (-2.2857 * YCbCr[1]) + 432.85;
//        return (YCbCr[0] > 80 && YCbCr[1] > 85 && YCbCr[1] < 135 &&
//                YCbCr[2] > 135 && YCbCr[2] < 180);


    }

    private void dilate(int _x, int _y, int size) {
        int mid = operand.length / 2;
        for(int i = 0; i < operand[size].length; i++) {
            int x = _x + i - mid;
            for(int j = 0; j < operand[size].length; j++) {
                int y = _y + j - mid;
                if(x >= 0 && y >= 0 && x < beforeMinkowski.length && y < beforeMinkowski[0].length) {
                    if(operand[size][i][j] == 1) {
                        afterMinkowski[x][y] = operand[size][i][j] == 1 ? Color.WHITE : Color.BLACK;
                    }
                }
            }
        }
    }

    public Bitmap dilation(Bitmap img, int size) {
        Bitmap output = img.copy(img.getConfig(), true);
        final int height = img.getHeight();
        final int width = img.getWidth();

        beforeMinkowski = new int[width][height];
        afterMinkowski = new int[width][height];
        for(int i = 0; i < width; i++) {
            for(int j = 0; j < height; j++) {
                beforeMinkowski[i][j] = img.getPixel(i, j);
            }
        }
        for(int i = 0; i < width; i++) {
            for(int j = 0; j < height; j++) {
                if(beforeMinkowski[i][j] == Color.WHITE) {
                    dilate(i, j, size);
                } else {
                    afterMinkowski[i][j] = beforeMinkowski[i][j];
                }
            }
        }
        for(int i = 0; i < width; i++) {
            for(int j = 0; j < height; j++) {
                output.setPixel(i, j, afterMinkowski[i][j]);
            }
        }
        return output;
    }

    public Bitmap toBlackWhite(Bitmap img) {
        Bitmap dummy = img.copy(img.getConfig(), true);
        int height = img.getHeight();
        int width = img.getWidth();
        int[][] type = new int[height][width];
        for (int i = 0; i < height; ++i) {
            for (int j = 0; j < width; ++j) {
//                type[i][j] = -1;
                dummy.setPixel(j, i, Color.rgb(0,0,0));
            }
        }
        for (int i = 0; i < height; ++i) {
            for (int j = 0; j < width; ++j) {
                int pixel = img.getPixel(j, i);
                if (validBrown(new int[]{Color.red(pixel),
                        Color.green(pixel),
                        Color.blue(pixel)})) {
                    dummy.setPixel(j, i, Color.rgb(255,255,255));
                }

            }
        }

        return dummy;
    }

    private void erode(int _x, int _y, int size) {
        boolean fit = true;
        int mid = operand.length / 2;
        for(int i = 0; i < operand[size].length; i++) {
            int x = _x + i - mid;
            for(int j = 0; j < operand[size].length; j++) {
                int y = _y + j - mid;
                if(x >= 0 && y >= 0 && x < beforeMinkowski.length && y < beforeMinkowski[0].length) {
                    if (operand[size][i][j] == 1) {
                        if (beforeMinkowski[x][y] != (operand[size][i][j] == 1 ? Color.WHITE : Color.BLACK))
                            fit = false;
                    }
                } else {
                    fit = false;
                }
                if (!fit) break;
            }
            if (!fit) break;
        }
        afterMinkowski[_x][_y] = fit ? Color.WHITE : Color.BLACK;
    }

    public Bitmap erosion(Bitmap input, int size) {
        Bitmap output = Bitmap.createBitmap(input.getWidth(), input.getHeight(), Bitmap.Config.ARGB_8888);
        final int height = input.getHeight();
        final int width = input.getWidth();

        beforeMinkowski = new int[width][height];
        afterMinkowski = new int[width][height];
        for(int i = 0; i < width; i++) {
            for(int j = 0; j < height; j++) {
                beforeMinkowski[i][j] = input.getPixel(i, j);
            }
        }
        for(int i = 0; i < width; i++) {
            for(int j = 0; j < height; j++) {
                if(beforeMinkowski[i][j] == Color.WHITE) {
                    erode(i, j, size);
                } else {
                    afterMinkowski[i][j] = beforeMinkowski[i][j];
                }
            }
        }
        for(int i = 0; i < width; i++) {
            for(int j = 0; j < height; j++) {
                output.setPixel(i, j, afterMinkowski[i][j]);
            }
        }
        return output;
    }
    Boolean valid(Point point) {
        return point.getX() < width &&
                point.getX() >= 0 &&
                point.getY() < height &&
                point.getY() >= 0;
    }
    Boolean validRed(int [] colorPallete){
        return  colorPallete[0] >= 128
                && colorPallete[0] >= colorPallete[1]
                && colorPallete[0] >= colorPallete[2]
                && (float)colorPallete[0]/(colorPallete[0]-colorPallete[1]) < 3.2
                && (float)colorPallete[0]/(colorPallete[0]-colorPallete[2]) < 3.2;
//                && colorPallete[1]-colorPallete[2] <= 15
//                && colorPallete[1]-colorPallete[2] >= -15;
    }

    public Bitmap process(Bitmap img, Bitmap originImg) {
        Bitmap dummy = img.copy(img.getConfig(), true);
        height = img.getHeight();
        width = img.getWidth();
        int[][] type = new int[height][width];
        for (int i = 0; i < height; ++i) {
            for (int j = 0; j < width; ++j) {
                type[i][j] = -1;
            }
        }
        int tot = 0;
        for (int i = 0; i < height; ++i) {
            for (int j = 0; j < width; ++j) {
                int pixel = img.getPixel(j, i);
                if (pixel == Color.WHITE) {
                    if (tot == 0) {
                        tot++;
                    }
                    type[i][j] = 0;
                }
            }
        }
        Boolean visitOuterZone = false;
        int counter = 2;
        for (int i = 0; i < height; ++i) {
            for (int j = 0; j < width; ++j) {
                if (type[i][j] == 0) {
                    Queue<Point> q = new LinkedList<>();
                    q.add(new Point(j,i));
                    Point curr;
                    while (!q.isEmpty()) {
                        curr = q.poll();
                        if (type[curr.getY()][curr.getX()] == 0) {
                            type[curr.getY()][curr.getX()] = counter;
                            for (int k = 0; k < dirX.length; ++k) {
                                Point next = new Point(curr.getX() + dirX[k], curr.getY() + dirY[k]);
                                if (valid(next) && type[next.getY()][next.getX()] == 0) {
                                    q.add(next);
                                }
                            }
                        }
                    }
                    ++counter;
                } else if (!visitOuterZone) {
                    visitOuterZone = true;
                    Queue<Point> q = new LinkedList<>();
                    q.add(new Point(j,i));
                    Point curr;
                    while (!q.isEmpty()) {
                        curr = q.poll();
                        if (type[curr.getY()][curr.getX()] == -1) {
                            type[curr.getY()][curr.getX()] = -VAL_INF;
                            for (int k = 0; k < dirX.length; ++k) {
                                Point next = new Point(curr.getX() + dirX[k], curr.getY() + dirY[k]);
                                if (valid(next) && type[next.getY()][next.getX()] == -1) {
                                    q.add(next);
                                }
                            }
                        }
                    }
                }
            }
        }
        Boolean[] isFace = new Boolean[counter+1];
        int[] feature = new int[counter+1];
        ArrayList<Pair<Point,Point>>[] features = new ArrayList[counter+1];
        for (int i = 0; i < isFace.length; ++i) {
            isFace[i] = false;
            feature[i] = 0;
            features[i] = new ArrayList<>();
        }

        for (int i = 0; i < height; ++i) {
            for (int j = 0; j < width; ++j) {
                if (type[i][j] == -1) {
                    int parent = -1;
                    for (int k = 0; k < dirX.length; ++k) {
                        Point adj = new Point(j + dirX[k], i + dirY[k]);
                        if (valid(adj) && type[adj.getY()][adj.getX()] > 0) {
                            parent = type[adj.getY()][adj.getX()];
                            break;
                        }
                    }
                    if (parent == -1) continue;
                    int maxX, maxY, minX, minY;
                    Queue<Point> q = new LinkedList<>();
                    q.add(new Point(j,i));
                    maxX = j;
                    maxY = i;
                    minX = j;
                    minY = i;
                    Point curr;
                    while (!q.isEmpty()) {
                        curr = q.poll();
                        if (type[curr.getY()][curr.getX()] == -1) {
                            type[curr.getY()][curr.getX()] = -parent;
                            for (int k = 0; k < dirX.length; ++k) {
                                Point next = new Point(curr.getX() + dirX[k], curr.getY() + dirY[k]);
                                if (valid(next) && type[next.getY()][next.getX()] == -1) {
                                    q.add(next);
                                    maxX = Math.max(maxX, next.getX());
                                    minX = Math.min(minX, next.getX());
                                    maxY = Math.max(maxY, next.getY());
                                    minY = Math.min(minY, next.getY());
                                }
                            }
                        }
                    }
                    ++feature[parent];
                    features[parent].add(new Pair<>(new Point(minX, minY), new Point(maxX, maxY)));
                }
            }
        }
        for (int i = 1; i < counter; ++i) {
            isFace[i] = feature[i] > 1;
        }
        int[][] batasArea = new int[counter+1][4];
        for (int i = 1; i < counter; ++i) {
            batasArea[i][0] = width;
            batasArea[i][1] = -1;
            batasArea[i][2] = height;
            batasArea[i][3] = -1;
        }
        for (int i = 0; i < height; ++i) {
            for (int j = 0; j < width; ++j) {
                if (type[i][j] > 0) {
                    batasArea[type[i][j]][0] = Math.min(batasArea[type[i][j]][0], j);
                    batasArea[type[i][j]][1] = Math.max(batasArea[type[i][j]][1], j);
                    batasArea[type[i][j]][2] = Math.min(batasArea[type[i][j]][2], i);
                    batasArea[type[i][j]][3] = Math.max(batasArea[type[i][j]][3], i);
                }
            }
        }
        dummy = originImg.copy(originImg.getConfig(), true);
        for (int i = 1; i < counter; ++i) {
            if (!isFace[i]) continue;
            batasArea[i][3] = batasArea[i][2] + ((batasArea[i][1] - batasArea[i][0]) * 5 + 1)/ 4;
//            for (Pair<Point, Point> batas : features[i]) {
//                for (int y = batas.first.getY(); y <= batas.second.getY(); ++y) {
//                    for (int x = batas.first.getX(); x <= batas.second.getX(); ++x) {
//                        if (x == batas.first.getX() || x == batas.second.getX() || y == batas.first.getY() ||
//                                y == batas.second.getY()){
//                            dummy.setPixel(x, y, Color.rgb(255,0,0));
//                        }
//                    }
//                }
//            }
            FaceFeature ff = new FaceFeature(batasArea[i], features[i], originImg);
//            for (int y = ff.eyeArea.first.getY(); y <= ff.eyeArea.second.getY(); ++y) {
//                for (int x = ff.eyeArea.first.getX(); x <= ff.eyeArea.second.getX(); ++x) {
//                    if (x == ff.eyeArea.first.getX() || x == ff.eyeArea.second.getX() || y == ff.eyeArea.first.getY() ||
//                            y == ff.eyeArea.second.getY()) {
//                        dummy.setPixel(x, y, Color.rgb(0, 0, 255));
//                    }
//                }
//            }
//            if (ff.hasEye()) {
                // CONFIGURE FACE SIZE
                for (int k = 0; k < ff.eyes.size(); ++k) {
                    Pair<Point, Point> eye = ff.eyes.get(k);
                    for (int y = eye.first.getY(); y <= eye.second.getY(); ++y) {
                        for (int x = eye.first.getX(); x <= eye.second.getX(); ++x) {
                            if (x == eye.first.getX() || x == eye.second.getX() || y == eye.first.getY() ||
                                    y == eye.second.getY()) {
                                dummy.setPixel(x, y, Color.rgb(255, 0, 0));
                            }
                        }
                    }
//                }
            }
//            for (int y = ff.mouthArea.first.getY(); y <= ff.mouthArea.second.getY(); ++y) {
//                for (int x = ff.mouthArea.first.getX(); x <= ff.mouthArea.second.getX(); ++x) {
//                    if (x == ff.mouthArea.first.getX() || x == ff.mouthArea.second.getX() || y == ff.mouthArea.first.getY() ||
//                            y == ff.mouthArea.second.getY()) {
//                        dummy.setPixel(x, y, Color.rgb(0, 0, 255));
//                    }
//                }
//            }
            if (ff.hasNose()) {
                for (int y = ff.nose.first.getY(); y <= ff.nose.second.getY(); ++y) {
                    for (int x = ff.nose.first.getX(); x <= ff.nose.second.getX(); ++x) {
                        if (x == ff.nose.first.getX() || x == ff.nose.second.getX() || y == ff.nose.first.getY() ||
                                y == ff.nose.second.getY()) {
                            dummy.setPixel(x, y, Color.rgb(255, 0, 0));
                        }
                    }
                }
            }
            if (ff.hasMouth()) {
                for (int y = ff.mouth.first.getY(); y <= ff.mouth.second.getY(); ++y) {
                    for (int x = ff.mouth.first.getX(); x <= ff.mouth.second.getX(); ++x) {
                        if (x == ff.mouth.first.getX() || x == ff.mouth.second.getX() || y == ff.mouth.first.getY() ||
                                y == ff.mouth.second.getY()) {
                            dummy.setPixel(x, y, Color.rgb(255, 0, 0));
                        }
                    }
                }
            }
            for (int y = batasArea[i][2]; y <= batasArea[i][3]; ++y) {
                for (int x = batasArea[i][0]; x <= batasArea[i][1]; ++x) {
                    if (x == batasArea[i][0] || x  == batasArea[i][1] || y == batasArea[i][2] ||
                            y == batasArea[i][3]){
                        dummy.setPixel(x, y, Color.rgb(0,255,0));
                    }
                }
            }
            ff.printPusat();
        }

        return dummy;


    }

}
