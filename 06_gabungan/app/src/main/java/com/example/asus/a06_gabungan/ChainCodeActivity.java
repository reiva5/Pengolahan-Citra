package com.example.asus.a06_gabungan;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class ChainCodeActivity extends AppCompatActivity {
    ImageView mImageView;
    Bitmap img;

    private int[] red = new int[256];
    private int[] green = new int[256];
    private int[] blue = new int[256];

    public static final int BORDER = 0;
    public static final int WHITE = 1;
    public static final int BLACK = 2;

    public static final int[] dirX = {1, 1, 0, -1, -1, -1, 0, 1};
    public static final int[] dirY = {0, 1, 1, 1, 0, -1, -1, -1};

    int width, height;
    private int[][] grayscale;
    private ArrayList<Integer> chaincode;
    private boolean[][] visited;
    private int[][] types;

    private float jumlah_timur;
    private float jumlah_selatan;
    private float jumlah_barat;
    private float jumlah_utara;
    private float jumlah_timur_selatan;
    private float jumlah_barat_selatan;
    private float jumlah_barat_utara;
    private float jumlah_timur_utara;
    private int nilai_gambar;
    private TextView predictTextView;

    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chaincode);

        setImage();

        Button chainCodeButton = (Button) findViewById(R.id.button_chaincode);
        chainCodeButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        getChainCode();
                        analyzeChainCode();
                    }
                }
        );
    }

    public void getChainCode() {
        Boolean done = false;
        getBitmapGrayscale(img);
        for (int y = 0; y < height && !done; ++y) {
            for (int x = 0; x < width && !done; ++x) {
                if (types[x][y] == BORDER) { // BORDER TYPE
                    kelilingi(x, y);
                    Log.d("Hehe", "Masuk sini gan");
                    done = true;
                }
            }
        }
//        Toast.makeText(this, chaincode.toString(), Toast.LENGTH_LONG).show();
//        Log.d("Bebas", chaincode.toString());
        ImageView res = findViewById(R.id.test);
    }

    public void kelilingi(int startX, int startY) {
        chaincode = new ArrayList<>();
        dfs(startX, startY);
    }

    private void dfs(int startX, int startY) {
        if (!visited[startX][startY]) {
            visited[startX][startY] = true;
            for (int i = 0; i < dirX.length; i += 2) {
                int x = startX + dirX[i];
                int y = startY + dirY[i];
                if (valid(x, y) && !visited[x][y] && types[x][y] == BORDER) {
                    chaincode.add(i);
                    dfs(x,y);
                    break;
                }
            }
        }
    }

    public void analyzeChainCode(){
        jumlah_timur = 0;
        jumlah_selatan = 0;
        jumlah_barat = 0;
        jumlah_utara = 0;
        jumlah_timur_selatan = 0;
        jumlah_barat_selatan = 0;
        jumlah_barat_utara = 0;
        jumlah_timur_utara = 0;

        int size_chaincode = chaincode.size();
        Log.d("chaincodelog : ", ""+size_chaincode);
        if (size_chaincode >= 1){
            for(int i=0; i<size_chaincode; ++i){
                Log.d("chaincodelog : ", ""+chaincode.get(i));
                // Hitung jumlah Timur
                if (chaincode.get(i).equals(0)){
                    jumlah_timur++;
                }
                // Hitung jumlah Selatan
                if (chaincode.get(i).equals(2)){
                    jumlah_selatan++;
                }
                // Hitung jumlah Barat
                if (chaincode.get(i).equals(4)){
                    jumlah_barat++;
                }
                // Hitung jumlah Utara
                if (chaincode.get(i).equals(6)){
                    jumlah_utara++;
                }
                // Hitung jumlah Timur-Selatan Berurutan
                if (i>0 && chaincode.get(i).equals(0) && chaincode.get(i-1).equals(2)){
                    jumlah_timur_selatan++;
                }
                // Hitung jumlah Barat-Selatan Berurutan
                if (i>0 && chaincode.get(i).equals(4) && chaincode.get(i-1).equals(2)){
                    jumlah_barat_selatan++;
                }
                // Hitung jumlah Barat-Utara Berurutan
                if (i>0 && chaincode.get(i ).equals(4) && chaincode.get(i-1).equals(6)){
                    jumlah_barat_utara++;
                }
                // Hitung jumlah Timur-Utara Berurutan
                if (i>0 && chaincode.get(i).equals(0) && chaincode.get(i-1).equals(6)){
                    jumlah_timur_utara++;
                }
            }
            jumlah_timur = jumlah_timur/size_chaincode;
            jumlah_selatan = jumlah_selatan/size_chaincode;
            jumlah_barat = jumlah_barat/size_chaincode;
            jumlah_utara = jumlah_utara/size_chaincode;
            jumlah_timur_selatan = jumlah_timur_selatan/size_chaincode;
            jumlah_barat_selatan = jumlah_barat_selatan/size_chaincode;
            jumlah_barat_utara = jumlah_barat_utara/size_chaincode;
            jumlah_timur_utara = jumlah_timur_utara/size_chaincode;
//            Log.d("chaincodelog", "jumlah_timur : "+jumlah_timur);
//            Log.d("chaincodelog", "jumlah_selatan : "+jumlah_selatan);
//            Log.d("chaincodelog", "jumlah_barat : "+jumlah_barat);
//            Log.d("chaincodelog", "jumlah_utara : "+jumlah_utara);
//            Log.d("chaincodelog", "jumlah_timur_selatan : "+jumlah_timur_selatan);
//            Log.d("chaincodelog", "jumlah_barat_selatan : "+jumlah_barat_selatan);
//            Log.d("chaincodelog", "jumlah_barat_utara : "+jumlah_barat_utara);
//            Log.d("chaincodelog", "jumlah_timur_utara : "+jumlah_timur_utara);
        }

        boolean bool_1 = approximate(jumlah_utara+jumlah_selatan, 0.76f);
        boolean bool_2 = approximate(jumlah_barat_selatan+jumlah_timur_utara, 0.214f)
                && jumlah_barat_utara != 0;
        boolean bool_3 = approximate(jumlah_timur+jumlah_barat, 0.52f);
        boolean bool_4 = approximate(jumlah_utara+jumlah_selatan, 0.59f);
        boolean bool_5 = approximate(jumlah_timur+jumlah_barat, 0.548f);

        boolean bool_7 = approximate(jumlah_timur_selatan, 0) && approximate(jumlah_barat_utara, 0)
                && approximate(jumlah_timur, 0.3f);
        boolean bool_8 = approximate(jumlah_timur_selatan+
                jumlah_barat_selatan+jumlah_barat_utara+ jumlah_timur_utara, 0.29f);
        boolean bool_0 = approximate(jumlah_utara+jumlah_selatan, 0.61f);

        boolean bool_6 = jumlah_timur_selatan > jumlah_barat_utara;
        boolean bool_9 = approximate(jumlah_timur+jumlah_barat, 0.478f);

        predictTextView = findViewById(R.id.predict_text_view);
        if(bool_1){
            predictTextView.setText("Prediksi : 1");
        } else if (bool_2){
            predictTextView.setText("Prediksi : 2");
        } else if (bool_3) {
            predictTextView.setText("Prediksi : 3");
        } else if (bool_4) {
            predictTextView.setText("Prediksi : 4");
        } else if (bool_5){
            predictTextView.setText("Prediksi : 5");
        } else if (bool_7){
            predictTextView.setText("Prediksi : 7");
        } else if (bool_8){
            predictTextView.setText("Prediksi : 8");
        } else if (bool_0){
            predictTextView.setText("Prediksi : 0");
        } else if (bool_6){
            predictTextView.setText("Prediksi : 6");
        } else if (bool_9){
            predictTextView.setText("Prediksi : 9");
        }
    }

    public boolean approximate(float number, float constant){
        return number <= constant + 0.01 && number >= constant - 0.01;
    }


    private void setImage(){
        mImageView = findViewById(R.id.image);
        byte[] byteArray = getIntent().getByteArrayExtra("image");
        img = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
        mImageView.setImageBitmap(img);
        getBitmapPixel(img);
    }

    public void getBitmapPixel(Bitmap bitmap) {
        width = bitmap.getWidth();
        height = bitmap.getHeight();
        int pixel;
        for (int i = 0; i < 256; i++) {
            red[i] = 0;
            green[i] = 0;
            blue[i] = 0;
        }

        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                pixel = bitmap.getPixel(i, j);
                red[Color.red(pixel)]++;
                green[Color.green(pixel)]++;
                blue[Color.blue(pixel)]++;
            }
        }
    }

    public void getBitmapGrayscale(Bitmap bitmap) {
        width = bitmap.getWidth();
        height = bitmap.getHeight();
        grayscale = new int[width][height];
        visited = new boolean[width][height];
        types = new int[width][height];

        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                visited[i][j] = false;
                int pixel = bitmap.getPixel(i, j);
                int sum = Color.red(pixel) + Color.green(pixel) + Color.blue(pixel);
                grayscale[i][j] = sum  / 3;
                if (grayscale[i][j] >= 127) types[i][j] = WHITE;
                else types[i][j] = BLACK;
            }
        }

        for (int y = 0; y < height; ++y) {
            for (int x = 0; x < width; ++x) {
                if (types[x][y] == BLACK) continue;
                Boolean exist = false;
                for (int k = 0; k < dirX.length; ++k) {
                    int currX = x + dirX[k];
                    int currY = y + dirY[k];
                    if (valid(currX, currY) && types[currX][currY] == BLACK) {
                        exist = true;
                    }
                }
                if (exist) types[x][y] = BORDER;
            }
        }

        for (int y = 0; y < height; ++y) {
            for (int x = 0; x < width; ++x) {
                if (types[x][y] == WHITE) System.err.print(' ');
                else if (types[x][y] == BORDER) System.err.print('-');
                else System.err.print('*');
            }
            System.err.println();
        }
    }

    private Boolean valid(int i, int j) {
        return i >= 0 && j >= 0 && i < visited.length && j < visited[i].length;
    }
}
