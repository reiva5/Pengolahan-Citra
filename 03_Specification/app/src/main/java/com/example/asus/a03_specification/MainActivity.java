package com.example.asus.a03_specification;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatSeekBar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class MainActivity extends AppCompatActivity {
    static final int REQUEST_TAKE_PHOTO = 1;
    private static final int CAMERA_REQUEST = 1888;
    private static final int MY_CAMERA_PERMISSION_CODE = 100;
    static final int REQUEST_IMAGE_CAPTURE = 1;
    static final String LOG_TAG = "MainClass";

    static final int PICK_IMAGE_REQUEST = 2;
    private int width, height;
    private float[] input_red = new float[256];
    private float[] input_green = new float[256];
    private float[] input_blue = new float[256];
    private float[] user_red = new float[256];
    private float[] user_green = new float[256];
    private float[] user_blue = new float[256];
    private TextView angka_pertama_edit;
    private TextView angka_kedua_edit;
    private TextView angka_ketiga_edit;
    private Bitmap img;
    ImageView newImageView;
    private String mCurrentPhotoPath;
    private ImageView mImageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        angka_pertama_edit = (TextView) findViewById(R.id.angka_pertama);
        angka_kedua_edit = (TextView) findViewById(R.id.angka_kedua);
        angka_ketiga_edit = (TextView) findViewById(R.id.angka_ketiga);
        SeekBar seekBar = findViewById(R.id.angka_pertama_slider);
        SeekBar seekBar2 = findViewById(R.id.angka_kedua_slider);
        SeekBar seekBar3 = findViewById(R.id.angka_ketiga_slider);
        angka_pertama_edit.setText(String.valueOf(seekBar.getProgress()));
        angka_kedua_edit.setText(String.valueOf(seekBar2.getProgress()));
        angka_ketiga_edit.setText(String.valueOf(seekBar3.getProgress()));
        seekBar.setOnSeekBarChangeListener(
                new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                        angka_pertama_edit.setText(String.valueOf(i));
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                    }
                }
        );
        seekBar2.setOnSeekBarChangeListener(
                new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                        angka_kedua_edit.setText(String.valueOf(i));
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                    }
                }
        );
        seekBar3.setOnSeekBarChangeListener(
                new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                        angka_ketiga_edit.setText(String.valueOf(i));
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                    }
                }
        );
        dispatchTakePictureIntent();
        getPhotoFromFile();
        executeUserButton();
        makeNewImage();
    }

    private void getPhotoFromFile(){
        Button getPhotoFromFileButton = findViewById(R.id.get_button);

        getPhotoFromFileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent photoIntent = new Intent();
                photoIntent.setType("image/*");
                photoIntent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(photoIntent, "Select Picture"),
                        PICK_IMAGE_REQUEST);
            }
        });
    }

    private void dispatchTakePictureIntent(){
        Button takePictureButton = findViewById(R.id.take_picture_button);

        takePictureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (ContextCompat.checkSelfPermission(v.getContext(), Manifest.permission.CAMERA)
//                        != PackageManager.PERMISSION_GRANTED) {
//                    Log.d("Test 1", "No Permission granted");
//                    ActivityCompat.requestPermissions((Activity) v.getContext(), new String[]{Manifest.permission.CAMERA},
//                            MY_CAMERA_PERMISSION_CODE);
//                } else {
                    Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    File photoFile = null;
                    try {
                        photoFile = createImageFile();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                    if (photoFile != null) {
                        Uri photoURI = FileProvider.getUriForFile(v.getContext(),
                                "com.example.asus.a03_specification.fileprovider",
                                photoFile);
                        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                        startActivityForResult(cameraIntent, CAMERA_REQUEST);
                    }

//                    startActivityForResult(cameraIntent, CAMERA_REQUEST);
//                }
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        mImageView = (ImageView) findViewById(R.id.photo_view);
        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK){
//            Bundle extras = data.getExtras();
            assert data != null;
            setPic();
//            makeCombinedChart();
            makeHistogram(input_red, 256, R.id.histogram_input_red);
            makeHistogram(input_green, 256, R.id.histogram_input_green);
            makeHistogram(input_blue, 256, R.id.histogram_input_blue);
        } else if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK){
            Uri uri = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                getBitmapPixel(bitmap);
                mImageView.setImageBitmap(bitmap);
                img = bitmap;
//                makeCombinedChart();
                makeHistogram(input_red, 256, R.id.histogram_input_red);
                makeHistogram(input_green, 256, R.id.histogram_input_green);
                makeHistogram(input_blue, 256, R.id.histogram_input_blue);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void setPic() {
        // Get the dimensions of the View
        int targetW = 300;
        int targetH = 400;

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW/targetW, photoH/targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        getBitmapPixel(bitmap);
        mImageView.setImageBitmap(bitmap);
        img = bitmap;
    }

    public void executeUserButton(){
        Button userHistogramButton = findViewById(R.id.user_histogram_button);
        userHistogramButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                float angka_pertama = Float.parseFloat(angka_pertama_edit.getText().toString());
                float angka_kedua = Float.parseFloat(angka_kedua_edit.getText().toString());
                float angka_ketiga = Float.parseFloat(angka_ketiga_edit.getText().toString());

                getUserHistogramArray(angka_pertama,angka_kedua,angka_ketiga);
                makeHistogram(user_red, 256, R.id.histogram_user_red);
                makeHistogram(user_green, 256, R.id.histogram_user_green);
                makeHistogram(user_blue, 256, R.id.histogram_user_blue);
            }
        });
    }

    public void getBitmapPixel(Bitmap bitmap) {
        width = bitmap.getWidth();
        height = bitmap.getHeight();
        int pixel;
        for (int i = 0; i < 256; i++) {
            input_red[i] = 0;
            input_green[i] = 0;
            input_blue[i] = 0;
        }

        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                pixel = bitmap.getPixel(i, j);
                input_red[Color.red(pixel)]++;
                input_green[Color.green(pixel)]++;
                input_blue[Color.blue(pixel)]++;
            }
        }
//        Log.d(LOG_TAG, String.valueOf(red[100]));
//        Log.d(LOG_TAG, String.valueOf(green[100]));
//        Log.d(LOG_TAG, String.valueOf(blue[100]));
    }

    public void getUserHistogramArray(float angka_pertama, float angka_kedua, float angka_ketiga){
        for (int i = 0; i < 256; i++) {
            user_red[i] = 0;
            user_blue[i] = 0;
            user_green[i] = 0;
        }

        for(int i=0; i<128; i++){
            user_red[i] = angka_pertama + (i * (angka_kedua - angka_pertama))/127;
            user_blue[i] = angka_pertama + (i * (angka_kedua - angka_pertama))/127;
            user_green[i] = angka_pertama + (i * (angka_kedua - angka_pertama))/127;
        }
        for(int i=128; i<255; i++){
            user_red[i] = angka_kedua + ((i-127) * (angka_ketiga - angka_kedua))/127;
            user_blue[i] = angka_kedua + ((i-127) * (angka_ketiga - angka_kedua))/127;
            user_green[i] = angka_kedua + ((i-127) * (angka_ketiga - angka_kedua))/127;
        }

//        int[] map = mapOriginalHistogram(roundCumulativeArray(input_red), roundCumulativeArray(user_red));
//        for(int i=0; i<256; i++){
//            Log.d(LOG_TAG, "isi map["+i+"] : "+map[i]);
//        }
    }

    public void makeHistogram(float[] arr, int length, int idHistogram){
        ArrayList<BarEntry> barEntries = new ArrayList<>();
        ArrayList<String> xAxis = new ArrayList<>();

        for (int i=0; i<length; i++){
            barEntries.add(new BarEntry(i, arr[i]));
            xAxis.add(""+i);
        }
        BarDataSet barDataSet = new BarDataSet(barEntries, "");

        if (idHistogram == R.id.histogram_input_red || idHistogram == R.id.histogram_user_red ||
                idHistogram == R.id.histogram_result_red){
            barDataSet.setLabel("red");
            barDataSet.setColor(Color.rgb(255, 0,0));
        }
        else if (idHistogram == R.id.histogram_input_green ||
                    idHistogram == R.id.histogram_user_green ||
                    idHistogram == R.id.histogram_result_green){
            barDataSet.setLabel("green");
            barDataSet.setColor(Color.rgb(0, 255,0));
        }
        else if (idHistogram == R.id.histogram_input_blue || idHistogram == R.id.histogram_user_blue
                    || idHistogram == R.id.histogram_result_blue){
            barDataSet.setLabel("blue");
            barDataSet.setColor(Color.rgb(0, 0,255));
        }

        BarData barData = new BarData(barDataSet);

        BarChart barChart = (BarChart) findViewById(idHistogram);
        barChart.setVisibility(View.VISIBLE);
        barChart.setData(barData);

        barChart.getDescription().setEnabled(false);
        barChart.getXAxis().setDrawGridLines(false);

        barChart.getAxisLeft().setDrawLabels(false);
        barChart.getAxisRight().setDrawLabels(false);
        barChart.getXAxis().setDrawLabels(false);
        barChart.getLegend().setEnabled(true);

        barChart.setTouchEnabled(true);
        barChart.setDragEnabled(true);
        barChart.setScaleEnabled(true);
        //        barChart.getAxisLeft().setDrawGridLines(false);
        //        barChart.getAxisRight().setDrawGridLines(false);
    }

    private void makeNewImage(){
        newImageView = findViewById(R.id.new_image);
        Button newImageButton = findViewById(R.id.make_new_image_button);

        newImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int[] map_red = mapOriginalHistogram(roundCumulativeArray(input_red), roundCumulativeArray(user_red));
                int[] map_green = mapOriginalHistogram(roundCumulativeArray(input_green), roundCumulativeArray(user_green));
                int[] map_blue = mapOriginalHistogram(roundCumulativeArray(input_blue), roundCumulativeArray(user_blue));
                Bitmap hasil = img.copy(img.getConfig(), true);

                float[] new_red = new float[256];
                float[] new_green= new float[256];
                float[] new_blue = new float[256];

                for(int i=0; i<width; i++){
                    for(int j=0; j<height; j++){
                        int pixel = hasil.getPixel(i, j);
                        int newPixel = mapNewPixel(pixel, map_red, map_green, map_blue);
                        hasil.setPixel(i, j, newPixel);
                        new_red[Color.red(newPixel)]++;
                        new_green[Color.green(newPixel)]++;
                        new_blue[Color.blue(newPixel)]++;
                    }
                }
                newImageView.setVisibility(View.VISIBLE);
                newImageView.setImageBitmap(hasil);
                makeHistogram(new_red, 256, R.id.histogram_result_red);
                makeHistogram(new_green, 256, R.id.histogram_result_green);
                makeHistogram(new_blue, 256, R.id.histogram_result_blue);
            }
        });
    }
    public int mapNewPixel(int pixel, int[] newRed, int[] newGreen, int[] newBlue) {
        int dummRed = (int) newRed[Color.red(pixel)];
        int dummGreen = (int) newGreen[Color.green(pixel)];
        int dummBlue = (int) newBlue[Color.blue(pixel)];

        return Color.rgb(dummRed, dummGreen, dummBlue);
    }

    private int[] roundCumulativeArray(float[] arr){
        float total = 0;
        int length = arr.length;
        int[] hasil = new int[length];
        float[] cumulative = new float[length];

        for(int i=0; i<length; i++){
            total = total + arr[i];
        }
        cumulative[0] = arr[0];
        for(int i=1; i<length; i++){
            cumulative[i] = arr[i] + cumulative[i-1];
        }
        for(int i=0; i<length; i++){
            cumulative[i] = cumulative[i] * (length-1) / total;
            hasil[i] = Math.round(cumulative[i]);
        }

        return hasil;
    }
    private int[] mapOriginalHistogram(int[] original_round, int[] target_round){
        int length = original_round.length;
        int[] hasil = new int[256];

        for(int i=0; i<length; i++){
            int j=0;
            while(target_round[j] < original_round[i] && j<length){
                j++;
            }
            hasil[i] = j;
        }
        return hasil;
    }
}

